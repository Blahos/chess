
class GameEngine;

#pragma once
#include <mutex>
#include <atomic>
#include <queue>
#include <algorithm>
#include <string>
#include <set>
#include <map>
#include <fstream>
#include <unordered_set>
#include "BoardState.h"
#include "CmdCommand.h"
#include "FunctionsMove.h"
#include "MoveProducer.h"
#include "Players/Player.h"
#include "Time.h"
#include "Exception.h"
#include "FunctionsBoard.h"
#include "MoveRequestId.h"
#include "Random.h"
#include "Players/KingHunter.h"
#include "Players/Materialist.h"

class GameEngine : public MoveProducer::Listener
{
public:

	enum class BoardMode
	{
		Game, Free
	};

	class Listener
	{
	public:
		virtual void BoardChanged(const BoardState& aNewBoard) = 0;
	};

private:

	Players::TreeSearch* mAnalyser = nullptr;

	Move mLastMove;
	BoardState mCurrentBoard;
	std::set<Listener*> mListeners = std::set<Listener*>();
	BoardMode mMode = BoardMode::Free;
	std::map<Colour, Players::Player*> mPlayers = { { C_WHITE, nullptr }, { C_BLACK, nullptr } };
	//std::unordered_set<BoardState, BoardState::Hash> mHashSet;
	int mGameMovesPlayed = 0;
	std::vector<Move> mCurrentGameMoves;
	/// <summary>
	/// includes all boards of this game, including the current one
	/// </summary>
	std::vector<BoardState> mCurrentGameBoards;
	Time mGameTime;
	bool mGameRunning = false;
	unsigned long long mCurrentGameId = 0;
	MoveRequestId mCurrentMoveRequest;
	// indicates whether the game is new for given player (i.e. true before their first move, false afterwards)
	std::map<Colour, bool> mIsNewGameForPlayer = { {C_WHITE, false}, { C_BLACK, false } };

	// moves (in PGN) that will be made from starting position after new game starts, before the game actually begins
	std::vector<std::string> mInitialMoves;

	std::string mCurrentGameFilePath = "";

	void MakeMove(const Move aMove, const std::string& aMoveNotes)
	{
		BoardState lBoard = mCurrentBoard;
		BoardState lNewBoard;

		try
		{
			const BoardState lOriginalBoardCopy(lBoard);
			lNewBoard = lBoard.MakeMove(aMove);
			mLastMove = aMove;
			//lNewBoard = BoardState::Decompress(BoardState::MakeMoveRawCompressed(lBoard.Compress(), aMove));
			lBoard = lNewBoard;

			std::cout << "New board game phase: " << std::endl;
			std::cout << lBoard.ComputeGamePhase() << std::endl;

			const bool lThreefold = IsThreefoldRepetition(lNewBoard, mCurrentGameBoards);
			if (lThreefold) std::cout << "Threefold repetition detected, game over (draw)" << std::endl;

			if (lNewBoard.IsMate())
				std::cout << "Checkmate, game over (" << ((lNewBoard.PlayerToMove & C_WHITE) ? "black" : "white") << " wins)" << std::endl;

			if (lNewBoard.IsStalemate())
				std::cout << "Stalemate, game over (draw)" << std::endl;

			if (mCurrentGameFilePath != "")
			{
				std::ofstream lFile(mCurrentGameFilePath, std::ios::app);
				lFile << MoveToPGN(aMove, lOriginalBoardCopy);

				if (aMoveNotes.length() > 0)
				{
					lFile << " { " << aMoveNotes << " }";
				}
				lFile << std::endl;
			}
		}
		catch (const Exception & lEx)
		{
			std::cout << "Move exception in GameEngine: " << std::endl;
			std::cout << lEx << std::endl;
			return;
		}

		mCurrentBoard = lNewBoard;
		mCurrentGameBoards.push_back(lNewBoard);
		mCurrentGameMoves.push_back(aMove);

		NotifyListeners();
	}
	void MakeMove(const Move aMove)
	{
		std::string lDummy;
		MakeMove(aMove, lDummy);
	}

	void AppendPGNHeader()
	{
		if (mCurrentGameFilePath == "") return;
		std::ofstream lFile(mCurrentGameFilePath, std::ios::app);

		lFile << "[Event \"" << "Jiri's chess app" << "\"]" << std::endl;
		lFile << "[White \"" << mPlayers[C_WHITE]->GetName() << "\"]" << std::endl;
		lFile << "[Black \"" << mPlayers[C_BLACK]->GetName() << "\"]" << std::endl;
		lFile << std::endl;
	}

public:

	void SetInitialMoves(const std::vector<std::string>& aPGNMoves)
	{
		mInitialMoves = aPGNMoves;
	}

	void NewGame(const std::vector<std::string>& aInitialMovesPGN = std::vector<std::string>(), 
		const BoardState& aStartBoard = BoardState::CreateInitialPosition())
	{
		if (mPlayers.find(C_WHITE) == mPlayers.end() || mPlayers.find(C_BLACK) == mPlayers.end())
			throw EXCEPTION_MEMBER("Missing a player!");

		if (mGameRunning)
		{
			std::cout << "Previous game total time: " << mGameTime.Elapsed() << " ms" << std::endl;
		}

		mPlayers[C_WHITE]->InvalidateAllRequests();
		mPlayers[C_BLACK]->InvalidateAllRequests();

		mCurrentBoard = aStartBoard;
		mGameMovesPlayed = 0;
		mLastMove.Start = BoardCoordinates(-1, -1);
		mLastMove.End = BoardCoordinates(-1, -1);
		mCurrentGameBoards.clear();
		mCurrentGameMoves.clear();
		mCurrentGameBoards.push_back(mCurrentBoard);
		mCurrentGameFilePath = "game_" + Time::CurrentUTCTimeString() + ".txt";
		AppendPGNHeader();
		NotifyListeners();

		for (int i = 0; i < aInitialMovesPGN.size(); i++)
		{
			try
			{
				const Move lMove = ParsePGNMove(aInitialMovesPGN[i], mCurrentBoard);
				MakeMove(lMove);
			}
			catch (const Exception & lEx)
			{
				throw EXCEPTION_MEMBER("Error parsing move no. " + std::to_string(i + 1) + " (" + aInitialMovesPGN[i] + "): " + lEx.Message + ", " + lEx.File + ", " + std::to_string(lEx.LineNumber));
			}
		}

		mGameTime.Start();
		mGameRunning = true;

		unsigned long long lNewId = mCurrentGameId;
		while (lNewId == mCurrentGameId)
		{
			lNewId = Random::GetIntU64(0, std::numeric_limits<unsigned long long>::max());
		}

		mCurrentGameId = lNewId;
		mCurrentMoveRequest.IsValid = false;
		mIsNewGameForPlayer[C_WHITE] = true;
		mIsNewGameForPlayer[C_BLACK] = true;
	}

	GameEngine()
	{
		//mAnalyser = new Players::Materialist(6, -1);
		mAnalyser = new Players::KingHunter(6, -1);
		mAnalyser->SetOnlyMoveShortcut(false);
	}

	~GameEngine()
	{
		if (mAnalyser != nullptr)
			delete mAnalyser;
	}

	Move GetLastMove()
	{
		return mLastMove;
	}

	BoardState GetCurrentBoard()
	{
		return mCurrentBoard;
	}

	void SetMode(const BoardMode aMode) { mMode = aMode; }

	void Subscribe(Listener* const aListener)
	{
		if (mListeners.find(aListener) == mListeners.end())
			mListeners.insert(aListener);
	}
	void Unsubscribe(Listener* const aListener)
	{
		if (mListeners.find(aListener) != mListeners.end())
			mListeners.erase(aListener);
	}
	void NotifyListeners()
	{
		for (auto nListener : mListeners)
		{
			nListener->BoardChanged(mCurrentBoard);
		}
	}

	void SetPlayer(const Colour aColour, Players::Player* const aPlayer)
	{
		mPlayers[aColour] = aPlayer;
	}

	const Players::Player* const GetPlayer(const Colour aColour)
	{
		if (mPlayers.find(aColour) == mPlayers.end()) throw EXCEPTION_MEMBER("Player not present!");
		return mPlayers[aColour];
	}

	void PlayGameTick()
	{
		if (mMode != BoardMode::Game) return;

		const BoardState lBoardCopy = mCurrentBoard;

		std::vector<BoardState> lGameHistory(mCurrentGameBoards);
		lGameHistory.pop_back();

		if (lBoardCopy.IsMate() || lBoardCopy.IsStalemate() || IsThreefoldRepetition(lBoardCopy, lGameHistory) || mGameMovesPlayed >= 1000
			/*|| lGameHistory.size() == 8*/ /* HACK */)
		{
			if (mGameRunning)
			{
				mGameRunning = false;
				std::cout << "Game total time: " << mGameTime.Elapsed() << " ms" << std::endl;
			}

			/*
			// HACK
			if (lGameHistory.size() == 8) NewGame();
			*/
			return;
		}

		Players::Player* const lPlayerToMove = mPlayers[lBoardCopy.PlayerToMove];

		if (lPlayerToMove == nullptr)
			throw EXCEPTION_MEMBER("Player not set!");

		if (mCurrentMoveRequest.IsValid)
		{
			std::optional<MoveWithNotes> lMoveWrapper = lPlayerToMove->GetMove(mCurrentMoveRequest, true);

			if (lMoveWrapper)
			{
				const BoardState lOriginalBoard = mCurrentBoard;
				MoveWithNotes lMove = lMoveWrapper.value();
				if (lMove.Move_.MovedPiece & ~lBoardCopy.PlayerToMove) throw EXCEPTION_MEMBER("Incorrect colour!");
				std::cout << "Move played: " << MoveToPGN(lMove.Move_, lOriginalBoard) << std::endl;
				MakeMove(lMove.Move_, lMove.Notes);
				mGameMovesPlayed++;
				mCurrentMoveRequest.IsValid = false;
				mIsNewGameForPlayer[lBoardCopy.PlayerToMove] = false;
			}
		}
		else
		{
			std::cout << ((lBoardCopy.PlayerToMove & C_WHITE) ? "White" : "Black") << " to move" << std::endl;

			mCurrentMoveRequest.IsValid = true;
			mCurrentMoveRequest.MoveNumber = mCurrentGameBoards.size();
			mCurrentMoveRequest.GameId = mCurrentGameId;
			mCurrentMoveRequest.PlayerToMove = lBoardCopy.PlayerToMove;
			mCurrentMoveRequest.ResetBefore = mIsNewGameForPlayer[lBoardCopy.PlayerToMove];

			std::vector<BoardState> lGameHistory(mCurrentGameBoards);
			lGameHistory.pop_back();

			lPlayerToMove->PrepareMove(lBoardCopy, lGameHistory, mCurrentGameMoves, mCurrentMoveRequest);
		}
	}

	// returns true if the command was (or could be at least) applied here, otherwise false
	bool ApplyCmdCommand(const CmdCommand aCommand)
	{
		switch (aCommand.Type)
		{
			case CmdCommandType::NewGame:
			{
				NewGame(mInitialMoves);
				return true;
				break;
			}
			case CmdCommandType::Evaluate:
			{
				// perform static evaluation of the current game position

				if (mGameRunning)
				{
					std::vector<BoardState> lGameHistory(mCurrentGameBoards);
					lGameHistory.pop_back();

					const double lValue = mAnalyser->StaticEvaluation(mCurrentBoard, lGameHistory);
					std::cout << "Current position static evaluation : " << lValue << std::endl;
					std::cout << "Evaluator: " << mAnalyser->GetName() << std::endl;
				}
				else std::cout << "No game running at the momoment" << std::endl;


				return true;
				break;
			}
			case CmdCommandType::Analyse:
			{
				// perform tree analysis
				if (mGameRunning)
				{
					std::vector<BoardState> lGameHistory(mCurrentGameBoards);
					lGameHistory.pop_back();

					BoardState lAnalysedBoard(mCurrentBoard);
					const Colour lPlayerToMove = mCurrentBoard.PlayerToMove;

					// if a move is analysed, we perform the move on the board first and then use analysis with depth - 1
					// to maintain the same depth with regards to the given position
					int lDepthDecrease = 0;
					if (aCommand.Arguments.size() > 0)
					{
						Move lCommandMove;
						try
						{
							// we analyse the same position for given move
							lCommandMove = ParsePGNMove(aCommand.Arguments[0], mCurrentBoard);
						}
						catch (const Exception& aEx)
						{
							std::cout << "Could not parse move: " << aCommand.Arguments[0] << std::endl;
							std::cout << "Exception: " << aEx << std::endl;
							return true;
						}

						lDepthDecrease = 1;
						lGameHistory.push_back(lAnalysedBoard);
						lAnalysedBoard = lAnalysedBoard.MakeMove(lCommandMove);

						std::cout << "Analysing position after move: " << MoveToPGN(lCommandMove, mCurrentBoard) << std::endl;
					}
					else
					{
						std::cout << "Analysing current position" << std::endl;
					}

					const int lUsedDepth = mAnalyser->GetDepth() - lDepthDecrease;
					const Players::TreeSearch::VariationValue lResult = mAnalyser->TreeAnalysis(lAnalysedBoard, lGameHistory, lPlayerToMove, lUsedDepth);
					std::cout << "Tree evaluation value		: " << lResult.Value << std::endl;
					std::cout << "Tree evaluation variation	: ";
					lResult.PrintVariation(lAnalysedBoard);
					std::cout << "Evaluator: " << mAnalyser->GetName() << std::endl;
				}
				else
					std::cout << "No game running at the momoment" << std::endl;


				return true;
				break;
			}
			case CmdCommandType::ExportFEN:
			{
				std::string lFileName = "FEN_" + Time::CurrentUTCTimeString() + "_" + std::to_string(Random::GetInt(1000, 9999)) + ".txt";
				if (aCommand.Arguments.size() > 0 && aCommand.Arguments[0].size() > 0)
				{
					lFileName = aCommand.Arguments[0];
				}

				std::string lFEN = mCurrentBoard.ToFEN(-1, (mCurrentGameBoards.size() - 1) / 2 + 1);

				std::ofstream lFile(lFileName);
				lFile << lFEN << std::endl;

				std::cout << "Board FEN written to \"" << lFileName << "\"" << std::endl;

				return true;
				break;
			}
			case CmdCommandType::ImportPGN:
			{
				if (aCommand.Arguments.size() > 0 && aCommand.Arguments[0].size() > 0)
				{
					const std::vector<std::string> lMovesPGN = ParsePGNMovesFile(aCommand.Arguments[0]);
					NewGame(lMovesPGN);
				}
				else std::cout << "Missing PGN file name for import as argument!" << std::endl;

				return true;
				break;
			}
			case CmdCommandType::ImportFEN:
			{
				if (aCommand.Arguments.size() > 0 && aCommand.Arguments[0].size() > 0)
				{

					std::string lFENString;

					const std::string lPath = aCommand.Arguments[0];
					std::ifstream lFile(lPath);

					if (!lFile.is_open())
					{
						// try the arguments as FEN string rather than a path
						if (aCommand.Arguments.size() >= 4)
						{
							for (int i = 0; i < aCommand.Arguments.size(); i++)
							{
								lFENString += aCommand.Arguments[i] + " ";
							}
						}
						else
						{
							std::cout << "Could not use arguments either as a file path or a FEN string" << std::endl;
						}
					}
					else
					{
						while (!lFile.eof())
						{
							std::string lBuffer;
							std::getline(lFile, lBuffer);
							lFENString += lBuffer;
						}
					}

					const BoardState lBoard = BoardState::FromFEN(lFENString);

					NewGame(std::vector<std::string>(), lBoard);
				}
				else std::cout << "Missing FEN file name for import as argument!" << std::endl;

				return true;
				break;
			}
			case CmdCommandType::CountPositions:
			{
				if (aCommand.Arguments.size() > 0 && aCommand.Arguments[0].size() > 0)
				{
					int lMaxDepth;
					try
					{
						lMaxDepth = std::stoi(aCommand.Arguments[0]);
					}
					catch (...)
					{
						std::cout << "Could not parse argument: " << aCommand.Arguments[0] << " as integer!" << std::endl;
						return true;
					}

					if (lMaxDepth < 0)
					{
						std::cout << "Max depth must be non-negative!" << std::endl;
						return true;
					}

					std::cout << "Counting number of positions on depth " << lMaxDepth << std::endl;
					Time lTime; lTime.Start();
					const int lPositionCount = mCurrentBoard.CountPositions(lMaxDepth, lMaxDepth);
					const double lElapsed = lTime.Elapsed();

					std::cout << "Position count: " << lPositionCount << " (time: " << lElapsed / 1000 << "s)" << std::endl;
				}
				else
				{
					std::cout << "Depth argument missing!" << std::endl;
				}
				return true;
			}
			default:
			{
				return false;
				break;
			}
		}

		return false;
	}

	virtual void NewMovePGN(const std::string& aMovePGN) override
	{
		if (mMode == BoardMode::Free)
		{
			Move lMove;
			try
			{
				lMove = ParsePGNMove(aMovePGN, mCurrentBoard);
			}
			catch (const Exception & lEx)
			{
				std::cout << "Exception when parsing the PGN move: " << std::endl;
				std::cout << lEx << std::endl;
				return;
			}
			MakeMove(lMove);
		}
	}

	virtual void NewMove(const Move aMove) override
	{
		if (mMode == BoardMode::Free)
		{
			MakeMove(aMove);
		}
	}
};