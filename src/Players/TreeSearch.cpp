
#include "TreeSearch.h"

// checkmate in zero meaning the checkmate is already on the board
// no "standard" position evaluation should ever reach a value of 30000 (from this value above it's reserved for checkmates)
const short Players::TreeSearch::cCheckmateInZeroWhite = 31000;
// we assume that there will never be checkmate in 1000 plies (seems reasonable)
const short Players::TreeSearch::cCheckmateThreshold = 30000;