
namespace Players
{
	class MonteCarlo;
}

#pragma once
#include "../PieceMovement.h"
#include "Player.h"

namespace Players
{
	class MonteCarlo : public Player
	{
	protected:
		virtual MoveWithNotes PrepareMoveInner(const BoardState& aBoard,
			const std::vector<BoardState>& aGameHistory, const std::vector<Move>& aMoveHistory,
			const std::atomic<bool>* const aIsAlive) override
		{

		}

	public:
		virtual std::string GetName() const override
		{
			return "MonteCarlo";
		}
	};
}
