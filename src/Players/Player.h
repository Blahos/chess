
namespace Players
{
	class Player;
}

#pragma once
#include <mutex>
#include <string>
#include <thread>
#include <map>
#include "../BoardState.h"
#include "../Move.h"
#include "../Exception.h"
#include "../MoveRequestId.h"

namespace Players
{
	class Player
	{
	private:
		std::mutex mStateMutex;
		std::mutex mPrepareMoveThreadMutex;
		std::mutex mAliveFlagsMutex;

		//bool mWaitingForMove = false;
		//bool mIsMovePrepared = false;
		//bool mResetFlag = false;

		std::map<MoveRequestId, MoveWithNotes> mPreparedMoves;
		std::map<MoveRequestId, std::atomic<bool>> mAliveFlags;

	protected:
		// the IsAlive bool pointer must be guaranteed to be valid for the entire run of this function
		// this function will never be running more than once again
		// it runs in a separate thread so it doesn't block anything, but it's inside a mutex so it's still synchronised for only one run at a time
		virtual MoveWithNotes PrepareMoveInner(const BoardState& aBoard, 
			const std::vector<BoardState>& aGameHistory, const std::vector<Move>& aMoveHistory,
			const std::atomic<bool> * const aIsAlive) = 0;

		// do whatever to reset the state of the player (i.e. clear cache)
		// typically done when a new game starts or something
		// will run in synch with the prepare move thread
		// since it will be done on a move request
		virtual void Reset()
		{

		}

	public:

		virtual std::string GetName() const = 0;

		void PrepareMoveThreadFunc(const BoardState aBoard, 
			const std::vector<BoardState> aGameHistory, const std::vector<Move> aMoveHistory, 
			const MoveRequestId aRequestId)
		{
			mAliveFlagsMutex.lock();

			if (mAliveFlags.find(aRequestId) != mAliveFlags.end())
				throw EXCEPTION_MEMBER("Duplicate move request id!");

			// create a new IsAlive flag for this request
			mAliveFlags[aRequestId] = true;
			mAliveFlagsMutex.unlock();

			MoveWithNotes lMove;
			try
			{
				mPrepareMoveThreadMutex.lock();

				if (aRequestId.ResetBefore)
					Reset();

				lMove = PrepareMoveInner(aBoard, aGameHistory, aMoveHistory, &mAliveFlags[aRequestId]);

				mPrepareMoveThreadMutex.unlock();
			}
			catch (const Exception & lEx)
			{
				mPrepareMoveThreadMutex.unlock();

				mAliveFlagsMutex.lock();
				mAliveFlags.erase(aRequestId);
				mAliveFlagsMutex.unlock();

				std::cout << "Prepare move exception: " << std::endl;
				std::cout << lEx << std::endl;
				return;
			}

			mAliveFlagsMutex.lock();
			mAliveFlags.erase(aRequestId);
			mAliveFlagsMutex.unlock();

			mStateMutex.lock();
			mPreparedMoves[aRequestId] = lMove;
			mStateMutex.unlock();
		}
		// GameHistory contains all boards in this game BEFORE the current board provided in the first argument
		void PrepareMove(const BoardState& aBoard, 
			const std::vector<BoardState>& aGameHistory, const std::vector<Move>& aMoveHistory,
			const MoveRequestId& aRequestId)
		{
			if (!aRequestId.IsValid)
				throw EXCEPTION_MEMBER("Requesting move with invalid id!");
			
			std::thread(&Player::PrepareMoveThreadFunc, this, aBoard, aGameHistory, aMoveHistory, aRequestId).detach();
		}

		std::optional<MoveWithNotes> GetMove(const MoveRequestId& aRequestId, const bool aClearOnSuccess)
		{
			std::optional<MoveWithNotes> lReturnValue = std::nullopt;
			mStateMutex.lock();
			if (mPreparedMoves.find(aRequestId) != mPreparedMoves.end())
			{
				lReturnValue = mPreparedMoves[aRequestId];
				if (aClearOnSuccess) mPreparedMoves.clear();
			}
			mStateMutex.unlock();
			return lReturnValue;
		}

		// tells the player that all the move requests made up to this point are no longer relevant
		void InvalidateAllRequests()
		{
			mAliveFlagsMutex.lock();
			mStateMutex.lock();
			mPreparedMoves.clear();

			for (auto& lIt : mAliveFlags)
			{
				lIt.second = false;
			}

			mStateMutex.unlock();
			mAliveFlagsMutex.unlock();
		}
	};
}
