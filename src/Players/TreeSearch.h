
namespace Players
{
	class TreeSearch;
}

#pragma once
#include <atomic>
#include <unordered_map>
#include <vector>
#include <limits>
#include <functional>
#include <tuple>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <optional>
#include <mutex>
#include <omp.h>

#include "Player.h"

#include "../Exception.h"
#include "../PieceMovement.h"
#include "../Time.h"
#include "../FunctionsBoard.h"
#include "../FunctionsMove.h"

#include "robin_hood.h"

namespace Players
{
	class TreeSearch : public Player
	{
	public:
		static const short cCheckmateInZeroWhite; 
		static const short cCheckmateThreshold;

		const bool cUseLeafCache;
		const bool cUseInnerTreeCache;
		const bool cGoDeeperOnCapture;

		class VariationValue
		{
		public:
			std::vector<Move> VariationMoves;
			// value of a board is in centipawns
			short Value = 0;
			//bool IsValid = true;

			void PrintVariation(const BoardState& aBoard) const
			{
				BoardState lTempBoardCopy(aBoard);
				for (int i = 0; i < VariationMoves.size(); i++)
				{
					const Move& lMove = VariationMoves[i];
					std::cout << MoveToPGN(lMove, lTempBoardCopy) << " ";
					lTempBoardCopy = lTempBoardCopy.MakeMove(lMove);
				}
				std::cout << std::endl;
			}
		};

	private:

		bool mOnlyMoveShortcut = true;

		// actual value for stalemate, from white's perspective (negative means stalemate is considered as worse than a 0 (exact draw))
		// value of a board is in centipawns
		constexpr static short cStalemateBias = -101;

		enum class MinMax
		{
			Min, Max
		};

		enum class ValueType
		{
			Equal, ThisColourLimit, OppositeColourLimit
		};

		enum class TerminalPositionType : unsigned char
		{
			None, Stalemate, WhiteCheckmated, BlackCheckmated
		};

		struct LeafCacheItem
		{
		public:
			BoardState Board;
			// value of a board is in centipawns
			short Value = 0;
			bool IsSet = false;
		};

		struct InnerTreeCacheItem
		{
		public:
			BoardState Board;
			// value of a board is in centipawns
			short Value = 0;
			ValueType ValueType_ = ValueType::Equal;
			bool IsSet = false;
			int Depth = -1;
		};

		struct CacheData
		{
		public:
			LeafCacheItem* LeafCache = nullptr;
			size_t LeafCacheSize = 0;
			bool UseLeafCache = false;

			InnerTreeCacheItem* InnerTreeCache = nullptr;
			size_t InnerTreeCacheSize = 0;
			bool UseInnerTreeCache = false;
			int InnerTreeCacheUse = 0;
			int InnerTreeCacheFind = 0;
			int InnerTreeCacheImprove = 0;
		};

		// 361531	- cca 100MB
		// 1807691	- cca 500MB
		// 3615299  - cca 1GB
		// 7230611  - cca 2GB
		// 14810233 - cca 4GB
		const size_t cLeafCacheSize;
		const size_t cInnerTreeCacheSize;
		const bool cDoMinimalWindowSearch;

		// cache for exact board evaluations
		LeafCacheItem* mLeafCache = nullptr;
		// cache for inner tree board evaluations
		InnerTreeCacheItem* mInnerTreeCache = nullptr;
		//static InnerTreeCacheItem mInnerTreeCacheDummyItem;

		// value of a board is in centipawns

		const std::function<short(const BoardState & aBoardState,
			const std::optional<BoardStateExtra> & aBoardExtraInfo,
			const int aDepth,
			const std::vector<BoardState> & aGameHistory,
			TerminalPositionType & aTerminalPosition, const bool aIsTerminalPositionAlreadySet, const Colour)> mEvalFunc =
			[&](const BoardState& aBoard, const std::optional<BoardStateExtra>& aBoardExtraInfo,
				const int aDepth,
				const std::vector<BoardState>& aGameHistory,
				TerminalPositionType& aTerminalPosition, const bool aIsTerminalPositionAlreadySet, const Colour aPointOfViewPlayer)
		{
			return this->EvaluatePosition(aBoard, aBoardExtraInfo, aDepth, aGameHistory, aTerminalPosition, aIsTerminalPositionAlreadySet, aPointOfViewPlayer);
		};

		class TreeNode
		{
		private:
			// for debugging purposes, saves the entire evaluation into a file (by appending for each node as they are walked throuhg)
			const bool cSaveEvaluationToFile = false;
			const std::string cEvaluationSaveFile = "tree_node_evaluation_file.txt";

			CacheData mSecondaryCache;
			
		public:

			BoardState State;
			std::optional<BoardStateExtra> StateExtraInfo = std::nullopt;

			struct BoardValue
			{
			public:
				BoardState BoardBeforeMove;
				BoardState Board;
				// value of a board is in centipawns
				short Value;
				short ValueDeeper;
				Move Move_;
				BoardStateExtra ExtraInfo;
				TerminalPositionType TerminalPositionType_;

				struct Smaller
				{
				public:
					inline bool operator()(const BoardValue& a1, const BoardValue& a2) const
					{
						return a1.Value < a2.Value;
					}
				};

				struct SmallerDeeper
				{
				public:
					inline bool operator()(const BoardValue& a1, const BoardValue& a2) const
					{
						return a1.ValueDeeper < a2.ValueDeeper;
					}
				};

				struct SmallerDeeperExtra
				{
				public:
					inline bool operator()(const BoardValue& a1, const BoardValue& a2) const
					{
						const bool lIsCapture1 = a1.Move_.IsCapture(a1.BoardBeforeMove);
						const bool lIsCapture2 = a2.Move_.IsCapture(a2.BoardBeforeMove);
						if (lIsCapture1 != lIsCapture2) return lIsCapture1 > lIsCapture2;
						else return a1.ValueDeeper < a2.ValueDeeper;
					}
				};

				struct Greater
				{
				public:
					inline bool operator()(const BoardValue& a1, const BoardValue& a2) const
					{
						return a1.Value > a2.Value;
					}
				};

				struct GreaterDeeper
				{
				public:
					inline bool operator()(const BoardValue& a1, const BoardValue& a2) const
					{
						return a1.ValueDeeper > a2.ValueDeeper;
					}
				};

				struct GreaterDeeperExtra
				{
				public:
					inline bool operator()(const BoardValue& a1, const BoardValue& a2) const
					{
						const bool lIsCapture1 = a1.Move_.IsCapture(a1.BoardBeforeMove);
						const bool lIsCapture2 = a2.Move_.IsCapture(a2.BoardBeforeMove);
						if (lIsCapture1 != lIsCapture2) return lIsCapture1 > lIsCapture2;
						else return a1.ValueDeeper > a2.ValueDeeper;
					}
				};
			};

			VariationValue Evaluate(
				const std::function<short(const BoardState & aBoard, const std::optional<BoardStateExtra> & aBoardExtraInfo, const int aDepth, const std::vector<BoardState> & aGameHistory, TerminalPositionType & aTerminalPosition, const bool aIsTerminalPositionAlreadySet, const Colour aPointOfViewPlayer)>& aEvalFunc,
				const MinMax aMinMaxChoice,
				const int aCurrentDepth,
				const int aMaxDepth,
				const short aBestValueWhite,
				const short aBestValueBlack,
				std::vector<BoardState>& aGameHistory,
				int& aNodesEvaluated,
				int& aNodesVisited,
				const Time& aStartTime,
				const double aMaxTime,
				bool& aTimedOut,
				const Colour aPointOfViewPlayer,
				TreeSearch::CacheData& aCacheData,
				const std::atomic<bool>* const aIsAlive,
				const bool aDoMinimalWindowSearch,
				const std::vector<Move>& aBestPreviousVariation = std::vector<Move>(),
				const bool& aIsOnBestPreviousVariation = false,
				const bool& aIsSecondarySearch = false,
				const bool& aOnlyCaptures = false,
				const bool& aGoDeeperOnCaptures = false)
			{
				aNodesVisited++;
				if (!(*aIsAlive))
				{
					// return whatever, won't matter, this evaluation is marked as useless
					return VariationValue();
				}

				//const double lBestValueWhiteEntry = aBestValueWhite;
				//const double lBestValueBlackEntry = aBestValueBlack;

				//const double lBestValueWhiteOrig = aBestValueWhite;

				short lBestValueWhite = aBestValueWhite;
				short lBestValueBlack = aBestValueBlack;

				VariationValue lReturnVariation;
				
				InnerTreeCacheItem* lCacheItem;
				size_t lCurrentPositionHash;
				// check if the current position is already in the cache
				if (aCacheData.UseInnerTreeCache)
				{
					aCacheData.InnerTreeCacheUse++;
					lCurrentPositionHash = this->State.HashValue;
					const size_t lCachePosition = lCurrentPositionHash % aCacheData.InnerTreeCacheSize;
					lCacheItem = &aCacheData.InnerTreeCache[lCachePosition];

					bool lHit = false;
					bool lReturn = false;

					if (lCacheItem->IsSet && lCacheItem->Depth == aCurrentDepth)
					{
						if (lCacheItem->Board.HashValue == lCurrentPositionHash)
						{
							if (lCacheItem->Board == this->State)
							{
								aCacheData.InnerTreeCacheFind++;
								if (lCacheItem->ValueType_ == ValueType::Equal)
								{
									aCacheData.InnerTreeCacheImprove++;
									lReturn = true;
									lReturnVariation.Value = lCacheItem->Value;
								}
								else if (
									(lCacheItem->ValueType_ == ValueType::ThisColourLimit && aMinMaxChoice == MinMax::Min)
									||
									(lCacheItem->ValueType_ == ValueType::OppositeColourLimit && aMinMaxChoice == MinMax::Max))
								{
									if (lCacheItem->Value < lBestValueBlack)
									{
										aCacheData.InnerTreeCacheImprove++;
										lBestValueBlack = lCacheItem->Value;
									}
								}
								else if (
									(lCacheItem->ValueType_ == ValueType::ThisColourLimit && aMinMaxChoice == MinMax::Max)
									||
									(lCacheItem->ValueType_ == ValueType::OppositeColourLimit && aMinMaxChoice == MinMax::Min))
								{
									if (lCacheItem->Value > lBestValueWhite)
									{
										aCacheData.InnerTreeCacheImprove++;
										lBestValueWhite = lCacheItem->Value;
									}
								}

								if (lBestValueBlack <= lBestValueWhite)
								{
									lReturnVariation.Value = lCacheItem->Value;
									lReturn = true;
								}
							}
						}
					}

					if (lReturn) return lReturnVariation;
				}

				aNodesEvaluated++;

				bool lChildrenEvaluated = false;

				MinMax lNextLevelMinMax;
				if (aMinMaxChoice == MinMax::Min)
				{
					lNextLevelMinMax = MinMax::Max;
				}
				else
				{
					lNextLevelMinMax = MinMax::Min;
				}

				//if (aCurrentDepth < aMaxDepth)
				//{
					StaticList<Move, PieceMovement::cMaxLegalMoves> lAllLegalMoves;
					PieceMovement::GetAllLegalMoves(State, lAllLegalMoves, StateExtraInfo);

					std::vector<BoardValue> lBoardValues;
					lBoardValues.reserve(lAllLegalMoves.ActualSize);

					BoardValue lNew;
					TreeNode lNewTmp;

					// transform moves into board value structs 
					// (i.e. generate children boards and evaluate them statically for sorting purposes)
					for (int i = 0; i < lAllLegalMoves.ActualSize; i++)
					{
						if (aOnlyCaptures && !lAllLegalMoves.Items[i].IsCapture(State))
							continue;

						lNew.BoardBeforeMove = State;
						lNew.Board = State.MakeMoveRaw(lAllLegalMoves.Items[i]);

						//lNew.ExtraInfo = BoardStateExtra();
						lNew.ExtraInfo.IsMovingPlayerInCheck = lNew.Board.IsMovingPlayerInCheck();
						// move that lead to this position
						lNew.Move_ = lAllLegalMoves.Items[i];

						// check leaf cache
						bool lLeafCacheUsed = false;

						LeafCacheItem* lLeafCacheItem = nullptr;

						// first check if the position is terminal or not
						lNew.TerminalPositionType_ = GetTerminalPositionType(lNew.Board, aGameHistory, lNew.ExtraInfo);

						if (aCacheData.UseLeafCache && lNew.TerminalPositionType_ == TerminalPositionType::None)
						{
							const size_t lLeafCachePositionReal = lNew.Board.HashValue % aCacheData.LeafCacheSize;
							if (lLeafCachePositionReal < aCacheData.LeafCacheSize)
							{
								lLeafCacheItem = aCacheData.LeafCache + lLeafCachePositionReal;
								if (lLeafCacheItem->IsSet && lNew.Board == lLeafCacheItem->Board)
								{
									lLeafCacheUsed = true;
									lNew.Value = lLeafCacheItem->Value;
								}
							}
						}

						if (!lLeafCacheUsed)
						{
							lNew.Value = aEvalFunc(lNew.Board, lNew.ExtraInfo, aCurrentDepth, aGameHistory, lNew.TerminalPositionType_, true, aPointOfViewPlayer);
						}

						lNew.ValueDeeper = lNew.Value;

						int lExtraEvalDepth = 0;
						// uncomment one of these to enable extra sorting
						//if (aCurrentDepth == 1 && aMaxDepth > 6) lExtraEvalDepth = 4;
						//if (aCurrentDepth <= 0 && aMaxDepth > 4) lExtraEvalDepth = 3;

						if (lExtraEvalDepth > 0 && !aIsSecondarySearch)
						{
							// set up the child node
							lNewTmp.State = lNew.Board;
							lNewTmp.StateExtraInfo = lNew.ExtraInfo;

							aGameHistory.push_back(State);

							int lDummy1 = 0;
							int lDummy2 = 0;
							bool lDummy3 = false;
							const std::atomic<bool> lDummy4 = true;

							mSecondaryCache.LeafCache = aCacheData.LeafCache;
							mSecondaryCache.UseLeafCache = aCacheData.UseLeafCache;
							mSecondaryCache.LeafCacheSize = aCacheData.LeafCacheSize;

							const VariationValue lVarTmp = lNewTmp.Evaluate(aEvalFunc, lNextLevelMinMax, 0, lExtraEvalDepth,
								std::numeric_limits<short>::lowest(), std::numeric_limits<short>::max(),
								aGameHistory, lDummy1, lDummy2, aStartTime, -1, lDummy3, GetOppositeColour(aPointOfViewPlayer),
								mSecondaryCache, &lDummy4, aDoMinimalWindowSearch, { }, false, true, false, false);

							lNew.ValueDeeper = lVarTmp.Value;

							aGameHistory.pop_back();
						}
						
						// we only save non terminal positions
						if (aCacheData.UseLeafCache && lLeafCacheItem != nullptr
							&& !lLeafCacheUsed && lNew.TerminalPositionType_ == TerminalPositionType::None)
						{
							lLeafCacheItem->Board = lNew.Board;
							lLeafCacheItem->IsSet = true;
							lLeafCacheItem->Value = lNew.Value;
						}

						lBoardValues.push_back(lNew);

						// if we are on the bottom depth (leaves), break if we crossed the best value (no point in continuing evaluation, we prune)
						// we can do this only at the last level since we know we are not going to evaluate these nodes any further
						//if (aCurrentDepth == aMaxDepth - 1)
						//{
						//	// we increment the node eval count here, since we are not going to call the evaluate function on the child
						//	// because it is a leaf node, so we directly use the value computed here, so we also account for the child node evaluation here
						//	// only increment if cache was not used (i.e. the eval function was called)
						//	if (!lLeafCacheUsed) aNodesEvaluated++;
						//	aNodesVisited++;

						//	if (aMinMaxChoice == MinMax::Min)
						//	{
						//		if (lNew.Value <= aBestValueWhite)
						//		{
						//			break;
						//		}
						//	}
						//	else
						//	{
						//		if (lNew.Value >= aBestValueBlack)
						//		{
						//			break;
						//		}
						//	}
						//}
					}

					// if there are any moves to make
					if (lBoardValues.size() > 0)
					{
						// the children of this node were evaluated
						lChildrenEvaluated = true;

						// prepare placeholders for the best variation
						VariationValue lBestChildVariation;
						Move lBestMove;
						//bool lBestChildVariationAssigned = false;

						int lMinMaxSign;

						// prepare settings for the next level
						if (aMinMaxChoice == MinMax::Min)
						{
							// sort if we are not at the bottom level
							if (aCurrentDepth < aMaxDepth - 1 && !aIsSecondarySearch)
								std::stable_sort(lBoardValues.begin(), lBoardValues.end(), BoardValue::SmallerDeeper());

							lBestChildVariation.Value = std::numeric_limits<short>::max();
							lMinMaxSign = 1;
						}
						else
						{
							// sort if we are not at the bottom level
							if (aCurrentDepth < aMaxDepth - 1 && !aIsSecondarySearch)
								std::stable_sort(lBoardValues.begin(), lBoardValues.end(), BoardValue::GreaterDeeper());

							lBestChildVariation.Value = std::numeric_limits<short>::lowest();
							lMinMaxSign = -1;
						}

						bool lBestPreviousVariationMoveFound = false;
						// if we are on the best previous variation, put that move to the front of the list
						if (aIsOnBestPreviousVariation && aCurrentDepth < aBestPreviousVariation.size())
						{
							int lMoveIndex = -1;
							for (int i = 0; i < lBoardValues.size(); i++)
							{
								if (lBoardValues[i].Move_ == aBestPreviousVariation[aCurrentDepth])
								{
									lMoveIndex = i;
									lBestPreviousVariationMoveFound = true;
									break;
								}
							}
							if (lMoveIndex >= 0)
							{
								auto lTemp = lBoardValues[0];
								lBoardValues[0] = lBoardValues[lMoveIndex];
								lBoardValues[lMoveIndex] = lTemp;
							}
							else
							{
								std::cout << "Best previous variation move not found! (depth " << aCurrentDepth << ")" << std::endl;
							}
						}

						// iterate over possible children board states

						bool lBreak = false;

						int lStartIndex = 0;

						// evaluate the first move sequentially before running the other moves in parallel
						{
							const int i = 0;
							VariationValue lChildEvaluation;

							const bool lGoDeeper1 = aCurrentDepth + 1 < aMaxDepth;
							const bool lGoDeeper2 = aGoDeeperOnCaptures && lBoardValues[i].Move_.IsCapture(lBoardValues[i].BoardBeforeMove);
							const bool lGoDeeper3 = lBoardValues[i].TerminalPositionType_ == TerminalPositionType::None;

							const bool lGoDeeper = (lGoDeeper1 || lGoDeeper2) && lGoDeeper3;
							const bool lOnlyCaptures = aCurrentDepth + 1 >= aMaxDepth || aOnlyCaptures;

							if (lGoDeeper)
							{
								const bool lIsOnBestPreviousVariation = aIsOnBestPreviousVariation && lBestPreviousVariationMoveFound
									&& i == 0 && aCurrentDepth + 1 < aBestPreviousVariation.size();

								TreeNode lChild;

								// set up the child node
								lChild.State = lBoardValues[i].Board;
								lChild.StateExtraInfo = lBoardValues[i].ExtraInfo;

								//std::vector<BoardState>* lGameHistoryPtr = &aGameHistory;

								// add current state to the game history
								aGameHistory.push_back(State);

								lChildEvaluation = lChild.Evaluate(aEvalFunc, lNextLevelMinMax, aCurrentDepth + 1,
									aMaxDepth,
									lBestValueWhite, lBestValueBlack,
									aGameHistory,
									aNodesEvaluated, aNodesVisited,
									aStartTime, aMaxTime, aTimedOut,
									aPointOfViewPlayer,
									aCacheData,
									aIsAlive,
									aDoMinimalWindowSearch,
									aBestPreviousVariation, lIsOnBestPreviousVariation, aIsSecondarySearch, 
									lOnlyCaptures, aGoDeeperOnCaptures);

								// remove the current state from the game history (return it back to the original state)
								aGameHistory.pop_back();
							}
							else
							{
								lChildEvaluation.Value = lBoardValues[i].Value;
							}

//#pragma omp critical
							{
								// if we found a child with a better value or we haven't assigned any child yet, set the best move to the new one
								if (lMinMaxSign * lChildEvaluation.Value < lMinMaxSign * lBestChildVariation.Value)
								{
									lBestChildVariation = lChildEvaluation;
									lBestMove = lBoardValues[i].Move_;
								}

								// check for timeout
								if (aTimedOut || (aMaxTime > 0 && aCurrentDepth < aMaxDepth - 1 && aStartTime.Elapsed() >= aMaxTime))
								{
									aTimedOut = true;
									lBreak = true;
								}

								// adjust the pruning thresholds by the new best move
								if (aMinMaxChoice == MinMax::Min)
								{
									lBestValueBlack = std::min(lBestChildVariation.Value, lBestValueBlack);
								}
								else
								{
									lBestValueWhite = std::max(lBestChildVariation.Value, lBestValueWhite);
								}

								// prune the rest of the tree if we found that this node is not good for either players
								if (i < lBoardValues.size() - 1 && lBestValueBlack <= lBestValueWhite)
								{
									lBreak = true;
								}
							}

							lStartIndex = 1;
						}

						//const bool lOMPSplit = !aIsSecondarySearch && (aCurrentDepth == 0);

						// evaluate the rest of the moves in parallel
//#pragma omp parallel for if(lOMPSplit) schedule(dynamic)
						for (int i = lStartIndex; i < lBoardValues.size(); i++)
						{
							if (lBreak) continue;

							int lNodesEvaluated = 0;
							int lNodesVisited = 0;

							TreeNode lChild;

							// set up the child node
							lChild.State = lBoardValues[i].Board;
							lChild.StateExtraInfo = lBoardValues[i].ExtraInfo;

							VariationValue lChildEvaluation;

							// if the child is not at the bottom depth and is not a terminal position (mate, stalemate, 3fold repetition),
							// call the evaluate on the child
							const bool lGoDeeper1 = aCurrentDepth + 1 < aMaxDepth;
							const bool lGoDeeper2 = aGoDeeperOnCaptures && lBoardValues[i].Move_.IsCapture(lBoardValues[i].BoardBeforeMove);
							const bool lGoDeeper3 = lBoardValues[i].TerminalPositionType_ == TerminalPositionType::None;

							const bool lGoDeeper = (lGoDeeper1 || lGoDeeper2) && lGoDeeper3;

							const bool lOnlyCaptures = aCurrentDepth + 1 >= aMaxDepth || aOnlyCaptures;

							if (lGoDeeper)
							{
								const bool lIsOnBestPreviousVariation = aIsOnBestPreviousVariation && lBestPreviousVariationMoveFound
									&& i == 0 && aCurrentDepth + 1 < aBestPreviousVariation.size();

								// minimal window search

								if (aDoMinimalWindowSearch)
								{
									short lBestValueWhiteTmp = lBestValueWhite;
									short lBestValueBlackTmp = lBestValueBlack;

									if (aMinMaxChoice == MinMax::Min)
									{
										lBestValueWhiteTmp = lBestValueBlackTmp - 1;
									}
									else
									{
										lBestValueBlackTmp = lBestValueWhiteTmp + 1;
									}

									aGameHistory.push_back(State);

									lChildEvaluation = lChild.Evaluate(aEvalFunc, lNextLevelMinMax, aCurrentDepth + 1,
										aMaxDepth,
										lBestValueWhiteTmp, lBestValueBlackTmp,
										aGameHistory,
										lNodesEvaluated, lNodesVisited,
										aStartTime, aMaxTime, aTimedOut,
										aPointOfViewPlayer,
										aCacheData,
										aIsAlive,
										false,
										aBestPreviousVariation, lIsOnBestPreviousVariation, aIsSecondarySearch, 
										lOnlyCaptures, aGoDeeperOnCaptures);

									aGameHistory.pop_back();
								}

								if (!aDoMinimalWindowSearch
									||
									(lMinMaxSign * lChildEvaluation.Value < lMinMaxSign * lBestChildVariation.Value 
									&& 
									lBestValueWhite < lChildEvaluation.Value && lChildEvaluation.Value < lBestValueBlack))
								{
									// reevaluate normally
									// add current state to the game history

									aGameHistory.push_back(State);

									lChildEvaluation = lChild.Evaluate(aEvalFunc, lNextLevelMinMax, aCurrentDepth + 1,
										aMaxDepth,
										lBestValueWhite, lBestValueBlack,
										aGameHistory,
										lNodesEvaluated, lNodesVisited,
										aStartTime, aMaxTime, aTimedOut,
										aPointOfViewPlayer,
										aCacheData,
										aIsAlive, 
										aDoMinimalWindowSearch,
										aBestPreviousVariation, lIsOnBestPreviousVariation, aIsSecondarySearch, 
										lOnlyCaptures, aGoDeeperOnCaptures);

									// remove the current state from the game history (return it back to the original state)
									aGameHistory.pop_back();
								}
							}
							// otherwise just use the static evaluation of the child
							else
							{
								lChildEvaluation.Value = lBoardValues[i].Value;
							}
//#pragma omp critical
							{
								aNodesEvaluated += lNodesEvaluated;
								aNodesVisited += lNodesVisited;

								// if we found a child with a better value or we haven't assigned any child yet, set the best move to the new one
								if (lMinMaxSign * lChildEvaluation.Value < lMinMaxSign * lBestChildVariation.Value)
								{
									lBestChildVariation = lChildEvaluation;
									lBestMove = lBoardValues[i].Move_;
								}

								// check for timeout
								if (aTimedOut || (aMaxTime > 0 && aCurrentDepth < aMaxDepth - 1 && aStartTime.Elapsed() >= aMaxTime))
								{
									aTimedOut = true;
									lBreak = true;
								}

								// adjust the pruning thresholds by the new best move
								if (aMinMaxChoice == MinMax::Min)
								{
									lBestValueBlack = std::min(lBestChildVariation.Value, lBestValueBlack);
								}
								else
								{
									lBestValueWhite = std::max(lBestChildVariation.Value, lBestValueWhite);
								}

								// prune the rest of the tree if we found that this node is not good for either players
								if (i < lBoardValues.size() - 1 && lBestValueBlack <= lBestValueWhite)
								{
									lBreak = true;
								}
							}
						}

						// add the best found move to the variation of the best child
						if (!aIsSecondarySearch)
							lBestChildVariation.VariationMoves.insert(lBestChildVariation.VariationMoves.begin(), lBestMove);

						// set the best variation as the return variation
						lReturnVariation = lBestChildVariation;

						if (aCacheData.UseInnerTreeCache)
						{
							if (!lCacheItem->IsSet || lCacheItem->Depth > aCurrentDepth)
							{
								lCacheItem->IsSet = true;
								lCacheItem->Board = this->State;
								lCacheItem->Depth = aCurrentDepth;
								lCacheItem->Value = lReturnVariation.Value;

								if ((lReturnVariation.Value <= lBestValueBlack && aMinMaxChoice == MinMax::Min)
									||
									(lReturnVariation.Value >= lBestValueWhite && aMinMaxChoice == MinMax::Max))
								{
									lCacheItem->ValueType_ = ValueType::ThisColourLimit;
								}
								else if (
									(lReturnVariation.Value <= lBestValueBlack && aMinMaxChoice == MinMax::Max)
									||
									(lReturnVariation.Value >= lBestValueWhite && aMinMaxChoice == MinMax::Min))
								{
									lCacheItem->ValueType_ = ValueType::OppositeColourLimit;
								}
								else
								{
									lCacheItem->ValueType_ = ValueType::Equal;
								}
							}
						}
					}
				//}

				// leaf node
				// the code probably shouldn't get here much, the leaf nodes should be detected one level above already, but just in case something is missed
				if (!lChildrenEvaluated)
				{
					// we need to tell the function to evaluate whether the position is terminal or not itself by setting nullopt
					TerminalPositionType lDummy;
					lReturnVariation.Value = aEvalFunc(State, StateExtraInfo, aCurrentDepth, aGameHistory, lDummy, false, aPointOfViewPlayer);
				}
				return lReturnVariation;
			}
		};

	protected:
		const int cDepth;
		// max time allowed for computation (seconds)
		const double cMaxTime;

		virtual short EvaluatePositionInner(const BoardState& aBoard, const int aDepth) = 0;
		virtual void Reset() override
		{
			ClearCache();
		}

	public:
		void SetOnlyMoveShortcut(const bool aValue) { mOnlyMoveShortcut = aValue; }
		int GetDepth() const { return cDepth; }
		// just a public exposure of the EvaluatePosition function
		short StaticEvaluation(const BoardState& aBoard, const std::vector<BoardState>& aGameHistory)
		{
			TerminalPositionType lDummy;
			const short lResult = EvaluatePosition(aBoard, std::nullopt, 1, aGameHistory, lDummy, false, aBoard.PlayerToMove);
			return lResult;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="aBoard"></param>
		/// <param name="aGameHistory"></param>
		/// <param name="aDepth">Optional depth modifying parameter. Leave default (-1) to use the constructor depth.</param>
		/// <returns></returns>
		VariationValue TreeAnalysis(const BoardState& aBoard, const std::vector<BoardState>& aGameHistory, const Colour aPointOfViewPlayer, const int aDepth = -1)
		{
			TreeNode lRoot;
			lRoot.State = aBoard;

			int lDummy1 = 0;
			int lDummy2 = 0;
			Time lTime;
			lTime.Start();
			bool lTimedOut = false;
			CacheData lEmptyCache;
			std::atomic<bool> lIsAlive = true;

			std::vector<BoardState> lGameHistoryCopy(aGameHistory);

			const int lUsedDepth = (aDepth > 0) ? aDepth : cDepth;

			const VariationValue lResult = lRoot.Evaluate(mEvalFunc, (aBoard.PlayerToMove & C_WHITE) ? MinMax::Max : MinMax::Min, 0, lUsedDepth,
				std::numeric_limits<short>::lowest(), std::numeric_limits<short>::max(),
				lGameHistoryCopy, lDummy1, lDummy2, lTime, cMaxTime * 1000, lTimedOut, aPointOfViewPlayer, lEmptyCache, &lIsAlive, cDoMinimalWindowSearch, 
				std::vector<Move>(), false, false, false, cGoDeeperOnCapture);

			return lResult;
		}

	private:

		static inline TerminalPositionType GetTerminalPositionType(const BoardState& aBoard, const std::vector<BoardState>& aGameHistory, const std::optional<BoardStateExtra>& aBoardExtraInfo)
		{
			bool lKnownCheck = false;
			bool lKnownNotCheck = false;

			if (aBoardExtraInfo)
			{
				if (aBoardExtraInfo->IsMovingPlayerInCheck) lKnownCheck = true;
				else lKnownNotCheck = true;
			}

			if (!lKnownCheck)
			{
				if (aBoard.IsStalemate(lKnownNotCheck))
				{
					return TerminalPositionType::Stalemate;
				}
			}
			if (!lKnownNotCheck)
			{
				if (aBoard.IsMate(lKnownCheck))
				{
					if (aBoard.PlayerToMove & C_WHITE)
					{
						return TerminalPositionType::WhiteCheckmated;
					}
					else
					{
						return TerminalPositionType::BlackCheckmated;
					}
				}
			}

			// check threefold repetition
			if (IsThreefoldRepetition(aBoard, aGameHistory, 40))
			{
				return TerminalPositionType::Stalemate;
			}

			return TerminalPositionType::None;
		}

		// white advantage is positive, black advantage is negative
		// the move argument indicates move that was leading to the given board (last move)
		// the last bool flag indicates whether the previous argument - terminal position type - has already been set from outside or 
		// needs to be evaluated here and returned
		// point of view player - from which perpective the position is evaluated
		// this will change values for a stalemate for instance
		// it is not a player who is made to move on this board!
		// in the tree search, it would be the player to move on the root board of the tree
		short EvaluatePosition(const BoardState& aBoard, const std::optional<BoardStateExtra>& aBoardExtraInfo,
			const int aDepth,
			const std::vector<BoardState>& aGameHistory,
			TerminalPositionType& aTerminalPosition, const bool aIsTerminalPositionAlreadySet, const Colour aPointOfViewPlayer)
		{
			if (!aIsTerminalPositionAlreadySet)
			{
				aTerminalPosition = GetTerminalPositionType(aBoard, aGameHistory, aBoardExtraInfo);
			}

			if (aTerminalPosition == TerminalPositionType::WhiteCheckmated)
				return -cCheckmateInZeroWhite + aDepth; // white is to move and is checkmated
			else if (aTerminalPosition == TerminalPositionType::BlackCheckmated)
				return cCheckmateInZeroWhite - aDepth; // black is to move and is checkmated
			else if (aTerminalPosition == TerminalPositionType::Stalemate)
			{
				if (aPointOfViewPlayer == C_WHITE)
				{
					return cStalemateBias;
				}
				else
				{
					return -cStalemateBias;
				}
			}

			// avoid too large values (like 1e200) (same for negative), those will be used for checkmates (in various number of moves)
			return EvaluatePositionInner(aBoard, aDepth);
		}

		virtual MoveWithNotes PrepareMoveInner(const BoardState& aBoard, 
			const std::vector<BoardState>& aGameHistory, const std::vector<Move>& aMoveHistory, const std::atomic<bool>* const aIsAlive) override
		{
			if (!(*aIsAlive))
			{
				// quick end, the system is telling us that this evaluation is no longer relevant (new game started or something)
				MoveWithNotes lDummy;
				return lDummy;
			}

			// check if there is only one legal move
			if (mOnlyMoveShortcut)
			{
				StaticList<Move, PieceMovement::cMaxLegalMoves> lOnlyMoveCheck;
				PieceMovement::GetAllLegalMoves(aBoard, lOnlyMoveCheck);
				if (lOnlyMoveCheck.ActualSize == 1)
				{
					MoveWithNotes lOnlyMove;
					lOnlyMove.Notes = "only move";
					lOnlyMove.Move_ = lOnlyMoveCheck.Items[0];
					return lOnlyMove;
				}
			}

			//const int lDepth = cDepth;

			std::vector<BoardState> lGameHistoryCopy(aGameHistory);
			// remove the last element, that is the current game state
			//lGameHistoryCopy.pop_back();

			//std::vector<std::vector<TreeNode>> lLayers;
			std::vector<Move> lFirstLayerMoves;
			//lLayers.reserve(lDepth);

			TreeNode lRoot;
			lRoot.State = aBoard;

			Time lRootEvalTime;
			lRootEvalTime.Start();

			int lNodesEvaluated = 0;
			int lNodesVisited = 0;

			VariationValue lBestVariation;

			const int lStartDepth = std::min(((cDepth + 1) % 2) + 3, cDepth);
			const int lDepthStep = 2;

			//const int lStartDepth = std::min(4, cDepth);
			//const int lDepthStep = 1;

			Time lComputationTime;
			lComputationTime.Start();

			if (cUseInnerTreeCache)
			{
				// clear inner tree cache before any evaluations
				for (int i = 0; i < cInnerTreeCacheSize; i++)
				{
					mInnerTreeCache[i].IsSet = false;
					mInnerTreeCache[i].Value = 0;
					mInnerTreeCache[i].ValueType_ = ValueType::Equal;
					mInnerTreeCache[i].Depth = -1;
				}
			}

			CacheData lCacheData;
			lCacheData.LeafCache = mLeafCache;
			lCacheData.UseLeafCache = cUseLeafCache;
			lCacheData.LeafCacheSize = cLeafCacheSize;

			lCacheData.InnerTreeCache = mInnerTreeCache;
			lCacheData.UseInnerTreeCache = cUseInnerTreeCache;
			lCacheData.InnerTreeCacheSize = cInnerTreeCacheSize;

			lCacheData.InnerTreeCacheUse = 0;
			lCacheData.InnerTreeCacheFind = 0;
			lCacheData.InnerTreeCacheImprove = 0;

			std::cout << "Evaluating depth: ";

			for (int iDepth = lStartDepth; iDepth <= cDepth; iDepth += lDepthStep)
			{
				//std::cout << "Depth: " << iDepth << std::endl;
				if (!(*aIsAlive))
				{
					// quick end, the system is telling us that this evaluation is no longer relevant (new game started or something)
					MoveWithNotes lDummy;
					return lDummy;
				}

				if (cUseInnerTreeCache && iDepth > lStartDepth)
				{
					// clear inner tree cache
					// or just move everything up in depth
					for (int i = 0; i < cInnerTreeCacheSize; i++)
					{
						//mInnerTreeCache[i].IsSet = false;
						//mInnerTreeCache[i].Value = 0;
						//mInnerTreeCache[i].ValueType_ = ValueType::Equal;
						//mInnerTreeCache[i].Depth = -1;
						if (mInnerTreeCache[i].IsSet)
							mInnerTreeCache[i].Depth += lDepthStep;
					}
				}

				bool lTimedOut = false;

				if (cMaxTime > 0 && iDepth > lStartDepth)
				{
					// percentage of time that if we cross we don't need to run another depth evaluation, since it will
					// most likely not be able to finish on time, given how much is left (deeper searches consume much more time, so if 
					// we already spent so much time on shorter ones, the deeper one won't finish for sure)
					const double lStopCoeff = 0.7;
					lTimedOut = (lComputationTime.Elapsed() >= lStopCoeff * cMaxTime * 1000);
				}
				if (lTimedOut) break;

				double lMaxTime = cMaxTime * 1000;
				// the starting depth should always be searched no matter what, so we have at least something
				if (iDepth == lStartDepth)
				{
					lMaxTime = std::numeric_limits<double>::max();
				}

				const int lPrevNodesEvaluated = lNodesEvaluated;
				const int lPrevNodesVisited = lNodesVisited;

				if (iDepth > lStartDepth) std::cout << ", ";
				std::cout << iDepth;

				const short lSearchWindowRadius = 51;
				short lLimitWhite = std::numeric_limits<short>::lowest();
				short lLimitBlack = std::numeric_limits<short>::max();

				if (iDepth > lStartDepth)
				{
					lLimitWhite = lBestVariation.Value - lSearchWindowRadius;
					lLimitBlack = lBestVariation.Value + lSearchWindowRadius;
				}

				VariationValue lNewBestVariation = lRoot.Evaluate(mEvalFunc,
					(aBoard.PlayerToMove & C_WHITE) ? MinMax::Max : MinMax::Min,
					0, iDepth,
					lLimitWhite, lLimitBlack,
					lGameHistoryCopy,
					lNodesEvaluated,
					lNodesVisited,
					lComputationTime, lMaxTime, lTimedOut,
					aBoard.PlayerToMove,
					lCacheData,
					aIsAlive,
					cDoMinimalWindowSearch,
					lBestVariation.VariationMoves, true, false, false, cGoDeeperOnCapture);

				//std::cout << "(LW:" << lLimitWhite << ")(V:" << lNewBestVariation.Value << ")(LB:" << lLimitBlack << ")" << std::endl;

				while (!lTimedOut &&
					(lNewBestVariation.Value <= lLimitWhite || lNewBestVariation.Value >= lLimitBlack))
				{
					if (lNewBestVariation.Value <= lLimitWhite)
					{
						std::cout << "(rw)";
						lLimitBlack = lNewBestVariation.Value + 1;
						lLimitWhite = std::min(lLimitBlack - 1 - 300, lLimitWhite - 300);
					}
					else if (lNewBestVariation.Value >= lLimitBlack)
					{
						std::cout << "(rb)";
						lLimitWhite = lNewBestVariation.Value - 1;
						lLimitBlack = std::max(lLimitWhite + 1 + 300, lLimitBlack + 300);
					}

					lNewBestVariation = lRoot.Evaluate(mEvalFunc,
						(aBoard.PlayerToMove & C_WHITE) ? MinMax::Max : MinMax::Min,
						0, iDepth,
						lLimitWhite, lLimitBlack,
						lGameHistoryCopy,
						lNodesEvaluated,
						lNodesVisited,
						lComputationTime, lMaxTime, lTimedOut,
						aBoard.PlayerToMove,
						lCacheData,
						aIsAlive,
						cDoMinimalWindowSearch,
						lBestVariation.VariationMoves, true, false, false, cGoDeeperOnCapture);

					//std::cout << "(LW:" << lLimitWhite << ")(V:" << lNewBestVariation.Value << ")(LB:" << lLimitBlack << ")" << std::endl;
				}

				if (!lTimedOut || iDepth == lStartDepth)
				{
					lBestVariation = lNewBestVariation;
				}
				else break;

				const bool lCheckmateFound = std::abs(lBestVariation.Value) >= cCheckmateThreshold;
				
				if (lCheckmateFound) break;

			}
			std::cout << std::endl;

			if (cUseInnerTreeCache)
			{
				std::cout << "Inner tree cache find ratio: "
					<< (double)lCacheData.InnerTreeCacheFind / lCacheData.InnerTreeCacheUse * 100 << "% (out of " <<
					lCacheData.InnerTreeCacheUse << ")" << std::endl;

				std::cout << "Inner tree cache improve ratio: "
					<< (double)lCacheData.InnerTreeCacheImprove / lCacheData.InnerTreeCacheUse * 100 << "% (out of " <<
					lCacheData.InnerTreeCacheUse << ")" << std::endl;

				int lSetCount = 0;
				for (int i = 0; i < cInnerTreeCacheSize; i++)
					if (mInnerTreeCache[i].IsSet) lSetCount++;

				std::cout << "Inner tree cache filled portion: " << (double)lSetCount / cInnerTreeCacheSize * 100 << "%" << std::endl;
			}


			const double lRootEvalElapsed = lRootEvalTime.Stop();

			std::cout << "Total tree evaluation time: " << lRootEvalElapsed << " ms" << std::endl;
			std::cout << "Nodes evaluated total: " << lNodesEvaluated << std::endl;
			std::cout << "Nodes visited total: " << lNodesVisited << std::endl;
			//std::cout << "kN/s: " << (double)lNodesVisited / lRootEvalElapsed << std::endl;

			int lLeafCacheFilledCount = 0;
			if (cUseLeafCache)
			{
				for (int i = 0; i < cLeafCacheSize; i++)
				{
					if (mLeafCache[i].IsSet) lLeafCacheFilledCount++;
				}
			}

			//std::cout << "Leaf cache filled ratio: " << (double)lLeafCacheFilledCount / cLeafCacheSize << std::endl;

			// better to do the value through stringstream to format it better (to_string can make a mess with large numbers)
			std::stringstream lValueString;
			lValueString << (double)lBestVariation.Value / 100;

			BoardState lTempBoardCopy(aBoard);
			std::cout << "Computed variation:" << std::endl;
			lBestVariation.PrintVariation(aBoard);
			std::cout << "Value: " << lValueString.str() << std::endl;

			lTempBoardCopy = aBoard;
			MoveWithNotes lReturnMove = lBestVariation.VariationMoves[0];


			lReturnMove.Notes += "score: ";
			lReturnMove.Notes += lValueString.str() + " (";
			for (int i = 0; i < lBestVariation.VariationMoves.size(); i++)
			{
				const Move& lMove = lBestVariation.VariationMoves[i];
				lReturnMove.Notes += MoveToPGN(lBestVariation.VariationMoves[i], lTempBoardCopy);
				if (i < lBestVariation.VariationMoves.size() - 1) lReturnMove.Notes += " ";
				lTempBoardCopy = lTempBoardCopy.MakeMove(lMove);
			}
			lReturnMove.Notes += ")";
			lReturnMove.Notes += ", eval. time: " + std::to_string(lRootEvalElapsed) + " ms";

			return lReturnMove;
		}

		void ClearCache()
		{
			//std::cout << "Clearing cache" << std::endl;
			if (cUseLeafCache)
			{
				for (int i = 0; i < cLeafCacheSize; i++)
				{
					mLeafCache[i].IsSet = false;
				}
			}
			if (cUseInnerTreeCache)
			{
				for (int i = 0; i < cInnerTreeCacheSize; i++)
				{
					mInnerTreeCache[i].IsSet = false;
				}
			}
		}

	public:
		/// <summary>
		/// 
		/// </summary>
		/// <param name="aDepth"></param>
		/// <param name="aMaxTime">In seconds</param>
		/// <param name="aLeafCacheSize">In MB</param>
		/// <param name="aInnerTreeCacheSize">In MB</param>
		TreeSearch(const int aDepth, const double aMaxTime = -1, 
			const size_t aLeafCacheSize = 0, const size_t aInnerTreeCacheSize = 0, 
			const bool aDoMinimalWindowSearch = false,
			const bool aGoDeeperOnCapture = false) :
			cDepth(aDepth), cMaxTime(aMaxTime), 
			cLeafCacheSize((aLeafCacheSize * 1024 * 1024) / sizeof(LeafCacheItem)), cUseLeafCache(aLeafCacheSize > 0),
			cInnerTreeCacheSize((aInnerTreeCacheSize * 1024 * 1024) / sizeof(InnerTreeCacheItem)), cUseInnerTreeCache(aInnerTreeCacheSize > 0),
			cDoMinimalWindowSearch(aDoMinimalWindowSearch), cGoDeeperOnCapture(aGoDeeperOnCapture)
		{
			std::cout << "Leaf cache size: " << cLeafCacheSize << std::endl;
			if (cDepth < 1) throw EXCEPTION_MEMBER("Depth must be at least 1!");

			if (cUseLeafCache)
			{
				mLeafCache = new LeafCacheItem[cLeafCacheSize];
				for (int i = 0; i < cLeafCacheSize; i++)
				{
					mLeafCache[i].IsSet = false;
				}
			}

			if (cUseInnerTreeCache)
			{
				mInnerTreeCache = new InnerTreeCacheItem[cInnerTreeCacheSize];
				for (int i = 0; i < cInnerTreeCacheSize; i++)
				{
					mInnerTreeCache[i].IsSet = false;
				}
			}
			ClearCache();
		}
		virtual ~TreeSearch()
		{
			if (mLeafCache != nullptr)
				delete[] mLeafCache;

			if (mInnerTreeCache != nullptr)
				delete[] mInnerTreeCache;
		}
	};
}

