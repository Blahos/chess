
namespace Players
{
	class KingHunter;
}

#pragma once

#include <limits>
#include <cmath>

#include "TreeSearch.h"
#include "../FunctionsMove.h"

namespace Players
{
	class KingHunter : public TreeSearch
	{
	private:
		//thread_local std::vector<BoardCoordinates> mWhiteRookBuffer;
		//thread_local std::vector<BoardCoordinates> mBlackRookBuffer;

	public:
		/// <summary>
		/// 
		/// </summary>
		/// <param name="aDepth"></param>
		/// <param name="aMaxTime">In seconds</param>
		/// <param name="aLeafCacheSize">In MB</param>
		/// <param name="aInnerTreeCacheSize">In MB</param>
		KingHunter(const int aDepth, const double aMaxTime = -1, 
			const size_t aLeafCacheSize = 0, const size_t aInnerTreeCacheSize = 0,
			const bool aDoMinimalWindowSearch = false,
			const bool aGoDeeperOnCapture = false)
			: TreeSearch(aDepth, aMaxTime, aLeafCacheSize, aInnerTreeCacheSize, aDoMinimalWindowSearch, aGoDeeperOnCapture) { }

	protected:
		virtual short EvaluatePositionInner(const BoardState& aBoard, const int aDepth) override
		{
			const GamePhase lGamePhase = aBoard.ComputeGamePhase();

			double lReturnValue = 0;

			int lBlackPawnCount = 0;
			int lWhitePawnCount = 0;

			// save locations of rooks
			// note: we are hoping that noone will ever have more than 6 rooks at any given position
			StaticList<BoardCoordinates, 6> lWhiteRookBuffer;
			StaticList<BoardCoordinates, 6> lBlackRookBuffer;

			const double lEndgameExtraRookValue = 0.02;

			const int lWhiteKingSquareInd = CoToSI(aBoard.WhiteKing);
			const int lBlackKingSquareInd = CoToSI(aBoard.BlackKing);

			const double lXRayCoeff = 0.01;

			const double lPawnInCenterCoeff = 0.04;
			const double lPawnInTightCenterCoeff = 0.01;

			const double lDevelopedPieceCoeff = 0.04;

			const double lPawnPushCoeff = 0.01;
			const double lPawnPushCoeffEngame = 0.1;

			const double lPassedPawnScore = 0.05;
			const double lRookOrQueenBehindPassedPawnScore = 0.02;
			const double lPawnProtectedPassedPawnScore = 0.02;

			// count pawns
			for (int iSquare = 0; iSquare < BoardSetup::cSquareCount; iSquare++)
			{
				if (aBoard.Squares[iSquare].Piece_ & P_PAWN & C_WHITE) lWhitePawnCount++;
				if (aBoard.Squares[iSquare].Piece_ & P_PAWN & C_BLACK) lBlackPawnCount++;
			}

			for (int iSquare = 0; iSquare < BoardSetup::cSquareCount; iSquare++)
			{
				// piece values
				const SquareState& lSquare = aBoard.Squares[iSquare];
				if (!lSquare.Piece_) continue;

				const Piece& lPiece = lSquare.Piece_;
				const char& lRow = lSquare.Coordinates.Row;
				const char& lCol = lSquare.Coordinates.Col;

				const int lColourSign = (lPiece & C_WHITE) ? 1 : -1;

				if (lPiece & P_PAWN) lReturnValue += lColourSign * 1;
				else if (lPiece & P_KNIGHT) lReturnValue += lColourSign * 3;
				else if (lPiece & P_BISHOP) lReturnValue += lColourSign * 3;
				else if (lPiece & P_ROOK)
				{
					lReturnValue += lColourSign * 5;
					if (lPiece & C_WHITE)
					{
						lWhiteRookBuffer.Items[lWhiteRookBuffer.ActualSize] = lSquare.Coordinates;
						lWhiteRookBuffer.ActualSize++;
					}
					if (lPiece & C_BLACK)
					{
						lBlackRookBuffer.Items[lBlackRookBuffer.ActualSize] = lSquare.Coordinates;
						lBlackRookBuffer.ActualSize++;
					}
				}
				else if (lPiece & P_QUEEN) lReturnValue += lColourSign * 9;

				// extra value for a rook in endgame
				if (lPiece & P_ROOK)
				{
					lReturnValue += lColourSign * lEndgameExtraRookValue * lGamePhase.Endgame;
				}

				// X rays on king (in middle and end game)
				if (lPiece & C_WHITE)
				{
					const int lXRayValue = IsXRay(aBoard, iSquare, lBlackKingSquareInd);
					if (lXRayValue >= 0)
					{
						lReturnValue += lXRayCoeff * (1 - lGamePhase.Opening);
					}
				}
				else
				{
					const int lXRayValue = IsXRay(aBoard, iSquare, lWhiteKingSquareInd);
					if (lXRayValue >= 0)
					{
						lReturnValue -= lXRayCoeff * (1 - lGamePhase.Opening);
					}
				}

				// panws in the center
				if (lRow >= 2 && lRow <= 5 &&
					lCol >= 2 && lCol <= 5)
				{
					if (lPiece & P_PAWN)
					{
						lReturnValue += lColourSign * lPawnInCenterCoeff * lGamePhase.Opening;

						if (lRow >= 3 && lRow <= 4 && lCol >= 3 && lCol <= 4)
							lReturnValue += lColourSign * lPawnInTightCenterCoeff * lGamePhase.Opening;
					}
				}

				// developed minor pieces
				if (lRow >= 1 && lRow <= 6)
				{
					if (lPiece & (P_KNIGHT | P_BISHOP))
					{
						lReturnValue += lColourSign * lDevelopedPieceCoeff * (1 - lGamePhase.Endgame);
					}
				}

				// push pawns
				// full value means all pawns that this side has are on the 7th rank (relative to that side), so just before promotion
				if (lPiece & P_PAWN)
				{
					int lPawnCount;
					if (lPiece & C_WHITE)
					{
						lPawnCount = lWhitePawnCount;
					}
					else
					{
						lPawnCount = lBlackPawnCount;
					}

					const int lRelRow = GetRelativeRow(lSquare.Coordinates, lSquare.Piece_);
					lReturnValue += lColourSign * (double)(lRelRow - 1) / 5 / lPawnCount * lPawnPushCoeff;
					lReturnValue += lColourSign * (double)(lRelRow - 1) / 5 / lPawnCount * lPawnPushCoeffEngame * lGamePhase.Endgame;
				}

				// passed pawns in endgame
				if (lPiece & P_PAWN)
				{
					const int lForwardDir = GetForwardRowDirection(lSquare.Piece_);

					const Colour lMyColour = PIECE_COLOUR(lSquare.Piece_);
					const Colour lOppositeColour = GetOppositeColour(lSquare.Piece_);

					bool lIsPassedPawn = true;

					// check this rank
					for (int iRow = lSquare.Coordinates.Row + lForwardDir; iRow >= 0 && iRow < BoardSetup::cRows; iRow += lForwardDir)
					{
						const char lIndex = CoToSI(iRow, lSquare.Coordinates.Col);

						// in case of a doubled pawn, the pawn in the back is not considered passed
						if (aBoard.Squares[lIndex].Piece_ & P_PAWN)
						{
							lIsPassedPawn = false;
							break;
						}
					}

					// check neighbour ranks
					if (lSquare.Coordinates.Col > 0)
					{
						for (int iRow = lSquare.Coordinates.Row + lForwardDir; iRow >= 0 && iRow < BoardSetup::cRows; iRow += lForwardDir)
						{
							const char lIndex = CoToSI(iRow, lSquare.Coordinates.Col - 1);

							if (aBoard.Squares[lIndex].Piece_ & lOppositeColour & P_PAWN)
							{
								lIsPassedPawn = false;
								break;
							}
						}
					}
					if (lIsPassedPawn && lSquare.Coordinates.Col < BoardSetup::cCols - 1)
					{
						for (int iRow = lSquare.Coordinates.Row + lForwardDir; iRow >= 0 && iRow < BoardSetup::cRows; iRow += lForwardDir)
						{
							const char lIndex = CoToSI(iRow, lSquare.Coordinates.Col + 1);

							if (aBoard.Squares[lIndex].Piece_ & lOppositeColour & P_PAWN)
							{
								lIsPassedPawn = false;
								break;
							}
						}
					}

					if (lIsPassedPawn)
					{
						lReturnValue += lPassedPawnScore * lColourSign * lGamePhase.Endgame;

						// check rook or queen behind the passed pawn
						bool lIsSupported = false;
						for (int iRow = lSquare.Coordinates.Row - lForwardDir; iRow >= 0 && iRow < BoardSetup::cRows; iRow -= lForwardDir)
						{
							const char lIndex = CoToSI(iRow, lSquare.Coordinates.Col);

							if (!aBoard.Squares[lIndex].Piece_) continue;

							if (aBoard.Squares[lIndex].Piece_ & lMyColour & (P_ROOK | P_QUEEN))
							{
								lIsSupported = true;
								break;
							}
							else
							{
								break;
							}
						}

						if (lIsSupported)
						{
							lReturnValue += lRookOrQueenBehindPassedPawnScore * lColourSign * lGamePhase.Endgame;
						}

						// check if it's protected by another pawn
						bool lIsProtected = false;
						const BoardCoordinates lProt1(lSquare.Coordinates.Row - lForwardDir, lSquare.Coordinates.Col - 1);
						const BoardCoordinates lProt2(lSquare.Coordinates.Row - lForwardDir, lSquare.Coordinates.Col + 1);

						if (IsIn(lProt1))
						{
							const SquareState& lProtSquare = aBoard.Squares[CoToSI(lProt1)];
							if (lProtSquare.Piece_ == lSquare.Piece_)
							{
								lIsProtected = true;
							}
						}
						if (!lIsProtected && IsIn(lProt2))
						{
							const SquareState& lProtSquare = aBoard.Squares[CoToSI(lProt2)];
							if (lProtSquare.Piece_ == lSquare.Piece_)
							{
								lIsProtected = true;
							}
						}

						if (lIsProtected)
						{
							lReturnValue += lPawnProtectedPassedPawnScore * lColourSign * lGamePhase.Endgame;
						}
					}
				}
			}

			// doubled (or more) pawns and isolated pawns
			const double lMultiplePawnPenaltyCoeff = 0.01;
			const double lIsolatedPawnPenaltyCoeff = 0.01;

			unsigned int lWhitePawnPerColCount[BoardSetup::cCols];
			unsigned int lBlackPawnPerColCount[BoardSetup::cCols];

			// doubled pawns
			for (int iCol = 0; iCol < BoardSetup::cCols; iCol++)
			{
				lWhitePawnPerColCount[iCol] = 0;
				lBlackPawnPerColCount[iCol] = 0;

				for (int iRow = 0; iRow < BoardSetup::cRows; iRow++)
				{
					const int lSquareIndex = CoToSI(iRow, iCol);
					const SquareState& lSquare = aBoard.Squares[lSquareIndex];

					if (lSquare.Piece_ & P_PAWN_WHITE) lWhitePawnPerColCount[iCol]++;
					if (lSquare.Piece_ & P_PAWN_BLACK) lBlackPawnPerColCount[iCol]++;
				}

				if (lWhitePawnPerColCount[iCol] > 1)
				{
					lReturnValue -= lMultiplePawnPenaltyCoeff;
				}
				if (lBlackPawnPerColCount[iCol] > 1)
				{
					lReturnValue += lMultiplePawnPenaltyCoeff;
				}
			}

			// isolated pawns
			for (int iCol = 0; iCol < BoardSetup::cCols; iCol++)
			{
				if (lWhitePawnPerColCount[iCol] >= 1)
				{
					bool lIsIsolated = true;
					if (iCol > 0 && lWhitePawnPerColCount[iCol - 1] > 0) lIsIsolated = false;
					if (iCol < BoardSetup::cCols - 1 && lWhitePawnPerColCount[iCol + 1] > 0) lIsIsolated = false;

					if (lIsIsolated)
					{
						lReturnValue -= lIsolatedPawnPenaltyCoeff;
					}
				}

				if (lBlackPawnPerColCount[iCol] >= 1)
				{
					bool lIsIsolated = true;
					if (iCol > 0 && lBlackPawnPerColCount[iCol - 1] > 0) lIsIsolated = false;
					if (iCol < BoardSetup::cCols - 1 && lBlackPawnPerColCount[iCol + 1] > 0) lIsIsolated = false;

					if (lIsIsolated)
					{
						lReturnValue += lIsolatedPawnPenaltyCoeff;
					}
				}
			}

			// king safety in opening and middle game
			const double lKingSafetyPawnCoeff = 0.01;
			const double lKingSafetyCornerCoeff = 0.02;

			const char lRowInFrontOfTheWhiteKing = aBoard.WhiteKing.Row + 1;
			const char lRowInFrontOfTheBlackKing = aBoard.BlackKing.Row - 1;

			// pawns in front of the king
			for (int iCol = -1; iCol <= 1; iCol++)
			{
				const BoardCoordinates lSquareInFrontOfTheWhiteKing(lRowInFrontOfTheWhiteKing, aBoard.WhiteKing.Col + iCol);
				if (IsIn(lSquareInFrontOfTheWhiteKing))
				{
					if (aBoard.Squares[CoToSI(lSquareInFrontOfTheWhiteKing)].Piece_ & P_PAWN & C_WHITE)
					{
						lReturnValue += lKingSafetyPawnCoeff * (1 - lGamePhase.Endgame);
					}
				}

				const BoardCoordinates lSquareInFrontOfTheBlackKing(lRowInFrontOfTheBlackKing, aBoard.BlackKing.Col + iCol);
				if (IsIn(lSquareInFrontOfTheBlackKing))
				{
					if (aBoard.Squares[CoToSI(lSquareInFrontOfTheBlackKing)].Piece_ & P_PAWN & C_BLACK)
					{
						lReturnValue -= lKingSafetyPawnCoeff * (1 - lGamePhase.Endgame);
					}
				}
			}

			// king in the cornerin opening and middle game
			// taxicab metric
			const int lWhiteKingCornerDist = aBoard.WhiteKing.Row + std::min(aBoard.WhiteKing.Col, (char)(7 - aBoard.WhiteKing.Col));
			const int lBlackKingCornerDist = (char)(7 - aBoard.BlackKing.Row) + std::min(aBoard.BlackKing.Col, (char)(7 - aBoard.BlackKing.Col));

			lReturnValue += (double)(10 - lWhiteKingCornerDist) / 10 * lKingSafetyCornerCoeff * (1 - lGamePhase.Endgame);
			lReturnValue -= (double)(10 - lBlackKingCornerDist) / 10 * lKingSafetyCornerCoeff * (1 - lGamePhase.Endgame);

			// connected rooks in middle game and endgame
			const double lConnectedRooksCoeff = 0.01;

			// if there are more than 2 rooks we don't do anything as it would cause issues 
			// (some rooks would prefer connection more than others)
			// so we only do this in case there are exactly 2 rooks
			if (lWhiteRookBuffer.ActualSize == 2)
			{
				const StaticList<SquareState, 6> lSquaresBetween = GetSquaresBetween(aBoard, lWhiteRookBuffer.Items[0], lWhiteRookBuffer.Items[1]);

				bool lIsClear = true;

				for (int i = 0; i < lSquaresBetween.ActualSize; i++)
				{
					if (lSquaresBetween.Items[i].Piece_) lIsClear = false;
				}

				if (lIsClear) lReturnValue += lConnectedRooksCoeff * (1 - lGamePhase.Opening);
			}
			if (lBlackRookBuffer.ActualSize == 2)
			{
				const auto lSquaresBetween = GetSquaresBetween(aBoard, lBlackRookBuffer.Items[0], lBlackRookBuffer.Items[1]);

				bool lIsClear = true;

				for (int i = 0; i < lSquaresBetween.ActualSize; i++)
				{
					if (lSquaresBetween.Items[i].Piece_) lIsClear = false;
				}

				if (lIsClear) lReturnValue -= lConnectedRooksCoeff * (1 - lGamePhase.Opening);
			}


			return (short)round(lReturnValue * 100);
		}

	public:
		virtual std::string GetName() const override
		{
			return "KingHunter (" + std::to_string(cDepth) + ", " + std::to_string(cMaxTime) + "s)";
		}
	};
}
