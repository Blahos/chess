
namespace Players
{
	class SmarterMaterialist;
}

#include <limits>

#include "TreeSearch.h"

namespace Players
{
	class SmarterMaterialist : public TreeSearch
	{
	public:
		SmarterMaterialist(const int aDepth, const double aMaxTime = -1, const size_t aLeafCacheSize = 0, const size_t aInnerTreeCacheSize = 0)
			: TreeSearch(aDepth, aMaxTime, aLeafCacheSize, aInnerTreeCacheSize) { }

	protected:
		virtual short EvaluatePositionInner(const BoardState& aBoard, const int aDepth) override
		{
			short lReturnValue = 0;

			// piece values
			for (int iSquare = 0; iSquare < BoardSetup::cSquareCount; iSquare++)
			{
				const SquareState& lSquare = aBoard.Squares[iSquare];
				if (!lSquare.Piece_) continue;

				const Piece& lPiece = lSquare.Piece_;

				int lColourSign;

				if (lPiece & C_WHITE)
					lColourSign = 1;
				else
					lColourSign = -1;

				if (lPiece & P_PAWN) lReturnValue += lColourSign * 100;
				else if (lPiece & P_KNIGHT) lReturnValue += lColourSign * 300;
				else if (lPiece & P_BISHOP) lReturnValue += lColourSign * 300;
				else if (lPiece & P_ROOK) lReturnValue += lColourSign * 500;
				else if (lPiece & P_QUEEN) lReturnValue += lColourSign * 900;
			}

			// panws in the center
			for (int i = 2; i <= 5; i++)
			{
				for (int j = 2; j <= 5; j++)
				{
					const int lSquareIndex = CoToSI(i, j);
					const SquareState& lSquare = aBoard.Squares[lSquareIndex];
					if (!lSquare.Piece_) continue;

					const Piece& lPiece = lSquare.Piece_;

					int lColourSign;
					if (lPiece & C_WHITE)
						lColourSign = 1;
					else
						lColourSign = -1;

					if (lPiece & P_PAWN)
					{
						lReturnValue += lColourSign * 2;

						if (i >= 3 && i <= 4 && j >= 3 && j <= 4)
							lReturnValue += lColourSign * 1;
					}
				}
			}

			// pieces in the center
			for (int i = 1; i <= 6; i++)
			{
				for (int j = 1; j <= 6; j++)
				{
					const int lSquareIndex = CoToSI(i, j);
					const SquareState& lSquare = aBoard.Squares[lSquareIndex];
					if (!lSquare.Piece_) continue;

					const Piece& lPiece = lSquare.Piece_;

					int lColourSign;
					if (lPiece & C_WHITE)
						lColourSign = 1;
					else
						lColourSign = -1;

					if (lPiece & (P_KNIGHT | P_BISHOP | P_QUEEN))
					{
						lReturnValue += lColourSign * 2;
					}
				}
			}

			return lReturnValue;
		}
	public:
		virtual std::string GetName() const override
		{
			return "SmarterMaterialist (" + std::to_string(cDepth) + ")";
		}
	};
}