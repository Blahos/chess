
namespace Players
{
	class Materialist;
}

#pragma once
#include <limits>

#include "TreeSearch.h"

namespace Players
{
	class Materialist : public TreeSearch
	{
	public:
		Materialist(const int aDepth, const double aMaxTime = -1, 
			const size_t aLeafCacheSize = 0, const size_t aInnerTreeCacheSize = 0,
			const bool aDoMinimalWindowSearch = false,
			const bool aGoDeeperOnCapture = false)
			: TreeSearch(aDepth, aMaxTime, aLeafCacheSize, aInnerTreeCacheSize, aDoMinimalWindowSearch, aGoDeeperOnCapture) { }

	protected:
		virtual short EvaluatePositionInner(const BoardState& aBoard, const int aDepth) override
		{
			short lReturnValue = 0;

			for (int iSquare = 0; iSquare < BoardSetup::cSquareCount; iSquare++)
			{
				const SquareState& lSquare = aBoard.Squares[iSquare];
				if (!lSquare.Piece_) continue;

				const Piece lPiece = lSquare.Piece_;

				int lColourSign;

				if (lPiece & C_WHITE)
					lColourSign = 1;
				else
					lColourSign = -1;

				if (lPiece & P_PAWN) lReturnValue += lColourSign * 100;
				else if (lPiece & P_KNIGHT) lReturnValue += lColourSign * 300;
				else if (lPiece & P_BISHOP) lReturnValue += lColourSign * 300;
				else if (lPiece & P_ROOK) lReturnValue += lColourSign * 500;
				else if (lPiece & P_QUEEN) lReturnValue += lColourSign * 900;
			}

			return lReturnValue;
		}

	public:
		virtual std::string GetName() const override
		{
			return "Materialist (" + std::to_string(cDepth) + ")";
		}
	};
}