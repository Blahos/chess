
namespace Players
{
	class Random;
}

#pragma once
#include "../PieceMovement.h"

#include "Player.h"
#include "../Random.h"

namespace Players
{
	class Random : public Player
	{
	private:
		StaticList<Move, PieceMovement::cMaxLegalMoves> mAllLegalMoves;
	protected:
		virtual MoveWithNotes PrepareMoveInner(const BoardState& aBoard, 
			const std::vector<BoardState>& aGameHistory, const std::vector<Move>& aMoveHistory, 
			const std::atomic<bool>* const aIsAlive) override
		{
			mAllLegalMoves.ActualSize = 0;
			PieceMovement::GetAllLegalMoves(aBoard, mAllLegalMoves);

			if (mAllLegalMoves.ActualSize == 0) throw EXCEPTION_MEMBER("No legal moves!");

			const int lIndex = ::Random::GetInt(0, mAllLegalMoves.ActualSize - 1);

			return mAllLegalMoves.Items[lIndex];
		}
	public:
		virtual std::string GetName() const override
		{
			return "Random";
		}
	};
}