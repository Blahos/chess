

#ifdef WITH_CURL

namespace Players
{
	class WebPlayer;
}

#pragma once
#include <string>

#include "Player.h"
#include "../Colour.h"

// JSON lib
#include "json.hpp"
// URL lib
#include "../FunctionsCURL.h"

namespace Players
{
	class WebPlayer : public Player
	{
	private:
		const std::string cServerURL;
		const std::string cNickname;
		const std::string cOpponentNickname;
		const Colour cColour;
		const std::string cId;

		std::string mGameId = "";
		std::string mOpponentId = "";

		std::string GetId(const std::string aServerURL, const std::string aNickname) const
		{
			nlohmann::json lJ;
			lJ["nickname"] = aNickname;

			const std::string lId = Curl::Post(lJ.dump(), cServerURL + "/register");

			if (lId.size() <= 0) throw EXCEPTION_MEMBER("Register failed!");

			return lId;
		}

		// finds a game on the server by the id
		nlohmann::json FindGame()
		{
			nlohmann::json lGameListJson;

			std::string lGameList = Curl::Get(cServerURL + "/check-games/" + cId);

			if (lGameList.size() <= 0)
				throw EXCEPTION_MEMBER("Could not retreive the game list!");

			lGameListJson = nlohmann::json::parse(lGameList);

			bool lGameFound = false;
			nlohmann::json lGameJson;

			for (int i = 0; i < lGameListJson.size(); i++)
			{
				if (lGameListJson[i]["gameId"] == mGameId)
				{
					lGameJson = lGameListJson[i];
					lGameFound = true;
					break;
				}
			}

			if (!lGameFound)
				throw EXCEPTION_MEMBER("Could not find a game!");

			return lGameJson;
		}

	protected:
		virtual MoveWithNotes PrepareMoveInner(const BoardState& aBoard, 
			const std::vector<BoardState>& aGameHistory, const std::vector<Move>& aMoveHistory, const std::atomic<bool>* const aIsAlive) override
		{
			// the move history will have the last move that was played by the opposing player
			// so we upload it to the server and then wait for reply
			// the only exception to this is when this player is white and it's the first move

			if (aMoveHistory.size() > 0)
			{
				std::cout << "Submitting move ... " << std::endl;
				std::cout << "Move history size: " << aMoveHistory.size() << std::endl;
				std::cout << "Game history size: " << aGameHistory.size() << std::endl;

				if (aGameHistory.size() <= 0)
					throw EXCEPTION_MEMBER("Invalid history!");

				const Move lLastMove = aMoveHistory[aMoveHistory.size() - 1];
				const BoardState lPreviousBoard = aGameHistory[aGameHistory.size() - 1];

				const std::string lLastMovePGN = MoveToPGN(lLastMove, lPreviousBoard);

				nlohmann::json lPlayJson;

				lPlayJson["playerId"] = mOpponentId;
				lPlayJson["gameId"] = mGameId;
				lPlayJson["move"] = lLastMovePGN;

				std::cout << "Poasting move: " << lLastMovePGN << std::endl;

				const std::string lPlay = lPlayJson.dump();

				const std::string lDummy = Curl::Post(lPlay, cServerURL + "/play");
			}

			// no we poll the server until we find a reply
			MoveWithNotes lReturnMove;

			while (true)
			{
				if (!(*aIsAlive))
					break;

				nlohmann::json lGame = FindGame();

				if ((lGame["gameStatus"] == "white" && cColour == C_BLACK)
					||
					(lGame["gameStatus"] == "black" && cColour == C_WHITE))
				{
					nlohmann::json lMoves = lGame["moves"];

					if (lMoves.size() == aMoveHistory.size() + 1)
					{
						const std::string lLastMove = lMoves[lMoves.size() - 1]["move"];

						std::cout << "Last move pulled: " << lLastMove << std::endl;

						const Move lMove = ParsePGNMove(lLastMove, aBoard);
						lReturnMove = lMove;
						break;
					}
				}

				std::this_thread::sleep_for(std::chrono::milliseconds(200));
			}

			return lReturnMove;
		}

		virtual void Reset() override
		{
			// get list of players
			const std::string lResult = Curl::Get(cServerURL + "/players");

			//std::cout << "Obtained players: " << std::endl;
			//std::cout << lResult << std::endl;

			nlohmann::json lResultJson = nlohmann::json::parse(lResult);

			//std::cout << "Player 1: " << std::endl;
			//std::cout << lResultJson[0]["id"] << std::endl;

			nlohmann::json lOpponent;
			bool lOpponentFound = false;
			for (int i = 0; i < lResultJson.size(); i++)
			{
				//std::cout << lResultJson[i].dump() << std::endl;
				if (lResultJson[i]["nickname"] == cOpponentNickname)
				{
					lOpponent = lResultJson[i];
					lOpponentFound = true;
					break;
				}
			}

			if (!lOpponentFound)
				throw EXCEPTION_MEMBER("Opponent not found!");

			if (cColour == C_WHITE)
			{
				// white starts a game
				nlohmann::json lStartGameJson;
				lStartGameJson["whitePlayer"] = cId;
				lStartGameJson["blackPlayer"] = lOpponent["id"];

				mGameId = Curl::Post(lStartGameJson.dump(), cServerURL + "/start-game");

				if (mGameId.size() <= 0)
					throw EXCEPTION_MEMBER("Could not start a game!");
				
				mOpponentId = lOpponent["id"];
			}
			else
			{
				// black just finds a game between him and his opponent

				nlohmann::json lGameListJson;

				std::string lGameList = Curl::Get(cServerURL + "/check-games/" + cId);

				if (lGameList.size() <= 0)
					throw EXCEPTION_MEMBER("Could not retreive the game list!");

				lGameListJson = nlohmann::json::parse(lGameList);
				
				bool lGameFound = false;
				nlohmann::json lGameJson;

				for (int i = 0; i < lGameListJson.size(); i++)
				{
					if (lGameListJson[i]["blackPlayer"] == cId)
					{
						lGameJson = lGameListJson[i];
						lGameFound = true;
						break;
					}
				}

				if (!lGameFound)
					throw EXCEPTION_MEMBER("Could not find a game!");

				mGameId = lGameJson["gameId"];
				mOpponentId = lGameJson["whitePlayer"];
			}
		}

	public:
		WebPlayer(const std::string& aServerURL, const std::string& aNickname, const std::string aOpponentNickname, const Colour aThisPlayerColour)
			: cServerURL(aServerURL), cNickname(aNickname), cOpponentNickname(aOpponentNickname), cColour(aThisPlayerColour), cId(GetId(aServerURL, aNickname))
		{
		}

		virtual std::string GetName() const override
		{
			return "WebPlayer (" + cNickname + ")";
		}

	};
}

#endif
