

namespace Players
{
	class Human;
}
#pragma once
#include <thread>
#include <chrono>
#include <mutex>

#include "Player.h"
#include "../MoveProducer.h"
#include "../FunctionsMove.h"
#include "../BoardState.h"
#include "../Exception.h"

namespace Players
{
	class Human : public Player, public MoveProducer::Listener
	{
	private:
		bool mHasMove = false;
		bool mWaitingForMove = false;
		Move mMove;
		BoardState mBoard;
		std::mutex mStateMutex;

	protected:
		virtual MoveWithNotes PrepareMoveInner(const BoardState& aBoard, 
			const std::vector<BoardState>& aGameHistory, const std::vector<Move>& aMoveHistory, const std::atomic<bool>* const aIsAlive) override
		{
			Move lReturnMove;

			// HACK
			//if (aMoveHistory.size() == 0)
			//	return ParsePGNMove("e4", aBoard);
			//else if (aMoveHistory.size() == 2)
			//	return ParsePGNMove("exd5", aBoard);
			//else if (aMoveHistory.size() == 4)
			//	return ParsePGNMove("Nc3", aBoard);
			//else if (aMoveHistory.size() == 6)
			//	return ParsePGNMove("Nge2", aBoard);

			mStateMutex.lock();
			mHasMove = false;
			mWaitingForMove = true;
			mBoard = aBoard;
			mStateMutex.unlock();

			while (true)
			{
				if (!(*aIsAlive))
				{
					mStateMutex.lock();
					mWaitingForMove = false;
					mStateMutex.unlock();
					break;
				}

				mStateMutex.lock();
				if (mHasMove)
				{
					lReturnMove = mMove;
					mWaitingForMove = false;
					mStateMutex.unlock();
					break;
				}
				mStateMutex.unlock();

				std::this_thread::sleep_for(std::chrono::milliseconds(10));
			}

			return lReturnMove;
		}

	public:
		virtual std::string GetName() const override
		{
			return "Human";
		}
		virtual void NewMove(const Move aMove) override
		{
			mStateMutex.lock();
			mMove = aMove;
			mHasMove = true;
			mStateMutex.unlock();
		}
		virtual void NewMovePGN(const std::string& aPGNMove) override
		{
			if (!mWaitingForMove) return;

			Move lParsedMove;
			try
			{
				lParsedMove = ParsePGNMove(aPGNMove, mBoard);
			}
			catch (const Exception & lEx)
			{
				std::cout << "PGN parse exception: " << std::endl;
				std::cout << lEx << std::endl;
				return;
			}

			mStateMutex.lock();
			mHasMove = true;
			mMove = lParsedMove;
			mStateMutex.unlock();
		}
	};
}