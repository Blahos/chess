
struct MoveRequestId;

#pragma once

struct MoveRequestId
{
public:
	int MoveNumber;
	unsigned long long GameId;
	Colour PlayerToMove;
	// indicates whether a player should reset before preparing this move (for instance when a new game started)
	bool ResetBefore;
	bool IsValid = false;

	bool operator<(const MoveRequestId& aOther) const
	{
		if (MoveNumber < aOther.MoveNumber) return true;
		else if (MoveNumber > aOther.MoveNumber) return false;
		else if (GameId < aOther.GameId) return true;
		else if (GameId > aOther.GameId) return false;
		else if (PlayerToMove < aOther.PlayerToMove) return true;
		else if (PlayerToMove > aOther.PlayerToMove) return false;
		else if (ResetBefore < aOther.ResetBefore) return true;
		else if (ResetBefore > aOther.ResetBefore) return false;
		else return IsValid < aOther.IsValid;
	}
};
