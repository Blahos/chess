class Random;

#pragma once

#include <random>
#include <iostream>

class Random
{
private:
	class StaticData
	{
	public:
		std::mt19937_64 Generator;
		std::random_device Device;
		StaticData()
			: Device()
		{
			Generator = std::mt19937_64(Device());
		}
	};

	static StaticData lData;

public:
	static int GetInt(const int aMin, int aMax)
	{
		std::uniform_int_distribution<unsigned long long> lDistribution(aMin, aMax);

		const int lResult = lDistribution(lData.Generator);
		return lResult;
	}

	static unsigned long long GetIntU64(const unsigned long long aMin = 0, unsigned long long aMax = std::numeric_limits<unsigned long long>::max())
	{
		std::uniform_int_distribution<unsigned long long> lDistribution(aMin, aMax);

		const unsigned long long lResult = lDistribution(lData.Generator);
		return lResult;
	}

	static double GetDouble(const double aMin, const double aMax)
	{
		std::uniform_real_distribution<double> lDistribution(aMin, aMax);

		const double lResult = lDistribution(lData.Generator);
		return lResult;
	}
};
