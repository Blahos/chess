
enum class CmdCommandType
{
    Empty,
	Move,
	NewGame,
	RotateBoard,
	Analyse,
	Evaluate,
	ExportFEN,
	ImportPGN,
	ImportFEN,
	CountPositions,
	Quit,
	Help
};

struct CmdCommand;

#pragma once
#include <string>
#include <vector>

struct CmdCommand
{
public:
    CmdCommandType Type = CmdCommandType::Empty;
    std::vector<std::string> Arguments = std::vector<std::string>();
};