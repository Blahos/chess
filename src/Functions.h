
#pragma once
#include <string>
#include <omp.h>


template <class T>
inline int Sgn(T val)
{
	return (T(0) < val) - (val < T(0));
}

inline std::vector<std::string> SplitString(const std::string& aString, const std::string& aDelimiter, const bool aIncludeDelimiter = false)
{
	std::vector<std::string> lReturnVector;

	std::string lStringCopy(aString);

	size_t lPos = 0;
	std::string lToken;
	while ((lPos = lStringCopy.find(aDelimiter)) != std::string::npos)
	{
		lToken = lStringCopy.substr(0, lPos);

		if (lToken.length() > 0)
			lReturnVector.push_back(lToken);

		if (aIncludeDelimiter)
			lReturnVector.push_back(aDelimiter);

		lStringCopy.erase(0, lPos + aDelimiter.length());
	}

	// the last part after the last delimiter
	if (lStringCopy.length() > 0)
		lReturnVector.push_back(lStringCopy);

	return lReturnVector;
}
inline std::vector<std::string> SplitString(const std::vector<std::string>& aStrings, const std::string& aDelimiter, const bool aIncludeDelimiter = false)
{
	std::vector<std::string> lReturnVector;

	for (int i = 0; i < aStrings.size(); i++)
	{
		std::vector<std::string> lTmp = SplitString(aStrings[i], aDelimiter, aIncludeDelimiter);
		for (int j = 0; j < lTmp.size(); j++)
			lReturnVector.push_back(lTmp[j]);
	}

	return lReturnVector;
}

inline int GetOMPThreadCount()
{
	int lThreadCount = 0;
#pragma omp parallel reduction(+: lThreadCount)
	{
		lThreadCount++;
	}

	return lThreadCount;
}