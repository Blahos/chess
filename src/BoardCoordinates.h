
struct BoardCoordinates;

#pragma once
#include <string>

struct BoardCoordinates
{
public:
	char Row = -1;
	char Col = -1;
	
	BoardCoordinates() noexcept { }
	BoardCoordinates(const char aRow, const char aCol) : Row(aRow), Col(aCol) { }
	
	inline bool operator==(const BoardCoordinates& aOther) const
	{
		return Row == aOther.Row && Col == aOther.Col;
	}
	inline bool operator!=(const BoardCoordinates& aOther) const
	{
		return !(*this == aOther);
	}
	inline bool operator<(const BoardCoordinates& aOther) const
	{
		if (Row == aOther.Row) 
			return Col < aOther.Col;
		return Row < aOther.Row;
	}
};