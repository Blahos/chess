
#include "Move.h"

bool Move::IsCapture(const BoardState& aBoard) const
{
	if (IsEnPassant) return true;
	const int lSquareIndex = CoToSI(End);
	const SquareState& lSquare = aBoard.Squares[lSquareIndex];

	if (lSquare.Piece_) return true;
	return false;
}

bool Move::IsPawnCapture(const BoardState& aBoard) const
{
	if (IsEnPassant) return true;
	const int lSquareIndex = CoToSI(End);
	const SquareState& lSquare = aBoard.Squares[lSquareIndex];

	if (lSquare.Piece_ & P_PAWN) return true;
	return false;
}
