
#pragma once
#include "Exception.h"

//enum class Colour : unsigned char
//{
//	White = 0, Black = 1
//};

typedef unsigned short Colour;

#define C_WHITE (Colour)(255)
#define C_BLACK (Colour)(C_WHITE << 8)

inline Colour GetOppositeColour(const Colour aColour)
{
	if (aColour & C_WHITE) return C_BLACK;
	else return C_WHITE;
}
