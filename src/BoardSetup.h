
class BoardSetup;

#pragma once

class BoardSetup
{
public:
	static const int cRows = 8;
	static const int cCols = 8;
	static const int cSquareCount = cRows * cCols;
	static const int cMaxPieces = 32;
};
