
class CmdInputReader;

#pragma once
#include <atomic>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <mutex>
#include "CmdCommand.h"
#include "MoveProducer.h"

class CmdInputReader : public MoveProducer
{
private:
	std::atomic<bool> mTerminate{ false };
    std::vector<CmdCommand> mCommands = std::vector<CmdCommand>();
    std::mutex mCommandsMutex;

	struct CmdCommandTranslation
	{
	public:
		std::string FullName = "";
		std::vector<std::string> Abbreviations;
		CmdCommandType CommandType = CmdCommandType::Empty;
		std::string Description = "";

		CmdCommandTranslation() { }

		CmdCommandTranslation(const std::string& aFullName, const std::vector<std::string> &aAbbreviations, const CmdCommandType aCommandType, const std::string aDescription)
			: FullName(aFullName), Abbreviations(aAbbreviations), CommandType(aCommandType), Description(aDescription)
		{

		}
	};

	std::vector<CmdCommandTranslation> mCommandTranslations =
	{
		CmdCommandTranslation("move", { "m" }, CmdCommandType::Move, "Make a move on current board"),
		CmdCommandTranslation("newgame", { "n" }, CmdCommandType::NewGame, "Start a new game"),
		CmdCommandTranslation("quit", { "q" }, CmdCommandType::Quit, "Quit program"),
		CmdCommandTranslation("rotate", { "r" }, CmdCommandType::RotateBoard, "Rotate board"),
		CmdCommandTranslation("analyse", { "a" }, CmdCommandType::Analyse, "Analyse current position (best move search)"),
		CmdCommandTranslation("evaluate", { "ev" }, CmdCommandType::Evaluate, "Evaluate current position (static position evaluation)"),
		CmdCommandTranslation("export", { "ex" }, CmdCommandType::ExportFEN, "Export current board as FEN"),
		CmdCommandTranslation("importpgn", { "ip", "importp" }, CmdCommandType::ImportPGN, "Import PGN file"),
		CmdCommandTranslation("importfen", { "if", "importf" }, CmdCommandType::ImportFEN, "Import FEN file"),
		CmdCommandTranslation("countpositions", { "cp", "countp" }, CmdCommandType::CountPositions, "Count positions reachable from current position"),
		CmdCommandTranslation("help", { "h", }, CmdCommandType::Help, "Print help"),
	};

    CmdCommand ParseCommand(const std::string& aLine)
    {
        CmdCommand lReturnCommand;
        lReturnCommand.Type = CmdCommandType::Empty;

        std::stringstream lS(aLine);

        std::vector<std::string> lParsedParts;
        std::string lPart;
        while (std::getline(lS, lPart, ' '))
        {
            if (lPart.size() == 0) 
                continue;
            lParsedParts.push_back(lPart);
        }

        if (lParsedParts.size() == 0) return lReturnCommand;

        // transform to lowercase
        std::string lFirstPart = lParsedParts[0];
        std::transform(lFirstPart.begin(), lFirstPart.end(), lFirstPart.begin(),
            [](unsigned char c){ return std::tolower(c); });


		bool lCommandFound = false;
		for (int i = 0; i < mCommandTranslations.size(); i++)
		{
			const CmdCommandTranslation& lCommand = mCommandTranslations.at(i);
			bool lMatch = false;
			if (lCommand.FullName == lFirstPart)
				lMatch = true;

			if (!lMatch)
			{
				for (int j = 0; j < lCommand.Abbreviations.size(); j++)
				{
					const std::string& lAbbrev = lCommand.Abbreviations.at(j);
					if (lFirstPart.substr(0, lAbbrev.size()).compare(lAbbrev) == 0)
					{
						lMatch = true;
						break;
					}
				}
			}

			if (lMatch)
			{
				lCommandFound = true;
				lReturnCommand.Type = lCommand.CommandType;
				break;
			}
		}

		if (!lCommandFound) std::cout << "Unrecognised command" << std::endl;

  //      if (lFirstPart.substr(0, 1).compare("m") == 0)
  //      {
		//	lReturnCommand.Type = CmdCommandType::Move;
  //      }
		//else if (lFirstPart.substr(0, 1).compare("n") == 0)
		//{
		//	lReturnCommand.Type = CmdCommandType::NewGame;
		//}
		//else if (lFirstPart.substr(0, 1).compare("q") == 0)
		//{
		//	lReturnCommand.Type = CmdCommandType::Quit;
		//}
		//else if (lFirstPart.substr(0, 1).compare("r") == 0)
		//{
		//	lReturnCommand.Type = CmdCommandType::RotateBoard;
		//}
		//else if (lFirstPart.substr(0, 1).compare("a") == 0)
		//{
		//	lReturnCommand.Type = CmdCommandType::Analyse;
		//}
		//else if (lFirstPart.substr(0, 2).compare("ev") == 0)
		//{
		//	lReturnCommand.Type = CmdCommandType::Evaluate;
		//}
		//else if (lFirstPart.substr(0, 2).compare("ex") == 0)
		//{
		//	lReturnCommand.Type = CmdCommandType::ExportFEN;
		//}
		//else if (lFirstPart.substr(0, 2).compare("ip") == 0 || lFirstPart.substr(0, 9).compare("importpgn") == 0)
		//{
		//	lReturnCommand.Type = CmdCommandType::ImportPGN;
		//}
		//else if (lFirstPart.substr(0, 2).compare("cp") == 0)
		//{
		//	lReturnCommand.Type = CmdCommandType::CountPositions;
		//}
  //      else std::cout << "Unrecognised command" << std::endl;

        for (int i = 1; i < lParsedParts.size(); i++)
        {
            lReturnCommand.Arguments.push_back(lParsedParts[i]);
        }

		if (lReturnCommand.Type == CmdCommandType::Move)
		{
			// we swallow the move command right here and turn it into a notification
			// it does not get passed further (we change it to empty command since we already processed it here)
			lReturnCommand.Type = CmdCommandType::Empty;
			if (lReturnCommand.Arguments.size() > 0)
			{
				NotifyNewMovePGN(lReturnCommand.Arguments[0]);
			}
		}
		else if (lReturnCommand.Type == CmdCommandType::Help)
		{
			// swallow and print help
			lReturnCommand.Type = CmdCommandType::Empty;

			for (int i = 0; i < mCommandTranslations.size(); i++)
			{
				const CmdCommandTranslation& lCommand = mCommandTranslations.at(i);
				std::cout << lCommand.FullName << " (";

				for (int j = 0; j < lCommand.Abbreviations.size(); j++)
				{
					std::cout << lCommand.Abbreviations[j];
					if (j < lCommand.Abbreviations.size() - 1) std::cout << ", ";
				}
				std::cout << ")" << std::endl;

				std::cout << "\t" << lCommand.Description << std::endl;
			}
		}

        return lReturnCommand;
    }

public:
    void Run()
    {
        while (!mTerminate)
        {
            std::string lLine;
            std::getline(std::cin, lLine);

            mCommandsMutex.lock();

            CmdCommand lCommand = ParseCommand(lLine);
            if (lCommand.Type != CmdCommandType::Empty)
            {
                mCommands.push_back(lCommand);
            }

            mCommandsMutex.unlock();
        }

		UnsubscribeAll();
    }

    void Terminate() { mTerminate  = true; }

    std::vector<CmdCommand> PopCommands()
    {
        std::vector<CmdCommand> lCommandsCopy;

        mCommandsMutex.lock();

        lCommandsCopy = std::vector<CmdCommand>(mCommands);
        mCommands.clear();

        mCommandsMutex.unlock();

        return lCommandsCopy;
    }
};