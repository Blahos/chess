
struct SquareState;

#pragma once

#include "Piece.h"
#include "Colour.h"
#include "BoardCoordinates.h"

struct SquareState
{
public:
	Piece Piece_;
	BoardCoordinates Coordinates;

	bool operator==(const SquareState& aOther) const
	{
		//if (SquareColour != aOther.SquareColour) return false;
		if (Coordinates != aOther.Coordinates) return false;
		return Piece_ == aOther.Piece_;
	}
	bool operator!=(const SquareState& aOther) const
	{
		return !(*this == aOther);
	}
};
