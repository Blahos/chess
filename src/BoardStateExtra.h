
struct BoardStateExtra;

#pragma once

// extra information about the board state, that is not necessary but can help the performance
struct BoardStateExtra
{
public:
	bool IsMovingPlayerInCheck = false;
};