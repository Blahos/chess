
#include "FunctionsMove.h"
#include "FunctionsBoard.h"
#include "Functions.h"

#include "Tables.h"

#include <fstream>
#include <unordered_set>

Move ParsePGNMove(const std::string& aPGNString, const BoardState& aBoard)
{
	if (aPGNString.size() < 2) throw EXCEPTION_GLOBAL("Too short string!");

	Move lReturnMove;

	// sets colour
	lReturnMove.MovedPiece = aBoard.PlayerToMove;

	BoardCoordinates lPartialStart;

	// first we check the long castle, since the short castle is a substring of the long castle (considering just O-O and O-O-O)
	if (aPGNString.size() >= 5 && aPGNString.substr(0, 5) == "O-O-O")
	{
		lReturnMove.MovedPiece &= P_KING;
		lReturnMove.End.Col = 2;
		lReturnMove.End.Row = (aBoard.PlayerToMove & C_WHITE) ? 0 : 7;
	}
	else if (aPGNString.size() >= 3 && aPGNString.substr(0, 3) == "O-O")
	{
		lReturnMove.MovedPiece &= P_KING;
		lReturnMove.End.Col = 6;
		lReturnMove.End.Row = (aBoard.PlayerToMove & C_WHITE) ? 0 : 7;
	}
	else
	{
		// sets piece type
		lReturnMove.MovedPiece &= GetMovedPiece(aPGNString);
		const std::string lCoordSubstring = GetPureMoveCoordinates(aPGNString);

		std::string lEndCoord = lCoordSubstring.substr(lCoordSubstring.size() - 2, 2);

		lReturnMove.End = PGNCoordToBoardCoord(lEndCoord);

		if (lCoordSubstring.size() > 2)
		{
			lPartialStart = PGNCoordToBoardCoord(lCoordSubstring.substr(0, lCoordSubstring.size() - 2));
		}
	}

	lReturnMove.PromotionPiece = P_NONE;
	// check for promotion
	if ((lReturnMove.MovedPiece & P_PAWN) && IsLastRow(lReturnMove.End, lReturnMove.MovedPiece))
	{
		const int lEqSignPos = aPGNString.find("=");
		if (lEqSignPos == std::string::npos) throw EXCEPTION_GLOBAL("Could not find expected promotion symbol!");
		if (lEqSignPos == aPGNString.size() - 1) throw EXCEPTION_GLOBAL("Expected promotion symbol is last character!");
		const char lPromotionPieceChar = aPGNString[lEqSignPos + 1];
		// this function only returns a colour undecided piece
		const Piece lPromotionPiece = GetPieceFromPNGSymbol(lPromotionPieceChar);

		if (lPromotionPiece & (P_PAWN | P_KING))
			throw EXCEPTION_GLOBAL("Invalid promotion piece (pawn, king or something completely wrong)! (" + std::string(1, lPromotionPieceChar) + ")");

		lReturnMove.PromotionPiece = lPromotionPiece & aBoard.PlayerToMove;
	}

	// find square with piece that has a move that satisfies the PGN string
	int lPieceSquareIndex = -1;
	bool lIsEnPassant = false;

	for (int i = 0; i < BoardSetup::cSquareCount; i++)
	{
		const SquareState& lSquare = aBoard.Squares[i];
		if (lSquare.Piece_ != lReturnMove.MovedPiece) continue;

		if (lPartialStart.Row >= 0 && lPartialStart.Row != lSquare.Coordinates.Row) continue;
		if (lPartialStart.Col >= 0 && lPartialStart.Col != lSquare.Coordinates.Col) continue;

		StaticList<Move, PieceMovement::cMaxLegalMoves> lPotentialMoves;
		PieceMovement::GetLegalMoves(aBoard, lSquare.Coordinates.Row, lSquare.Coordinates.Col, lPotentialMoves);

		for (int iMove = 0; iMove < lPotentialMoves.ActualSize; iMove++)
		{
			const Move& lThisPieceMove = lPotentialMoves.Items[iMove];

			if (lThisPieceMove.End != lReturnMove.End) continue;

			// if it's a pawn moving to last row, we need to compare the promotion
			if ((lSquare.Piece_ & P_PAWN) && IsLastRow(lReturnMove.End, lReturnMove.MovedPiece))
			{
				if (lReturnMove.PromotionPiece != lThisPieceMove.PromotionPiece) continue;

				// if a move was already found previously and this is another one
				if (lPieceSquareIndex >= 0) throw EXCEPTION_GLOBAL("Multiple moves possible, add specification in the PGN!");
				lPieceSquareIndex = i;
				lIsEnPassant = lThisPieceMove.IsEnPassant;
			}
			else
			{
				if (lPieceSquareIndex >= 0) throw EXCEPTION_GLOBAL("Multiple moves possible, add specification in the PGN!");
				lPieceSquareIndex = i;
				lIsEnPassant = lThisPieceMove.IsEnPassant;
			}
		}
		// even if we found the piece, we still iterate over the rest of the moves to check if there are more possible moves
		// like when you have 2 knights that can both go to the same square and the PGN doesn't specify which knight it should be
		// then we throw an exception
	}

	if (lPieceSquareIndex < 0) throw EXCEPTION_GLOBAL("Could not find a square with a piece satisfying the PGN!");

	lReturnMove.Start = aBoard.Squares[lPieceSquareIndex].Coordinates;

	lReturnMove.IsEnPassant = lIsEnPassant;

	return lReturnMove;
}

std::string MoveToPGN(const Move& aMove, const BoardState& aBoard)
{
	const BoardState lBoardAfterMove = aBoard.MakeMove(aMove);
	const bool lIsMate = lBoardAfterMove.IsMate();
	const bool lIsCheck = lBoardAfterMove.IsMovingPlayerInCheck();

	std::string lReturnString = "";

	// handle castling
	if (aMove.IsCastle())
	{
		const int lColDiff = aMove.End.Col - aMove.Start.Col;
		if (lColDiff > 0) lReturnString = "O-O";
		else lReturnString = "O-O-O";

		if (lIsMate) lReturnString += "#";
		else if (lIsCheck) lReturnString += "+";

		return lReturnString;
	}

	// add piece type letter
	lReturnString += GetPiecePGNSymbolFromPiece(aMove.MovedPiece);

	std::vector<LocalisedPiece> lAllPiecesWithMoveEnd;

	for (int i = 0; i < BoardSetup::cSquareCount; i++)
	{
		const SquareState& lSquare = aBoard.Squares[i];
		if (lSquare.Piece_ != aMove.MovedPiece) continue;

		LocalisedPiece lPiece;
		lPiece.Piece_ = lSquare.Piece_;
		lPiece.Coord = lSquare.Coordinates;

		std::vector<Move> lPotentialMoves;
		PieceMovement::GetPotentialMoves(aBoard, lPiece.Coord.Row, lPiece.Coord.Col, lPotentialMoves);

		for (int j = 0; j < lPotentialMoves.size(); j++)
		{
			if (lPotentialMoves[j].End == aMove.End)
			{
				// check if this piece is not already added (can be in promotion case, where we have 4 different moves with the same piece)
				bool lIsIn = false;
				for (int k = 0; k < lAllPiecesWithMoveEnd.size(); k++)
				{
					if (lAllPiecesWithMoveEnd[k] == lPiece)
					{
						lIsIn = true;
						break;;
					}
				}
				if (!lIsIn)
					lAllPiecesWithMoveEnd.push_back(lPiece);
			}
		}
	}

	// check which coord is repeated multiple times among the pieces
	int lOtherRowSameColCoords = 0;
	int lOtherColSameRowCoords = 0;

	for (int i = 0; i < lAllPiecesWithMoveEnd.size(); i++)
	{
		if (lAllPiecesWithMoveEnd[i].Coord == aMove.Start) continue; // this is the piece that is actually moving

		if (lAllPiecesWithMoveEnd[i].Coord.Row == aMove.Start.Row)
			lOtherColSameRowCoords++;

		if (lAllPiecesWithMoveEnd[i].Coord.Col == aMove.Start.Col)
			lOtherRowSameColCoords++;
	}

	const bool lNeedRowSpec = lOtherRowSameColCoords > 0;
	const bool lNeedColSpec = lOtherColSameRowCoords > 0;

	// specify the starting piece further by the starting coordinates if necessary

	bool lStartSpecified = false;

	if (lAllPiecesWithMoveEnd.size() > 1)
	{
		if (lNeedColSpec)
		{
			lReturnString += ('a' + aMove.Start.Col);
			lStartSpecified = true;
		}

		if (lNeedRowSpec)
		{
			lReturnString += ('1' + aMove.Start.Row);
			lStartSpecified = true;
		}

		// if there are multiple pieces with the same target square but none share rows or columns with the moved piece,
		// we can specify either row or column only
		if (!lStartSpecified)
		{
			lReturnString += ('a' + aMove.Start.Col);
			lStartSpecified = true;
		}
	}

	// with pawn captures we specify the column even if there are not multiple pieces that could have made the move
	// just so the notation doesn't start with the "x" symbol for captures
	if ((aMove.MovedPiece & P_PAWN) && aMove.Start.Col != aMove.End.Col && !lStartSpecified)
	{
		lReturnString += ('a' + aMove.Start.Col);
		lStartSpecified = true;
	}

	// check for capture
	const SquareState& lEndSquare = aBoard.Squares[CoToSI(aMove.End)];
	if ((lEndSquare.Piece_ & GetOppositeColour(aMove.MovedPiece))
		||
		aMove.IsEnPassant)
	{
		lReturnString += 'x';
	}

	lReturnString += 'a' + aMove.End.Col;
	lReturnString += '1' + aMove.End.Row;

	if (aMove.IsPromotion())
	{
		lReturnString += '=';
		lReturnString += GetPiecePGNSymbolFromPiece(aMove.PromotionPiece);
	}

	if (lIsMate) lReturnString += '#';
	else if (lIsCheck) lReturnString += '+';

	return lReturnString;
}

// check if there are any potential moves to the given square on the given board
bool CanBeMovedTo(const BoardState& aBoard, const BoardCoordinates& aSquareCoord)
{
	for (int i = 0; i < BoardSetup::cSquareCount; i++)
	{
		const SquareState& lSquare = aBoard.Squares[i];
		if (!(lSquare.Piece_ & aBoard.PlayerToMove)) continue;

		const bool lCanMoveTo = PieceMovement::CanPotentiallyMoveTo(aBoard, lSquare.Coordinates, aSquareCoord);
		if (lCanMoveTo) return true;
	}
	return false;
	//for (int iPiece = 0; iPiece < aBoard.GetPieceCount(); iPiece++)
	//{
	//	const LocalisedPiece& lPiece = aBoard.Pieces[iPiece];
	//	if (lPiece.Piece_.Colour_ != aBoard.PlayerToMove) continue;

	//	const bool lCanMoveTo = PieceMovement::CanPotentiallyMoveTo(aBoard, lPiece.Coord, aSquareCoord);
	//	if (lCanMoveTo) return true;
	//}
	//return false;
}

// check if a square is potentially attacked by any piece (of colour of current player to move)
// does not consider en passant, so don't use it to detect attacked pawns
bool IsAttacked(const BoardState& aBoard, const BoardCoordinates& aSquareCoord)
{
	// check for knights
	const char lKnightRowDiff[] = { -2, -1, 1, 2, 2, 1, -1, -2 };
	const char lKnightColDiff[] = { -1, -2, -2, -1, 1, 2, 2, 1 };

	BoardCoordinates lStartCoord;

	const Piece lKnight = P_KNIGHT & aBoard.PlayerToMove;

	for (int i = 0; i < 8; i++)
	{
		lStartCoord.Row = aSquareCoord.Row + lKnightRowDiff[i];
		lStartCoord.Col = aSquareCoord.Col + lKnightColDiff[i];
		if (!IsIn(lStartCoord)) continue;
		const char lIndex = CoToSI(lStartCoord);
		//const SquareState& lSquare = aBoard.Squares[lIndex];
		if (aBoard.Squares[lIndex].Piece_ == lKnight) return true;
	}

	// check for bishops, queens and kings
	const char lDiagRowDiff[] = { 1, 1, -1, -1 };
	const char lDiagColDiff[] = { 1, -1, 1, -1 };

	for (int i = 0; i < 4; i++)
	{
		char lDist = 0;

		const char& lCurentRowDiff = lDiagRowDiff[i];
		const char& lCurrentColDiff = lDiagColDiff[i];

		while (true)
		{
			lDist++;

			lStartCoord.Row = aSquareCoord.Row + lDist * lCurentRowDiff;
			lStartCoord.Col = aSquareCoord.Col + lDist * lCurrentColDiff;
			if (!IsIn(lStartCoord)) break;
			const char lIndex = CoToSI(lStartCoord);
			const SquareState& lSquare = aBoard.Squares[lIndex];
			if (!lSquare.Piece_) continue;
			if (!(lSquare.Piece_ & aBoard.PlayerToMove)) break;
			if (lSquare.Piece_ & P_QUEEN) return true;
			if (lSquare.Piece_ & P_BISHOP) return true;
			if ((lSquare.Piece_ & P_KING) && lDist == char(1)) return true;

			break;
		}
	}

	// check for rooks, queens and kings
	const char lStarightRowDiff[] = { 1, 0, -1, 0 };
	const char lStarightColDiff[] = { 0, 1, 0, -1 };

	for (int i = 0; i < 4; i++)
	{
		char lDist = 0;

		const char& lCurentRowDiff = lStarightRowDiff[i];
		const char& lCurrentColDiff = lStarightColDiff[i];

		while (true)
		{
			lDist++;

			lStartCoord.Row = aSquareCoord.Row + lDist * lCurentRowDiff;
			lStartCoord.Col = aSquareCoord.Col + lDist * lCurrentColDiff;

			if (!IsIn(lStartCoord)) break;
			const char lIndex = CoToSI(lStartCoord);
			const SquareState& lSquare = aBoard.Squares[lIndex];
			if (!lSquare.Piece_) continue;
			if (!(lSquare.Piece_ & aBoard.PlayerToMove)) break;
			if (lSquare.Piece_ & P_QUEEN) return true;
			if (lSquare.Piece_ & P_ROOK) return true;
			if ((lSquare.Piece_ & P_KING) && lDist == char(1)) return true;

			break;
		}
	}

	// check for pawns, only standard capture (no en passant)
	const char lForwardDir = GetForwardRowDirection(aBoard.PlayerToMove);

	const Piece lPawn = P_PAWN & aBoard.PlayerToMove;

	// we are at the end square of a potential capture, so we need to look in the opposite of the forward direction
	const BoardCoordinates lPawnCaptureStart1(aSquareCoord.Row - lForwardDir, aSquareCoord.Col + char(1));
	const BoardCoordinates lPawnCaptureStart2(aSquareCoord.Row - lForwardDir, aSquareCoord.Col - char(1));

	if (IsIn(lPawnCaptureStart1))
	{
		const char lIndex = CoToSI(lPawnCaptureStart1);
		const SquareState& lSquare = aBoard.Squares[lIndex];
		if (lSquare.Piece_ & lPawn) return true;
	}

	if (IsIn(lPawnCaptureStart2))
	{
		const char lIndex = CoToSI(lPawnCaptureStart2);
		const SquareState& lSquare = aBoard.Squares[lIndex];
		if (lSquare.Piece_ & lPawn) return true;
	}

	return false;
}

int IsXRay(const BoardState& aBoard, const int& aFromSquareIndex, const int& aToSquareIndex)
{
	if (aFromSquareIndex == aToSquareIndex)
		return -1;
	
	const SquareState& lFromSquare = aBoard.Squares[aFromSquareIndex];
	if (!(lFromSquare.Piece_)) return -1;

	if (!(lFromSquare.Piece_ & (P_BISHOP | P_ROOK | P_QUEEN)))
		return -1;

	const SquareState& lToSquare = aBoard.Squares[aToSquareIndex];

	const int lRowDiff = lToSquare.Coordinates.Row - lFromSquare.Coordinates.Row;
	const int lColDiff = lToSquare.Coordinates.Col - lFromSquare.Coordinates.Col;

	const int lRowDiffAbs = std::abs(lRowDiff);
	const int lColDiffAbs = std::abs(lColDiff);

	if (!(lRowDiffAbs == lColDiffAbs || lRowDiffAbs == 0 || lColDiffAbs == 0)) 
		return -1;

	if ((lRowDiffAbs == 0 || lColDiffAbs == 0) && (lFromSquare.Piece_ & P_BISHOP))
		return -1;

	if (lRowDiffAbs == lColDiffAbs && (lFromSquare.Piece_ & P_ROOK))
		return -1;

	const int lRowDiffSign = Sgn(lRowDiff);
	const int lColDiffSign = Sgn(lColDiff);

	int lReturnValue = 0;

	for (int i = 1; i < std::max(lRowDiffAbs, lColDiffAbs); i++)
	{
		const BoardCoordinates lMiddleSquareCoord(lFromSquare.Coordinates.Row + i * lRowDiffSign, lFromSquare.Coordinates.Col + i * lColDiffSign);
		const int lMiddleSquareIndex = CoToSI(lMiddleSquareCoord);
		if (aBoard.Squares[lMiddleSquareIndex].Piece_)
			lReturnValue++;
	}

	return lReturnValue;
}

std::vector<std::string> ParsePGNMovesFile(const std::string aPath)
{
	std::vector<std::string> lReturnVector;
	std::ifstream lFile(aPath);

	if (!lFile.is_open())
	{
		std::cout << "Moves file " << aPath << " not found!" << std::endl;
		return lReturnVector;
	}

	// indicate whether we are currently inside a comment block of the pgn file
	bool lInsideComment = false;

	while (!lFile.eof())
	{
		std::string lBuffer;
		std::getline(lFile, lBuffer);

		int lLineStart = 0;

		// find line start (skip white spaces)

		while (lLineStart < lBuffer.size() && (lBuffer[lLineStart] == ' ' || lBuffer[lLineStart] == '\t'))
			lLineStart;

		lBuffer = lBuffer.substr(lLineStart);

		if (lBuffer.size() <= 0) continue;
		//if (lBuffer[0] == '[') continue;

		std::vector<std::string> lBufferSplit = SplitString(lBuffer, " ");
		lBufferSplit = SplitString(lBufferSplit, ".");
		lBufferSplit = SplitString(lBufferSplit, "{", true);
		lBufferSplit = SplitString(lBufferSplit, "}", true);
		lBufferSplit = SplitString(lBufferSplit, "[", true);
		lBufferSplit = SplitString(lBufferSplit, "]", true);

		if (lBufferSplit.size() <= 0) continue;

		for (int i = 0; i < lBufferSplit.size(); i++)
		{
			const std::string& lPGNCandidate = lBufferSplit.at(i);
			if (lPGNCandidate.size() <= 0) continue;

			if (lPGNCandidate == "{" || lPGNCandidate == "[")
			{
				lInsideComment = true;
				continue;
			}
			if (lPGNCandidate == "}" || lPGNCandidate == "]")
			{
				lInsideComment = false;
				continue;
			}

			if (lPGNCandidate.size() < 2) continue;
			if (lInsideComment) continue;

			bool lAllNumbers = true;

			for (int k = 0; k < lPGNCandidate.size(); k++)
			{
				const char lChar = lPGNCandidate[k];
				if (lChar < '0' || lChar > '9')
				{
					lAllNumbers = false;
					break;
				}
			}

			if (!lAllNumbers &&
				lPGNCandidate != "1-0" && lPGNCandidate != "0-1" && lPGNCandidate != "1/2-1/2")
				lReturnVector.push_back(lPGNCandidate);
		}
	}

	return lReturnVector;
}