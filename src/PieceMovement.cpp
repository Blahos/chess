
#include "PieceMovement.h"

#include "FunctionsMove.h"
#include "FunctionsCoord.h"
#include "FunctionsBoard.h"
#include "Exception.h"
#include "Functions.h"

#include "BoardStateExtra.h"

thread_local std::vector<Move> PieceMovement::mMoveBuffer;
thread_local std::vector<int> PieceMovement::mIntMoveBuffer;

void PieceMovement::GetPotentialMoves(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<Move>& aMoves, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aStartRow, aStartCol);

	if (!IsIn(aStartRow, aStartCol)) throw EXCEPTION_GLOBAL("Coordinates out of board!");

	const int lSquareIndex = CoToSI(aStartRow, aStartCol);
	const SquareState& lSquare = aBoardState.Squares[lSquareIndex];

	//std::vector<Move> lReturnVector;

	if (!(lSquare.Piece_ & aBoardState.PlayerToMove))
	//if (!lSquare.HasPiece || lSquare.Piece_.Colour_ != aBoardState.PlayerToMove)
	{
		return;
	}

	mIntMoveBuffer.clear();

	if (lSquare.Piece_ & P_PAWN)
		GetPotentialMovesPawn(aBoardState, aStartRow, aStartCol, mIntMoveBuffer, aStopEndSquare);
	else if (lSquare.Piece_ & P_KNIGHT)
		GetPotentialMovesKnight(aBoardState, aStartRow, aStartCol, mIntMoveBuffer, aStopEndSquare);
	else if (lSquare.Piece_ & P_BISHOP)
		GetPotentialMovesBishop(aBoardState, aStartRow, aStartCol, mIntMoveBuffer, aStopEndSquare);
	else if (lSquare.Piece_ & P_ROOK)
		GetPotentialMovesRook(aBoardState, aStartRow, aStartCol, mIntMoveBuffer, aStopEndSquare);
	else if (lSquare.Piece_ & P_QUEEN)
		GetPotentialMovesQueen(aBoardState, aStartRow, aStartCol, mIntMoveBuffer, aStopEndSquare);
	else if (lSquare.Piece_ & P_KING)
		GetPotentialMovesKing(aBoardState, aStartRow, aStartCol, mIntMoveBuffer, aStopEndSquare);
	else throw EXCEPTION_GLOBAL("Unknown piece!");

	for (int i = 0; i < mIntMoveBuffer.size(); i++)
	{
		Move lMove;
		lMove.MovedPiece = lSquare.Piece_;
		lMove.Start = lStartCoord;
		lMove.End = SIToCo(mIntMoveBuffer[i]);

		// check for en passant
		if (lMove.MovedPiece & P_PAWN &&
			lMove.Start.Col != lMove.End.Col &&
			!aBoardState.Squares[mIntMoveBuffer[i]].Piece_)
		{
			lMove.IsEnPassant = true;
		}

		// Handle promotion case, only case where the move is not uniquely described by its coordinates
		if (lMove.MovedPiece & P_PAWN && IsLastRow(lMove.End, lMove.MovedPiece))
		{
			Move lMove1(lMove);
			Move lMove2(lMove);
			Move lMove3(lMove);
			Move lMove4(lMove);
			lMove1.PromotionPiece = P_KNIGHT & aBoardState.PlayerToMove;
			lMove2.PromotionPiece = P_BISHOP & aBoardState.PlayerToMove;
			lMove3.PromotionPiece = P_ROOK & aBoardState.PlayerToMove;
			lMove4.PromotionPiece = P_QUEEN & aBoardState.PlayerToMove;

			aMoves.push_back(lMove1);
			aMoves.push_back(lMove2);
			aMoves.push_back(lMove3);
			aMoves.push_back(lMove4);
		}
		else
		{
			aMoves.push_back(lMove);
		}
	}
}

void PieceMovement::GetPotentialMovesYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol, YieldReturnMoveBase& aYieldReturn, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aStartRow, aStartCol);

	if (!IsIn(aStartRow, aStartCol)) throw EXCEPTION_GLOBAL("Coordinates out of board!");

	const int lSquareIndex = CoToSI(aStartRow, aStartCol);
	const SquareState& lSquare = aBoardState.Squares[lSquareIndex];
	
	if (!(lSquare.Piece_ & aBoardState.PlayerToMove))
	{
		return;
	}

	GetPotentialMovesYieldReturnWrapper lWrapper(lSquare, lStartCoord, aBoardState, aYieldReturn, (bool)(lSquare.Piece_ & P_PAWN));

	if (lSquare.Piece_ & P_PAWN)
	{
		GetPotentialMovesPawnYield(aBoardState, aStartRow, aStartCol, lWrapper, aStopEndSquare);
	}
	else
	{
		if (lSquare.Piece_ & P_KNIGHT)
			GetPotentialMovesKnightYield(aBoardState, aStartRow, aStartCol, lWrapper, aStopEndSquare);
		else if (lSquare.Piece_ & P_BISHOP)
			GetPotentialMovesBishopYield(aBoardState, aStartRow, aStartCol, lWrapper, aStopEndSquare);
		else if (lSquare.Piece_ & P_ROOK)
			GetPotentialMovesRookYield(aBoardState, aStartRow, aStartCol, lWrapper, aStopEndSquare);
		else if (lSquare.Piece_ & P_QUEEN)
			GetPotentialMovesQueenYield(aBoardState, aStartRow, aStartCol, lWrapper, aStopEndSquare);
		else if (lSquare.Piece_ & P_KING)
			GetPotentialMovesKingYield(aBoardState, aStartRow, aStartCol, lWrapper, aStopEndSquare);
		else throw EXCEPTION_GLOBAL("Unknown piece!");
	}
}

bool PieceMovement::CanPotentiallyMoveTo(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord)
{
	if (aStartCoord == aTargetCoord) return false;
	if (!IsIn(aStartCoord)) throw EXCEPTION_GLOBAL("Not in!");
	if (!IsIn(aTargetCoord)) throw EXCEPTION_GLOBAL("Not in!");
	const int lStartInd = CoToSI(aStartCoord);

	const SquareState& lStartSquare = aBoardState.Squares[lStartInd];

	if (!(lStartSquare.Piece_ & aBoardState.PlayerToMove)) return false;

	const int lEndInd = CoToSI(aTargetCoord);
	const SquareState& lEndSquare = aBoardState.Squares[lEndInd];
	if (lEndSquare.Piece_ & aBoardState.PlayerToMove) return false;

	if (lStartSquare.Piece_ & P_PAWN)
		return CanPotentiallyMoveToPawn(aBoardState, aStartCoord, aTargetCoord);
	else if (lStartSquare.Piece_ & P_KNIGHT)
		return CanPotentiallyMoveToKnight(aBoardState, aStartCoord, aTargetCoord);
	else if (lStartSquare.Piece_ & P_BISHOP)
		return CanPotentiallyMoveToBishop(aBoardState, aStartCoord, aTargetCoord);
	else if (lStartSquare.Piece_ & P_ROOK)
		return CanPotentiallyMoveToRook(aBoardState, aStartCoord, aTargetCoord);
	else if (lStartSquare.Piece_ & P_QUEEN)
		return CanPotentiallyMoveToQueen(aBoardState, aStartCoord, aTargetCoord);
	else if (lStartSquare.Piece_ & P_KING)
		return CanPotentiallyMoveToKing(aBoardState, aStartCoord, aTargetCoord);
	else throw EXCEPTION_GLOBAL("Unknown piece!");
}

void PieceMovement::GetLegalMoves(const BoardState& aBoardState, const int aStartRow, const int aStartCol, StaticList<Move, PieceMovement::cMaxLegalMoves>& aMoves, const std::optional<BoardStateExtra>& aExtraInfo)
{
	const Colour lMovingPlayer = aBoardState.PlayerToMove;
	const Colour lOppositePlayer = GetOppositeColour(lMovingPlayer);

	mMoveBuffer.clear();

	GetPotentialMoves(aBoardState, aStartRow, aStartCol, mMoveBuffer);

	const Piece lMyKing = P_KING & lMovingPlayer;
	const BoardCoordinates& lMyKingNow = (lMovingPlayer & C_WHITE ? aBoardState.WhiteKing : aBoardState.BlackKing);

	BoardState lOppositePlayerToMoveBoard(aBoardState);
	lOppositePlayerToMoveBoard.PlayerToMove = GetOppositeColour(aBoardState.PlayerToMove);

	for (int iMove = 0; iMove < mMoveBuffer.size(); iMove++)
	{
		const Move& lPotentialMove = mMoveBuffer.at(iMove);

		bool lCanCreateCheck = true;

		if (aExtraInfo)
		{
			lCanCreateCheck = aExtraInfo->IsMovingPlayerInCheck;
		}

		if (lPotentialMove.MovedPiece & lMyKing) lCanCreateCheck = true;

		if (!lCanCreateCheck)
		{
			// if the king is not in check and if the move is not with a king, a check can only be created if a piece "discovers" check on it's own king,
			// this can only happen if the piece is on the same row, column or diagonal as the king

			const char lDiffFromKingRow = lPotentialMove.Start.Row - lMyKingNow.Row;
			const char lDiffFromKingCol = lPotentialMove.Start.Col - lMyKingNow.Col;

			if (lDiffFromKingRow == 0 || lDiffFromKingCol == 0 || std::abs(lDiffFromKingRow) == std::abs(lDiffFromKingCol))
				lCanCreateCheck = true;
		}

		if (lCanCreateCheck)
		{
			// we have to make the move and check if the king got into a check
			const BoardState lBoardAfterMove = aBoardState.MakeMoveRaw(lPotentialMove);

			//const Piece lMyKing = P_KING & lMovingPlayer;
			//const BoardCoordinates& lMyKingNow = (lMovingPlayer & C_WHITE ? aBoardState.WhiteKing : aBoardState.BlackKing);
			const BoardCoordinates& lMyKingAfterMove = (lMovingPlayer & C_WHITE ? lBoardAfterMove.WhiteKing : lBoardAfterMove.BlackKing);
			if (!IsIn(lMyKingAfterMove)) throw EXCEPTION_GLOBAL("King not found!");

			// if our king can be captured after this move then it is not legal
			//if (CanBeMovedTo(lBoardAfterMove, lMyKingAfterMove)) continue;
			if (IsAttacked(lBoardAfterMove, lMyKingAfterMove)) continue;

			// is it a castle? If it is, do extra checks
			if (lPotentialMove.IsCastle())
			{
				// is king currently in check?
				BoardState lCurrentBoardOtherPlayer(aBoardState);
				lCurrentBoardOtherPlayer.PlayerToMove = lOppositePlayer;

				//if (CanBeMovedTo(lCurrentBoardOtherPlayer, lMyKingNow)) continue;
				if (IsAttacked(lCurrentBoardOtherPlayer, lMyKingNow)) continue;

				// is king crossing a checked square (there is always only one basically, other than the start and end ones)
				const int lMiddleCol = (lPotentialMove.End.Col + lPotentialMove.Start.Col) / 2;

				// move the king to the middle square
				BoardCoordinates lMiddleSquare(lPotentialMove.Start.Row, lMiddleCol);
				lCurrentBoardOtherPlayer.Squares[CoToSI(lMyKingNow)].Piece_ = P_NONE;
				lCurrentBoardOtherPlayer.Squares[CoToSI(lMiddleSquare)].Piece_ = lMyKing;

				//lCurrentBoardOtherPlayer.RebuildPieceList();

				if (CanBeMovedTo(lCurrentBoardOtherPlayer, lMiddleSquare)) continue;
			}
		}

		// the move is accepted as legal
		aMoves.Items[aMoves.ActualSize++] = lPotentialMove;

		if (aMoves.ActualSize > PieceMovement::cMaxLegalMoves) throw EXCEPTION_GLOBAL("Static array not large enough!");
	}

	return;
}

void PieceMovement::GetLegalMovesWithBoards(const BoardState& aBoardState, const int aStartRow, const int aStartCol, StaticList<MoveBoard, cMaxLegalMoves>& aMoves, const std::optional<BoardStateExtra>& aExtraInfo)
{
	const Colour lMovingPlayer = aBoardState.PlayerToMove;
	const Colour lOppositePlayer = GetOppositeColour(lMovingPlayer);

	mMoveBuffer.clear();

	GetPotentialMoves(aBoardState, aStartRow, aStartCol, mMoveBuffer);

	const Piece lMyKing = P_KING & lMovingPlayer;
	const BoardCoordinates& lMyKingNow = (lMovingPlayer & C_WHITE ? aBoardState.WhiteKing : aBoardState.BlackKing);

	BoardState lOppositePlayerToMoveBoard(aBoardState);
	lOppositePlayerToMoveBoard.PlayerToMove = GetOppositeColour(aBoardState.PlayerToMove);

	MoveBoard lNewMove;

	BoardState lBoardAfterMove;
	bool lMoveMade = false;

	for (int iMove = 0; iMove < mMoveBuffer.size(); iMove++)
	{
		const Move& lPotentialMove = mMoveBuffer.at(iMove);

		bool lCanCreateCheck = true;

		if (aExtraInfo)
		{
			lCanCreateCheck = aExtraInfo->IsMovingPlayerInCheck;
		}

		if (lPotentialMove.MovedPiece & lMyKing) lCanCreateCheck = true;

		if (!lCanCreateCheck)
		{
			// if the king is not in check and if the move is not with a king, a check can only be created if a piece "discovers" check on it's own king,
			// this can only happen if the piece is on the same row, column or diagonal as the king

			const char lDiffFromKingRow = lPotentialMove.Start.Row - lMyKingNow.Row;
			const char lDiffFromKingCol = lPotentialMove.Start.Col - lMyKingNow.Col;

			if (lDiffFromKingRow == 0 || lDiffFromKingCol == 0 || std::abs(lDiffFromKingRow) == std::abs(lDiffFromKingCol))
				lCanCreateCheck = true;
		}

		if (lCanCreateCheck)
		{
			// we have to make the move and check if the king got into a check
			lBoardAfterMove = aBoardState.MakeMoveRaw(lPotentialMove);
			lMoveMade = true;

			//const Piece lMyKing = P_KING & lMovingPlayer;
			//const BoardCoordinates& lMyKingNow = (lMovingPlayer & C_WHITE ? aBoardState.WhiteKing : aBoardState.BlackKing);
			const BoardCoordinates& lMyKingAfterMove = (lMovingPlayer & C_WHITE ? lBoardAfterMove.WhiteKing : lBoardAfterMove.BlackKing);
			if (!IsIn(lMyKingAfterMove)) throw EXCEPTION_GLOBAL("King not found!");

			// if our king can be captured after this move then it is not legal
			//if (CanBeMovedTo(lBoardAfterMove, lMyKingAfterMove)) continue;
			if (IsAttacked(lBoardAfterMove, lMyKingAfterMove)) continue;

			// is it a castle? If it is, do extra checks
			if (lPotentialMove.IsCastle())
			{
				// is king currently in check?
				BoardState lCurrentBoardOtherPlayer(aBoardState);
				lCurrentBoardOtherPlayer.PlayerToMove = lOppositePlayer;

				//if (CanBeMovedTo(lCurrentBoardOtherPlayer, lMyKingNow)) continue;
				if (IsAttacked(lCurrentBoardOtherPlayer, lMyKingNow)) continue;

				// is king crossing a checked square (there is always only one basically, other than the start and end ones)
				const int lMiddleCol = (lPotentialMove.End.Col + lPotentialMove.Start.Col) / 2;

				// move the king to the middle square
				BoardCoordinates lMiddleSquare(lPotentialMove.Start.Row, lMiddleCol);
				lCurrentBoardOtherPlayer.Squares[CoToSI(lMyKingNow)].Piece_ = P_NONE;
				lCurrentBoardOtherPlayer.Squares[CoToSI(lMiddleSquare)].Piece_ = lMyKing;

				//lCurrentBoardOtherPlayer.RebuildPieceList();

				if (CanBeMovedTo(lCurrentBoardOtherPlayer, lMiddleSquare)) continue;
			}
		}

		// the move is accepted as legal

		lNewMove.Move_ = lPotentialMove;

		if (lMoveMade)
			lNewMove.Board = lBoardAfterMove;
		else
			lNewMove.Board = aBoardState.MakeMoveRaw(lPotentialMove);

		aMoves.Items[aMoves.ActualSize++] = lNewMove;
		if (aMoves.ActualSize > PieceMovement::cMaxLegalMoves) throw EXCEPTION_GLOBAL("Static array not large enough!");
	}

	return;
}

void PieceMovement::GetAllLegalMoves(const BoardState& aBoardState, StaticList<Move, PieceMovement::cMaxLegalMoves>& aMoves, const std::optional<BoardStateExtra>& aExtraInfo)
{
	for (int iSquare = 0; iSquare < BoardSetup::cSquareCount; iSquare++)
	{
		const SquareState& lSquare = aBoardState.Squares[iSquare];
		if (!(lSquare.Piece_ & aBoardState.PlayerToMove)) continue;

		GetLegalMoves(aBoardState, lSquare.Coordinates.Row, lSquare.Coordinates.Col, aMoves, aExtraInfo);
	}
}


void PieceMovement::GetAllLegalMovesWithBoards(const BoardState& aBoardState, StaticList<MoveBoard, cMaxLegalMoves>& aMoves, const std::optional<BoardStateExtra>& aExtraInfo)
{
	for (int iSquare = 0; iSquare < BoardSetup::cSquareCount; iSquare++)
	{
		const SquareState& lSquare = aBoardState.Squares[iSquare];
		if (!(lSquare.Piece_ & aBoardState.PlayerToMove)) continue;

		GetLegalMovesWithBoards(aBoardState, lSquare.Coordinates.Row, lSquare.Coordinates.Col, aMoves, aExtraInfo);
		//GetLegalMoves(aBoardState, lSquare.Coordinates.Row, lSquare.Coordinates.Col, aMoves);
	}
}

bool PieceMovement::AnyLegalMoves(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<Move>& aMoveBuffer)
{
	const Colour lMovingPlayer = aBoardState.PlayerToMove;
	const Colour lOppositePlayer = GetOppositeColour(lMovingPlayer);

	aMoveBuffer.clear();

	GetPotentialMoves(aBoardState, aStartRow, aStartCol, aMoveBuffer);

	for (int iMove = 0; iMove < aMoveBuffer.size(); iMove++)
	{
		const Move& lPotentialMove = aMoveBuffer.at(iMove);

		const BoardState lBoardAfterMove = aBoardState.MakeMoveRaw(lPotentialMove);

		const Piece lMyKing = P_KING & lMovingPlayer;
		const BoardCoordinates& lMyKingNow = ((lMovingPlayer & C_WHITE) ? aBoardState.WhiteKing : aBoardState.BlackKing);
		const BoardCoordinates& lMyKingAfterMove = ((lMovingPlayer & C_WHITE) ? lBoardAfterMove.WhiteKing : lBoardAfterMove.BlackKing);
		if (!IsIn(lMyKingAfterMove)) throw EXCEPTION_GLOBAL("King not found!");

		// if our king can be captured after this move then it is not legal
		//if (CanBeMovedTo(lBoardAfterMove, lMyKingAfterMove)) continue;
		if (IsAttacked(lBoardAfterMove, lMyKingAfterMove)) continue;

		// is it a castle? If it is, do extra checks
		if (lPotentialMove.IsCastle())
		{
			// is king currently in check?
			BoardState lCurrentBoardOtherPlayer(aBoardState);
			lCurrentBoardOtherPlayer.PlayerToMove = lOppositePlayer;

			if (CanBeMovedTo(lCurrentBoardOtherPlayer, lMyKingNow)) continue;

			// is king crossing a checked square (there is always only one basically, other than the start and end ones)
			const int lMiddleCol = (lPotentialMove.End.Col + lPotentialMove.Start.Col) / 2;

			// move the king to the middle square
			BoardCoordinates lMiddleSquare(lPotentialMove.Start.Row, lMiddleCol);
			lCurrentBoardOtherPlayer.Squares[CoToSI(lMyKingNow)].Piece_ = P_NONE;
			lCurrentBoardOtherPlayer.Squares[CoToSI(lMiddleSquare)].Piece_ = lMyKing;

			//lCurrentBoardOtherPlayer.RebuildPieceList();

			if (CanBeMovedTo(lCurrentBoardOtherPlayer, lMiddleSquare)) continue;
		}

		// the move is accepted as legal
		return true;
	}

	return false;
}

bool PieceMovement::AnyLegalMovesYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol)
{
	AnyLegalMovesYieldReturnWrapper lWrapper(aBoardState);

	GetPotentialMovesYield(aBoardState, aStartRow, aStartCol, lWrapper);

	return lWrapper.LegalMoveFound;
}

bool PieceMovement::AnyLegalMoves(const BoardState& aBoardState, std::vector<Move>& aMoveBuffer)
{
	for (int iSquare = 0; iSquare < BoardSetup::cSquareCount; iSquare++)
	{
		const SquareState& lSquare = aBoardState.Squares[iSquare];
		if (!(lSquare.Piece_ & aBoardState.PlayerToMove)) continue;

		if (IsBlocked(aBoardState, iSquare)) continue;

		//const bool lAny = AnyLegalMoves(aBoardState, lSquare.Coordinates.Row, lSquare.Coordinates.Col, aMoveBuffer);
		const bool lAny = AnyLegalMovesYield(aBoardState, lSquare.Coordinates.Row, lSquare.Coordinates.Col);

		if (lAny) return true;
	}

	return false;
}

void PieceMovement::GetPotentialMovesPawn(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<int>& aMoves, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aStartRow, aStartCol);
	const int lSquareIndex = CoToSI(lStartCoord);
	const SquareState& lStartSquare = aBoardState.Squares[lSquareIndex];
	const Colour lOppositeColour = GetOppositeColour(lStartSquare.Piece_);

	const int lForwardDir = (lStartSquare.Piece_ & C_WHITE) ? 1 : -1;

	// check move forward by one
	BoardCoordinates lForwardOne(aStartRow + lForwardDir, aStartCol);

	if (IsIn(lForwardOne) && IsSquareEmpty(aBoardState, lForwardOne))
	{
		aMoves.push_back(CoToSI(lForwardOne));

		if (lForwardOne == aStopEndSquare) return;
	}

	// check move forward by two
	if (GetRelativeRow(lStartCoord, lStartSquare.Piece_) == 1)
	{
		BoardCoordinates lForwardTwo(aStartRow + 2 * lForwardDir, aStartCol);

		if (IsIn(lForwardTwo) && IsSquareEmpty(aBoardState, lForwardOne) && IsSquareEmpty(aBoardState, lForwardTwo))
		{
			aMoves.push_back(CoToSI(lForwardTwo));

			if (lForwardTwo == aStopEndSquare) return;
		}
	}

	// check captures (normal)
	BoardCoordinates lCapture1(aStartRow + lForwardDir, aStartCol - 1);
	BoardCoordinates lCapture2(aStartRow + lForwardDir, aStartCol + 1);

	if (IsIn(lCapture1) && HasPieceOfColour(aBoardState, lCapture1, lOppositeColour))
	{
		aMoves.push_back(CoToSI(lCapture1));
	}

	if (IsIn(lCapture2) && HasPieceOfColour(aBoardState, lCapture2, lOppositeColour))
	{
		aMoves.push_back(CoToSI(lCapture2));
	}

	// check captures (en passant)
	if (GetRelativeRow(lStartCoord, lStartSquare.Piece_) == 4)
	{
		for (char iColShift = -1; iColShift <= 1; iColShift += 2)
		{
			// move end squares
			BoardCoordinates lEnPassant(aStartRow + lForwardDir, aStartCol + iColShift);
			if (!IsIn(lEnPassant)) continue;

			// squares next to current start square, where the passing pawn would end
			BoardCoordinates lEnPassantSide(aStartRow, aStartCol + iColShift);
			if (!IsIn(lEnPassantSide)) continue;

			// side squares for the previous move, where the passing pawn would come from
			BoardCoordinates lEnPassantSidePrev(aStartRow + 2 * lForwardDir, aStartCol + iColShift);
			if (!IsIn(lEnPassantSidePrev)) continue;

			int lEnPassantInd = CoToSI(lEnPassant);

			int lEnPassantSideInd = CoToSI(lEnPassantSide);

			int lEnPassantSidePrevInd = CoToSI(lEnPassantSidePrev);

			//const SquareState& lPrevSquare = aBoardState.PreviousSquares[lEnPassantSidePrevInd];
			//const bool lHasPreviousPawnOn2ndRow = aBoardState.HasPreviousPawnOn2ndRow(aStartCol + iColShift, lOppositeColour);
			//const bool lHasNowPawnOn2ndRow = aBoardState.Squares[lEnPassantSidePrevInd].Piece_ & (P_PAWN & lOppositeColour);
			//const SquareState& lNowSquare = aBoardState.Squares[lEnPassantSideInd];

			if (IsIn(lEnPassant) && aBoardState.WasLastMoveDoublePawn(lOppositeColour, aStartCol + iColShift))
			{
				aMoves.push_back(CoToSI(lEnPassant));
				if (lEnPassant == aStopEndSquare) return;
			}
		}
	}
}

void PieceMovement::GetPotentialMovesKnight(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<int>& aMoves, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aStartRow, aStartCol);
	const int lSquareIndex = CoToSI(lStartCoord);
	const SquareState& lStartSquare = aBoardState.Squares[lSquareIndex];
	const Colour lOppositeColour = GetOppositeColour(lStartSquare.Piece_);

	const int cMoveCount = 8;
	const int cRowDiff[] = { -2, -1, 1, 2, 2, 1, -1, -2 };
	const int cColDiff[] = { -1, -2, -2, -1, 1, 2, 2, 1 };

	for (int i = 0; i < cMoveCount; i++)
	{
		BoardCoordinates lEndCoord(aStartRow + cRowDiff[i], aStartCol + cColDiff[i]);
		const int lEndSquareInd = CoToSI(lEndCoord);
		if (IsIn(lEndCoord) && (IsSquareEmpty(aBoardState, lEndCoord) || HasPieceOfColour(aBoardState, lEndCoord, lOppositeColour)))
		{
			aMoves.push_back(lEndSquareInd);
			if (lEndCoord == aStopEndSquare) return;
		}
	}
}

void PieceMovement::GetPotentialMovesBishop(const BoardState& aBoardState, const int aRow, const int aStartCol, std::vector<int>& aMoves, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aRow, aStartCol);
	const int lSquareIndex = CoToSI(lStartCoord);
	const SquareState& lStartSquare = aBoardState.Squares[lSquareIndex];
	const Colour lOppositeColour = GetOppositeColour(lStartSquare.Piece_);

	const int cDirCount = 4;
	const int cRowDir[] = { -1, 1, 1, -1 };
	const int cColDir[] = { -1, -1, 1, 1 };

	for (int iDir = 0; iDir < cDirCount; iDir++)
	{
		int lDirStep = 1;

		while (true)
		{
			const BoardCoordinates lEndCoord(aRow + lDirStep * cRowDir[iDir], aStartCol + lDirStep * cColDir[iDir]);

			if (!IsIn(lEndCoord)) break;

			const int lEndSquareInd = CoToSI(lEndCoord);
			if (IsSquareEmpty(aBoardState, lEndCoord))
			{
				aMoves.push_back(lEndSquareInd);
				if (lEndCoord == aStopEndSquare) return;
			}
			else if (HasPieceOfColour(aBoardState, lEndCoord, lOppositeColour))
			{
				aMoves.push_back(lEndSquareInd);
				if (lEndCoord == aStopEndSquare) return;
				break;
			}
			else break;

			lDirStep++;
		}
	}
}

void PieceMovement::GetPotentialMovesRook(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<int>& aMoves, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aStartRow, aStartCol);
	const int lSquareIndex = CoToSI(lStartCoord);
	const SquareState& lStartSquare = aBoardState.Squares[lSquareIndex];
	const Colour lOppositeColour = GetOppositeColour(lStartSquare.Piece_);

	const int cDirCount = 4;
	const int cRowDir[] = { -1,  0, 1, 0 };
	const int cColDir[] = { 0, -1, 0, 1 };

	for (int iDir = 0; iDir < cDirCount; iDir++)
	{
		int lDirStep = 1;

		while (true)
		{
			const BoardCoordinates lEndCoord(aStartRow + lDirStep * cRowDir[iDir], aStartCol + lDirStep * cColDir[iDir]);

			if (!IsIn(lEndCoord)) break;

			const int lEndSquareInd = CoToSI(lEndCoord);
			if (IsSquareEmpty(aBoardState, lEndCoord))
			{
				aMoves.push_back(lEndSquareInd);
				if (lEndCoord == aStopEndSquare) return;
			}
			else if (HasPieceOfColour(aBoardState, lEndCoord, lOppositeColour))
			{
				aMoves.push_back(lEndSquareInd);
				if (lEndCoord == aStopEndSquare) return;
				break;
			}
			else break;

			lDirStep++;
		}
	}
}

void PieceMovement::GetPotentialMovesQueen(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<int>& aMoves, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aStartRow, aStartCol);
	const int lSquareIndex = CoToSI(lStartCoord);
	const SquareState& lStartSquare = aBoardState.Squares[lSquareIndex];
	const Colour lOppositeColour = GetOppositeColour(lStartSquare.Piece_);

	const int cDirCount = 8;
	const int cRowDir[] = { -1, -1,  0,  1, 1, 1, 0, -1 };
	const int cColDir[] = { 0, -1, -1, -1, 0, 1, 1,  1 };

	for (int iDir = 0; iDir < cDirCount; iDir++)
	{
		int lDirStep = 1;

		while (true)
		{
			const BoardCoordinates lEndCoord(aStartRow + lDirStep * cRowDir[iDir], aStartCol + lDirStep * cColDir[iDir]);

			if (!IsIn(lEndCoord)) break;

			const int lEndSquareInd = CoToSI(lEndCoord);
			if (IsSquareEmpty(aBoardState, lEndCoord))
			{
				aMoves.push_back(lEndSquareInd);
				if (lEndCoord == aStopEndSquare) return;
			}
			else if (HasPieceOfColour(aBoardState, lEndCoord, lOppositeColour))
			{
				aMoves.push_back(lEndSquareInd);
				if (lEndCoord == aStopEndSquare) return;
				break;
			}
			else break;

			lDirStep++;
		}
	}
}

void PieceMovement::GetPotentialMovesKing(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<int>& aMoves, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aStartRow, aStartCol);
	const int lSquareIndex = CoToSI(lStartCoord);
	const SquareState& lStartSquare = aBoardState.Squares[lSquareIndex];
	const Colour lOppositeColour = GetOppositeColour(lStartSquare.Piece_);

	const int cDirCount = 8;
	const int cRowDir[] = { -1, -1,  0,  1, 1, 1, 0, -1 };
	const int cColDir[] = { 0, -1, -1, -1, 0, 1, 1,  1 };

	for (int iDir = 0; iDir < cDirCount; iDir++)
	{
		const BoardCoordinates lEndCoord(aStartRow + cRowDir[iDir], aStartCol + cColDir[iDir]);

		if (!IsIn(lEndCoord)) continue;

		const int lEndSquareInd = CoToSI(lEndCoord);
		if (IsSquareEmpty(aBoardState, lEndCoord))
		{
			aMoves.push_back(lEndSquareInd);
			if (lEndCoord == aStopEndSquare) return;
		}
		else if (HasPieceOfColour(aBoardState, lEndCoord, lOppositeColour))
		{
			aMoves.push_back(lEndSquareInd);
			if (lEndCoord == aStopEndSquare) return;
		}
	}

	// check for castling
	if (lStartSquare.Piece_ & P_KING)
	{
		if (lStartCoord.Col == 4 && GetRelativeRow(lStartCoord, aBoardState.PlayerToMove) == 0)
		{
			const int lStartRow = lStartCoord.Row;
			const int lStartCol = lStartCoord.Col;

			// king side
			if (((aBoardState.PlayerToMove & C_WHITE) && aBoardState.CanCastleKingsideWhite)
				||
				((aBoardState.PlayerToMove & C_BLACK) && aBoardState.CanCastleKingsideBlack))
			{
				const BoardCoordinates lRookCoord(lStartRow, lStartCol + 3);
				const char lRookSquareIndex = CoToSI(lRookCoord);

				if (IsSquareEmpty(aBoardState, lStartRow, lStartCol + 1) &&
					IsSquareEmpty(aBoardState, lStartRow, lStartCol + 2) &&
					aBoardState.Squares[lRookSquareIndex].Piece_ == (P_ROOK & aBoardState.PlayerToMove))
				{
					aMoves.push_back(CoToSI(lStartRow, lStartCol + 2));
					if (BoardCoordinates(lStartRow, lStartCol + 2) == aStopEndSquare) return;
				}
			}

			// queen side
			if (((aBoardState.PlayerToMove & C_WHITE) && aBoardState.CanCastleQueensideWhite)
				||
				((aBoardState.PlayerToMove & C_BLACK) && aBoardState.CanCastleQueensideBlack))
			{
				const BoardCoordinates lRookCoord(lStartRow, lStartCol - 4);
				const char lRookSquareIndex = CoToSI(lRookCoord);

				if (IsSquareEmpty(aBoardState, lStartRow, lStartCol - 1) &&
					IsSquareEmpty(aBoardState, lStartRow, lStartCol - 2) &&
					IsSquareEmpty(aBoardState, lStartRow, lStartCol - 3) &&
					aBoardState.Squares[lRookSquareIndex].Piece_ == (P_ROOK & aBoardState.PlayerToMove))
				{
					aMoves.push_back(CoToSI(lStartRow, lStartCol - 2));
					if (BoardCoordinates(lStartRow, lStartCol - 2) == aStopEndSquare) return;
				}
			}
		}
	}
}


void PieceMovement::GetPotentialMovesPawnYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol, PieceMovement::YieldReturnMoveIntBase& aYieldReturn, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aStartRow, aStartCol);
	const int lSquareIndex = CoToSI(lStartCoord);
	const SquareState& lStartSquare = aBoardState.Squares[lSquareIndex];
	const Colour lOppositeColour = GetOppositeColour(lStartSquare.Piece_);

	const int lForwardDir = (lStartSquare.Piece_ & C_WHITE) ? 1 : -1;

	// check move forward by one
	BoardCoordinates lForwardOne(aStartRow + lForwardDir, aStartCol);

	if (IsIn(lForwardOne) && IsSquareEmpty(aBoardState, lForwardOne))
	{
		const bool lBreak = aYieldReturn(CoToSI(lForwardOne));

		if (lBreak)
			return;

		if (lForwardOne == aStopEndSquare) return;
	}

	// check move forward by two
	if (GetRelativeRow(lStartCoord, lStartSquare.Piece_) == 1)
	{
		BoardCoordinates lForwardTwo(aStartRow + 2 * lForwardDir, aStartCol);

		if (IsIn(lForwardTwo) && IsSquareEmpty(aBoardState, lForwardOne) && IsSquareEmpty(aBoardState, lForwardTwo))
		{
			const bool lBreak = aYieldReturn(CoToSI(lForwardTwo));

			if (lBreak)
			{
				return;
			}

			if (lForwardTwo == aStopEndSquare) return;
		}
	}

	// check captures (normal)
	BoardCoordinates lCapture1(aStartRow + lForwardDir, aStartCol - 1);
	BoardCoordinates lCapture2(aStartRow + lForwardDir, aStartCol + 1);

	if (IsIn(lCapture1) && HasPieceOfColour(aBoardState, lCapture1, lOppositeColour))
	{
		const bool lBreak = aYieldReturn(CoToSI(lCapture1));

		if (lBreak)
			return;
	}

	if (IsIn(lCapture2) && HasPieceOfColour(aBoardState, lCapture2, lOppositeColour))
	{
		const bool lBreak = aYieldReturn(CoToSI(lCapture2));

		if (lBreak)
			return;
	}

	// check captures (en passant)
	if (GetRelativeRow(lStartCoord, lStartSquare.Piece_) == 4)
	{
		for (char iColShift = -1; iColShift <= 1; iColShift += 2)
		{
			// move end squares
			BoardCoordinates lEnPassant(aStartRow + lForwardDir, aStartCol + iColShift);
			if (!IsIn(lEnPassant)) continue;

			// squares next to current start square, where the passing pawn would end
			BoardCoordinates lEnPassantSide(aStartRow, aStartCol + iColShift);
			if (!IsIn(lEnPassantSide)) continue;

			// side squares for the previous move, where the passing pawn would come from
			BoardCoordinates lEnPassantSidePrev(aStartRow + 2 * lForwardDir, aStartCol + iColShift);
			if (!IsIn(lEnPassantSidePrev)) continue;

			int lEnPassantInd = CoToSI(lEnPassant);

			int lEnPassantSideInd = CoToSI(lEnPassantSide);

			int lEnPassantSidePrevInd = CoToSI(lEnPassantSidePrev);

			//const SquareState& lPrevSquare = aBoardState.PreviousSquares[lEnPassantSidePrevInd];
			//const bool lHasPrevousPawnOn2ndRow = aBoardState.HasPreviousPawnOn2ndRow(aStartCol + iColShift, lOppositeColour);
			//const SquareState& lNowSquare = aBoardState.Squares[lEnPassantSideInd];

			if (IsIn(lEnPassant) && aBoardState.WasLastMoveDoublePawn(lOppositeColour, aStartCol + iColShift))
			{
				const bool lBreak = aYieldReturn(CoToSI(lEnPassant));

				if (lBreak)
					return;

				if (lEnPassant == aStopEndSquare) return;
			}
		}
	}
}

void PieceMovement::GetPotentialMovesKnightYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol, PieceMovement::YieldReturnMoveIntBase& aYieldReturn, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aStartRow, aStartCol);
	const int lSquareIndex = CoToSI(lStartCoord);
	const SquareState& lStartSquare = aBoardState.Squares[lSquareIndex];
	const Colour lOppositeColour = GetOppositeColour(lStartSquare.Piece_);

	const int cMoveCount = 8;
	const int cRowDiff[] = { -2, -1, 1, 2, 2, 1, -1, -2 };
	const int cColDiff[] = { -1, -2, -2, -1, 1, 2, 2, 1 };

	for (int i = 0; i < cMoveCount; i++)
	{
		BoardCoordinates lEndCoord(aStartRow + cRowDiff[i], aStartCol + cColDiff[i]);
		const int lEndSquareInd = CoToSI(lEndCoord);
		if (IsIn(lEndCoord) && (IsSquareEmpty(aBoardState, lEndCoord) || HasPieceOfColour(aBoardState, lEndCoord, lOppositeColour)))
		{
			const bool lBreak = aYieldReturn(lEndSquareInd);

			if (lBreak)
				return;

			if (lEndCoord == aStopEndSquare) return;
		}
	}
}

void PieceMovement::GetPotentialMovesBishopYield(const BoardState& aBoardState, const int aRow, const int aStartCol, PieceMovement::YieldReturnMoveIntBase& aYieldReturn, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aRow, aStartCol);
	const int lSquareIndex = CoToSI(lStartCoord);
	const SquareState& lStartSquare = aBoardState.Squares[lSquareIndex];
	const Colour lOppositeColour = GetOppositeColour(lStartSquare.Piece_);

	const int cDirCount = 4;
	const int cRowDir[] = { -1, 1, 1, -1 };
	const int cColDir[] = { -1, -1, 1, 1 };

	for (int iDir = 0; iDir < cDirCount; iDir++)
	{
		int lDirStep = 1;

		while (true)
		{
			const BoardCoordinates lEndCoord(aRow + lDirStep * cRowDir[iDir], aStartCol + lDirStep * cColDir[iDir]);

			if (!IsIn(lEndCoord)) break;

			const int lEndSquareInd = CoToSI(lEndCoord);
			if (IsSquareEmpty(aBoardState, lEndCoord))
			{
				const bool lBreak = aYieldReturn(lEndSquareInd);

				if (lBreak)
					return;

				if (lEndCoord == aStopEndSquare) return;
			}
			else if (HasPieceOfColour(aBoardState, lEndCoord, lOppositeColour))
			{
				const bool lBreak = aYieldReturn(lEndSquareInd);

				if (lBreak)
					return;

				if (lEndCoord == aStopEndSquare) return;
				break;
			}
			else break;

			lDirStep++;
		}
	}
}

void PieceMovement::GetPotentialMovesRookYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol, PieceMovement::YieldReturnMoveIntBase& aYieldReturn, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aStartRow, aStartCol);
	const int lSquareIndex = CoToSI(lStartCoord);
	const SquareState& lStartSquare = aBoardState.Squares[lSquareIndex];
	const Colour lOppositeColour = GetOppositeColour(lStartSquare.Piece_);

	const int cDirCount = 4;
	const int cRowDir[] = { -1,  0, 1, 0 };
	const int cColDir[] = { 0, -1, 0, 1 };

	for (int iDir = 0; iDir < cDirCount; iDir++)
	{
		int lDirStep = 1;

		while (true)
		{
			const BoardCoordinates lEndCoord(aStartRow + lDirStep * cRowDir[iDir], aStartCol + lDirStep * cColDir[iDir]);

			if (!IsIn(lEndCoord)) break;

			const int lEndSquareInd = CoToSI(lEndCoord);
			if (IsSquareEmpty(aBoardState, lEndCoord))
			{
				const bool lBreak = aYieldReturn(lEndSquareInd);

				if (lBreak)
					return;

				if (lEndCoord == aStopEndSquare) return;
			}
			else if (HasPieceOfColour(aBoardState, lEndCoord, lOppositeColour))
			{
				const bool lBreak = aYieldReturn(lEndSquareInd);

				if (lBreak)
					return;

				if (lEndCoord == aStopEndSquare) return;
				break;
			}
			else break;

			lDirStep++;
		}
	}
}

void PieceMovement::GetPotentialMovesQueenYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol, PieceMovement::YieldReturnMoveIntBase& aYieldReturn, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aStartRow, aStartCol);
	const int lSquareIndex = CoToSI(lStartCoord);
	const SquareState& lStartSquare = aBoardState.Squares[lSquareIndex];
	const Colour lOppositeColour = GetOppositeColour(lStartSquare.Piece_);

	const int cDirCount = 8;
	const int cRowDir[] = { -1, -1,  0,  1, 1, 1, 0, -1 };
	const int cColDir[] = { 0, -1, -1, -1, 0, 1, 1,  1 };

	for (int iDir = 0; iDir < cDirCount; iDir++)
	{
		int lDirStep = 1;

		while (true)
		{
			const BoardCoordinates lEndCoord(aStartRow + lDirStep * cRowDir[iDir], aStartCol + lDirStep * cColDir[iDir]);

			if (!IsIn(lEndCoord)) break;

			const int lEndSquareInd = CoToSI(lEndCoord);
			if (IsSquareEmpty(aBoardState, lEndCoord))
			{
				const bool lBreak = aYieldReturn(lEndSquareInd);

				if (lBreak)
				{
					return;
				}

				if (lEndCoord == aStopEndSquare) return;
			}
			else if (HasPieceOfColour(aBoardState, lEndCoord, lOppositeColour))
			{
				const bool lBreak = aYieldReturn(lEndSquareInd);

				if (lBreak)
					return;

				if (lEndCoord == aStopEndSquare) return;
				break;
			}
			else break;

			lDirStep++;
		}
	}
}

void PieceMovement::GetPotentialMovesKingYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol, PieceMovement::YieldReturnMoveIntBase& aYieldReturn, const BoardCoordinates aStopEndSquare)
{
	const BoardCoordinates lStartCoord(aStartRow, aStartCol);
	const int lSquareIndex = CoToSI(lStartCoord);
	const SquareState& lStartSquare = aBoardState.Squares[lSquareIndex];
	const Colour lOppositeColour = GetOppositeColour(lStartSquare.Piece_);

	const int cDirCount = 8;
	const int cRowDir[] = { -1, -1,  0,  1, 1, 1, 0, -1 };
	const int cColDir[] = { 0, -1, -1, -1, 0, 1, 1,  1 };

	for (int iDir = 0; iDir < cDirCount; iDir++)
	{
		const BoardCoordinates lEndCoord(aStartRow + cRowDir[iDir], aStartCol + cColDir[iDir]);

		if (!IsIn(lEndCoord)) continue;

		const int lEndSquareInd = CoToSI(lEndCoord);
		if (IsSquareEmpty(aBoardState, lEndCoord))
		{
			const bool lBreak = aYieldReturn(lEndSquareInd);

			if (lBreak)
				return;

			if (lEndCoord == aStopEndSquare) return;
		}
		else if (HasPieceOfColour(aBoardState, lEndCoord, lOppositeColour))
		{
			const bool lBreak = aYieldReturn(lEndSquareInd);

			if (lBreak)
				return;

			if (lEndCoord == aStopEndSquare) return;
		}
	}

	// check for castling
	if (lStartSquare.Piece_ & P_KING)
	{
		if (lStartCoord.Col == 4 && GetRelativeRow(lStartCoord, aBoardState.PlayerToMove) == 0)
		{
			const int lStartRow = lStartCoord.Row;
			const int lStartCol = lStartCoord.Col;

			// king side
			if (((aBoardState.PlayerToMove & C_WHITE) && aBoardState.CanCastleKingsideWhite)
				||
				((aBoardState.PlayerToMove & C_BLACK) && aBoardState.CanCastleKingsideBlack))
			{
				const BoardCoordinates lRookCoord(lStartRow, lStartCol + 3);
				const char lRookSquareIndex = CoToSI(lRookCoord);

				if (IsSquareEmpty(aBoardState, lStartRow, lStartCol + 1) &&
					IsSquareEmpty(aBoardState, lStartRow, lStartCol + 2) &&
					aBoardState.Squares[lRookSquareIndex].Piece_ == (P_ROOK & aBoardState.PlayerToMove))
				{
					const bool lBreak = aYieldReturn(CoToSI(lStartRow, lStartCol + 2));

					if (lBreak)
						return;

					if (BoardCoordinates(lStartRow, lStartCol + 2) == aStopEndSquare) return;
				}
			}

			// queen side
			if (((aBoardState.PlayerToMove & C_WHITE) && aBoardState.CanCastleQueensideWhite)
				||
				((aBoardState.PlayerToMove & C_BLACK) && aBoardState.CanCastleQueensideBlack))
			{
				const BoardCoordinates lRookCoord(lStartRow, lStartCol - 4);
				const char lRookSquareIndex = CoToSI(lRookCoord);

				if (IsSquareEmpty(aBoardState, lStartRow, lStartCol - 1) &&
					IsSquareEmpty(aBoardState, lStartRow, lStartCol - 2) &&
					IsSquareEmpty(aBoardState, lStartRow, lStartCol - 3) &&
					aBoardState.Squares[lRookSquareIndex].Piece_ == (P_ROOK & aBoardState.PlayerToMove))
				{
					const bool lBreak = aYieldReturn(CoToSI(lStartRow, lStartCol - 2));

					if (lBreak)
						return;

					if (BoardCoordinates(lStartRow, lStartCol - 2) == aStopEndSquare) return;
				}
			}
		}
	}
}


bool PieceMovement::CanPotentiallyMoveToPawn(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord)
{
	const int lRowDiff = aTargetCoord.Row - aStartCoord.Row;
	const int lRowDiffAbs = std::abs(lRowDiff);
	if (lRowDiffAbs > 2) return false;
	const int lForwardRowDir = (aBoardState.PlayerToMove & C_WHITE) ? 1 : -1;
	if (lForwardRowDir * lRowDiff <= 0) return false;

	const int lColDiff = aTargetCoord.Col - aStartCoord.Col;
	const int lColDiffAbs = std::abs(lColDiff);

	if (lColDiffAbs > 1) return false;

	// move forward
	if (lColDiff == 0)
	{
		if (lRowDiffAbs == 1)
		{
			if (IsSquareEmpty(aBoardState, aStartCoord.Row + lForwardRowDir, aStartCoord.Col))
				return true;
		}
		if (lRowDiffAbs == 2)
		{
			if (GetRelativeRow(aStartCoord, aBoardState.PlayerToMove) == 1
				&&
				IsSquareEmpty(aBoardState, aStartCoord.Row + lForwardRowDir, aStartCoord.Col)
				&&
				IsSquareEmpty(aBoardState, aStartCoord.Row + 2 * lForwardRowDir, aStartCoord.Col))
				return true;
		}
	}

	// capture
	if (lColDiffAbs == 1 && lRowDiffAbs == 1)
	{
		if (HasPieceOfColour(aBoardState, aTargetCoord, GetOppositeColour(aBoardState.PlayerToMove)))
			return true;

		// en passant
		if (GetRelativeRow(aStartCoord, aBoardState.PlayerToMove) == 4)
		{
			const BoardCoordinates lPassedPawnCoord(aStartCoord.Row, aStartCoord.Col + lColDiff);
			const char lPassedPawnSquareIndex = CoToSI(lPassedPawnCoord);

			if (IsSquareEmpty(aBoardState, aStartCoord.Row + lForwardRowDir, aStartCoord.Col + lColDiff)
				&&
				aBoardState.Squares[lPassedPawnSquareIndex].Piece_ == (P_PAWN & GetOppositeColour(aBoardState.PlayerToMove)))
			{
				const BoardCoordinates lOppositePawnStartSquareCoord(aStartCoord.Row + 2 * lForwardRowDir, aStartCoord.Col + lColDiff);
				if (!IsSquareEmpty(aBoardState, lOppositePawnStartSquareCoord)) return false;

				//const SquareState& lPrevSquare = aBoardState.PreviousSquares[lOppositePawnStartSquareInd];
				//const bool lHasPreviousPawnOn2ndRow = aBoardState.HasPreviousPawnOn2ndRow(aStartCoord.Col + lColDiff, GetOppositeColour(aBoardState.PlayerToMove));
				if (aBoardState.WasLastMoveDoublePawn(GetOppositeColour(aBoardState.PlayerToMove), aStartCoord.Col + lColDiff))
					return true;
			}
		}
	}

	return false;
}

bool PieceMovement::CanPotentiallyMoveToKnight(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord)
{
	const int lRowDiffAbs = std::abs(aTargetCoord.Row - aStartCoord.Row);
	const int lColDiffAbs = std::abs(aTargetCoord.Col - aStartCoord.Col);

	if (lRowDiffAbs == 2 && lColDiffAbs == 1) return true;
	if (lRowDiffAbs == 1 && lColDiffAbs == 2) return true;
	return false;
}
bool PieceMovement::CanPotentiallyMoveToBishop(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord)
{
	const int lRowDiff = aTargetCoord.Row - aStartCoord.Row;
	const int lColDiff = aTargetCoord.Col - aStartCoord.Col;
	const int lRowDiffSign = Sgn(lRowDiff);
	const int lColDiffSign = Sgn(lColDiff);
	const int lRowDiffAbs = std::abs(lRowDiff);
	const int lColDiffAbs = std::abs(lColDiff);

	if (lRowDiffAbs != lColDiffAbs) return false;

	for (int i = 1; i < lRowDiffAbs; i++)
	{
		const int lSquareInd = CoToSI(aStartCoord.Row + i * lRowDiffSign, aStartCoord.Col + i * lColDiffSign);
		if (aBoardState.Squares[lSquareInd].Piece_) return false;
	}

	// the target square already handled in the common function
	return true;
}
bool PieceMovement::CanPotentiallyMoveToRook(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord)
{
	const int lRowDiff = aTargetCoord.Row - aStartCoord.Row;
	const int lColDiff = aTargetCoord.Col - aStartCoord.Col;
	const int lRowDiffSign = Sgn(lRowDiff);
	const int lColDiffSign = Sgn(lColDiff);

	if (lRowDiff != 0 && lColDiff != 0) return false;

	for (int i = 1; i < std::max(std::abs(lRowDiff), std::abs(lColDiff)); i++)
	{
		const int lSquareInd = CoToSI(aStartCoord.Row + i * lRowDiffSign, aStartCoord.Col + i * lColDiffSign);
		if (aBoardState.Squares[lSquareInd].Piece_) return false;
	}

	// the target square already handled in the common function
	return true;
}
bool PieceMovement::CanPotentiallyMoveToQueen(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord)
{
	const int lRowDiff = aTargetCoord.Row - aStartCoord.Row;
	const int lColDiff = aTargetCoord.Col - aStartCoord.Col;
	const int lRowDiffSign = Sgn(lRowDiff);
	const int lColDiffSign = Sgn(lColDiff);
	const int lRowDiffAbs = std::abs(lRowDiff);
	const int lColDiffAbs = std::abs(lColDiff);

	if (lRowDiffAbs != 0 && lColDiffAbs != 0 && lRowDiffAbs != lColDiffAbs) return false;

	for (int i = 1; i < std::max(lRowDiffAbs, lColDiffAbs); i++)
	{
		const int lSquareInd = CoToSI(aStartCoord.Row + i * lRowDiffSign, aStartCoord.Col + i * lColDiffSign);
		if (aBoardState.Squares[lSquareInd].Piece_) return false;
	}

	// the target square already handled in the common function
	return true;
}
bool PieceMovement::CanPotentiallyMoveToKing(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord)
{
	const int lRowDiff = aTargetCoord.Row - aStartCoord.Row;
	const int lColDiff = aTargetCoord.Col - aStartCoord.Col;
	const int lRowDiffAbs = std::abs(lRowDiff);
	const int lColDiffAbs = std::abs(lColDiff);

	if (lRowDiffAbs <= 1 && lColDiffAbs <= 1) return true;

	// castle
	if (lRowDiffAbs == 0 && lColDiffAbs == 2)
	{
		const int lRelativeRow = GetRelativeRow(aStartCoord, aBoardState.PlayerToMove);
		if (lRelativeRow != 0) return false;

		if (lColDiff == 2) // king side
		{
			if ((aBoardState.PlayerToMove & C_WHITE) && !aBoardState.CanCastleKingsideWhite)
				return false;
			if ((aBoardState.PlayerToMove & C_BLACK) && !aBoardState.CanCastleKingsideBlack)
				return false;

			const BoardCoordinates lRookCoord(aStartCoord.Row, aStartCoord.Col + 3);
			const char lRookSquareIndex = CoToSI(lRookCoord);

			if (IsSquareEmpty(aBoardState, aStartCoord.Row, aStartCoord.Col + 1) &&
				IsSquareEmpty(aBoardState, aStartCoord.Row, aStartCoord.Col + 2) &&
				aBoardState.Squares[lRookSquareIndex].Piece_ == (P_ROOK & aBoardState.PlayerToMove))
			{
				return true;
			}
		}
		else // -2 // queenside
		{
			if ((aBoardState.PlayerToMove & C_WHITE) && !aBoardState.CanCastleQueensideWhite)
				return false;
			if ((aBoardState.PlayerToMove & C_BLACK) && !aBoardState.CanCastleQueensideBlack)
				return false;

			const BoardCoordinates lRookCoord(aStartCoord.Row, aStartCoord.Col - 4);
			const char lRookSquareIndex = CoToSI(lRookCoord);

			if (IsSquareEmpty(aBoardState, aStartCoord.Row, aStartCoord.Col - 1) &&
				IsSquareEmpty(aBoardState, aStartCoord.Row, aStartCoord.Col - 2) &&
				IsSquareEmpty(aBoardState, aStartCoord.Row, aStartCoord.Col - 3) &&
				aBoardState.Squares[lRookSquareIndex].Piece_ == (P_ROOK & aBoardState.PlayerToMove))
			{
				return true;
			}
		}
	}
	return false;
}

bool PieceMovement::AnyLegalMovesYieldReturnWrapper::operator()(const Move& aMove)
{
	const Move& lPotentialMove = aMove;
	const Colour lMovingPlayer = mBoardState.PlayerToMove;
	const Colour lOppositePlayer = GetOppositeColour(lMovingPlayer);

	const BoardState lBoardAfterMove = mBoardState.MakeMoveRaw(lPotentialMove);

	const Piece lMyKing = P_KING & lMovingPlayer;
	const BoardCoordinates& lMyKingNow = ((lMovingPlayer & C_WHITE) ? mBoardState.WhiteKing : mBoardState.BlackKing);
	const BoardCoordinates& lMyKingAfterMove = ((lMovingPlayer & C_WHITE) ? lBoardAfterMove.WhiteKing : lBoardAfterMove.BlackKing);
	if (!IsIn(lMyKingAfterMove)) throw EXCEPTION_GLOBAL("King not found!");

	// if our king can be captured after this move then it is not legal
	//if (CanBeMovedTo(lBoardAfterMove, lMyKingAfterMove)) continue;
	if (IsAttacked(lBoardAfterMove, lMyKingAfterMove)) return false;

	// is it a castle? If it is, do extra checks
	if (lPotentialMove.IsCastle())
	{
		// is king currently in check?
		BoardState lCurrentBoardOtherPlayer(mBoardState);
		lCurrentBoardOtherPlayer.PlayerToMove = lOppositePlayer;

		if (CanBeMovedTo(lCurrentBoardOtherPlayer, lMyKingNow)) return false;

		// is king crossing a checked square (there is always only one basically, other than the start and end ones)
		const int lMiddleCol = (lPotentialMove.End.Col + lPotentialMove.Start.Col) / 2;

		// move the king to the middle square
		BoardCoordinates lMiddleSquare(lPotentialMove.Start.Row, lMiddleCol);
		lCurrentBoardOtherPlayer.Squares[CoToSI(lMyKingNow)].Piece_ = P_NONE;
		lCurrentBoardOtherPlayer.Squares[CoToSI(lMiddleSquare)].Piece_ = lMyKing;

		//lCurrentBoardOtherPlayer.RebuildPieceList();

		if (CanBeMovedTo(lCurrentBoardOtherPlayer, lMiddleSquare)) return false;
	}

	// the move is accepted as legal
	LegalMoveFound = true;
	return true;
}

bool PieceMovement::GetPotentialMovesYieldReturnWrapper::operator()(const int& aEndSquareIndex)
{
	if (mIsPawn)
	{
		Move lMove;
		lMove.MovedPiece = mSquare.Piece_;
		lMove.Start = mStartCoord;
		lMove.End = SIToCo(aEndSquareIndex);

		// check for en passant
		if (lMove.MovedPiece & P_PAWN &&
			lMove.Start.Col != lMove.End.Col &&
			!mBoardState.Squares[aEndSquareIndex].Piece_)
		{
			lMove.IsEnPassant = true;
		}

		// Handle promotion case, only case where the move is not uniquely described by its coordinates
		if (IsLastRow(lMove.End, lMove.MovedPiece))
		{
			Move lMove1(lMove);
			Move lMove2(lMove);
			Move lMove3(lMove);
			Move lMove4(lMove);

			lMove1.PromotionPiece = P_KNIGHT & mBoardState.PlayerToMove;
			lMove2.PromotionPiece = P_BISHOP & mBoardState.PlayerToMove;
			lMove3.PromotionPiece = P_ROOK & mBoardState.PlayerToMove;
			lMove4.PromotionPiece = P_QUEEN & mBoardState.PlayerToMove;

			bool lBreak = false;
			lBreak = mYieldInner(lMove1);
			if (lBreak) return true;
			lBreak = mYieldInner(lMove2);
			if (lBreak) return true;
			lBreak = mYieldInner(lMove3);
			if (lBreak) return true;
			lBreak = mYieldInner(lMove4);
			return lBreak;
		}
		else
		{
			const bool lBreak = mYieldInner(lMove);
			return lBreak;
		}
	}
	else
	{
		Move lMove;
		lMove.MovedPiece = mSquare.Piece_;
		lMove.Start = mStartCoord;
		lMove.End = SIToCo(aEndSquareIndex);

		const bool lBreak = mYieldInner(lMove);

		return lBreak;
	}
}