
class BoardState;

#pragma once
#include <array>
#include <iostream>
#include <vector>

#include "BoardSetup.h"
#include "SquareState.h"
#include "FunctionsCoord.h"
#include "Colour.h"
#include "Piece.h"
#include "Move.h"
#include "GamePhase.h"

class BoardState
{
public:

//#define HASH_ZOBRIST // warning: zobrish hashing involves random initialisation, so a cache based on this can behave differently every time
#define HASH_INHOUSE

	class Hash
	{
	public:
		std::size_t operator()(const BoardState& aBoardState) const;
	};

private:
	const static int cRows = BoardSetup::cRows;
	const static int cCols = BoardSetup::cCols;
	const static int cSquareCount = BoardSetup::cSquareCount;
	const static int cMaxPieces = BoardSetup::cMaxPieces;
	static thread_local std::vector<Move> mMoveBuffer;
	static thread_local Hash mStaticHasher;
	const static bool cInvertHashForBlack = false;

public:

	struct BoardInfo
	{
	public:
		bool CanCastleKingsideWhite = false;
		bool CanCastleQueensideWhite = false;
		bool CanCastleKingsideBlack = false;
		bool CanCastleQueensideBlack = false;
		Colour PlayerToMove = C_WHITE;
	};
	
public:

	std::array<SquareState, cSquareCount> Squares;
	// board state before last move
	//std::array<SquareState, cSquareCount> PreviousSquares;
	// 8 bits, 1 if the second row (relative) had pawn on itself in the previous state, 0 otherwise
	// this is all we need to detect en passant
	// pawn bit for column i can be obtained as ( PreviousPawnsWhite & (1 << i) )
	//unsigned char PreviousPawnsWhite = 0;
	//unsigned char PreviousPawnsBlack = 0;
	// indicates whether the last move (the one that led to this board state) was a pawn push by 2 squares (so from the second row)
	// this is important for en passant, as the second row previous pawn list is not enough
	// you can have doubled pawn (one on 2nd and one on 4th row)
	// the one on the second captures something -> leaves 2nd row
	// if the one on 4th row has a pawn next to it, it will detect en passant possibility (pawn was on 2nd row, now it's not and a pawn is now on 4th row)
	// therefore we need one extra indicator to check if the last performed move was pawn double push
	// this should eliminate all false en passant cases, since en passant is only possible after pawn double push
	//bool WasLastMoveDoublePawn = false;

	// if negative, the last move was not a pawn double push of this colour
	// if non-negative, the last move (the move that lead to this board) was a pawn double push on the given column by that colour
	char LastMoveWhitePawnDoublePushCol = -1;
	char LastMoveBlackPawnDoublePushCol = -1;
	//std::array<LocalisedPiece, cMaxPieces> Pieces;

	// redundant info, does not need to be used in comparison, hashing or anything, just to be able to quickly access kings positions
	// taken care of in the RebuildPieceList() function
	BoardCoordinates WhiteKing;
	BoardCoordinates BlackKing;

	//int GetPieceCount() const { return mPiecesCount; }
	
	bool CanCastleQueensideWhite = false;
	bool CanCastleKingsideWhite = false;
	bool CanCastleQueensideBlack = false;
	bool CanCastleKingsideBlack = false;
	
	Colour PlayerToMove;

	// same value as the Hash class would compute, but evaluated when the board is created by make move from a previous board
	size_t HashValue = 0;
	
	static BoardState CreateInitialPosition();
	static BoardState CreateEmptyBoard();
	
	BoardState MakeMove(const Move& aMove) const;
	// do with moves that you already know are at least potential
	BoardState MakeMoveRaw(const Move& aMove) const;

	//bool CheckConsistency() const;
	// reassemble the Pieces array based on the current Squares array
	//void RebuildPieceList();
	//inline int FindPiece(const int aRow, const int aCol) const
	//{
	//	for (int i = 0; i < mPiecesCount; i++)
	//	{
	//		if (Pieces[i].Coord.Row == aRow && Pieces[i].Coord.Col == aCol) return i;
	//	}
	//	return -1;
	//}
	//inline int FindPiece(const BoardCoordinates& aCoord) const { return FindPiece(aCoord.Row, aCoord.Col); }
	//inline void RemovePiece(const int aRow, const int aCol)
	//{
	//	const int lPieceIndex = FindPiece(aRow, aCol);
	//	if (lPieceIndex < 0) return;
	//	
	//	const int lSquareIndex = CoToSI(Pieces[lPieceIndex].Coord);
	//	Squares[lSquareIndex].HasPiece = false;
	//	
	//	for (int i = lPieceIndex; i < mPiecesCount - 1; i++)
	//	{
	//		Pieces[i] = Pieces[i + 1];
	//	}
	//	mPiecesCount--;
	//}
	//inline void RemovePiece(const BoardCoordinates& aCoord) { RemovePiece(aCoord.Row, aCoord.Col); }

	// if aCheckedForCheckAlready is true, we are telling the function that the check was already checked 
	// (and the check exists, otherwise this function would not be called)
	bool IsMate(const bool aCheckedForCheckAlready = false) const;

	// if aCheckedForCheckAlready is true, we are telling the function that the check was already checked 
	// (and the check does not exist, otherwise this function would not be called)
	bool IsStalemate(const bool aCheckedForCheckAlready = false) const;

	static BoardState FromFEN(const std::string& aFEN);
	std::string ToFEN(const int aPliesFromLastPawnCorA = -1, const int aMoveNumber = -1) const;

	// count how many positions we can reach from this board on given depth
	int CountPositions(const int aDepth, const int aStartDepth) const;

	inline bool operator==(const BoardState& aOther) const
	{
		if (PlayerToMove != aOther.PlayerToMove) return false;

		const bool lCompare1 =
			PlayerToMove == aOther.PlayerToMove &&
			CanCastleKingsideWhite == aOther.CanCastleKingsideWhite &&
			CanCastleKingsideBlack == aOther.CanCastleKingsideBlack &&
			CanCastleQueensideWhite == aOther.CanCastleQueensideWhite &&
			CanCastleQueensideBlack == aOther.CanCastleQueensideBlack;

		if (!lCompare1) return false;

		if (WhiteKing != aOther.WhiteKing) return false;
		if (BlackKing != aOther.BlackKing) return false;

		if (LastMoveWhitePawnDoublePushCol != aOther.LastMoveWhitePawnDoublePushCol) return false;
		if (LastMoveBlackPawnDoublePushCol != aOther.LastMoveBlackPawnDoublePushCol) return false;

		for (int iSquare = 0; iSquare < cSquareCount; iSquare++)
		{
			if (Squares[iSquare] != aOther.Squares[iSquare]) return false;
		}

		return true;
	}

	inline bool operator!=(const BoardState& aOther) const
	{
		return !(*this == aOther);
	}

	GamePhase ComputeGamePhase() const
	{
		GamePhase lReturnValue;

		unsigned char lWhitePawnCount = 0;
		unsigned char lBlackPawnCount = 0;
		unsigned char lWhitePieceCount = 0;
		unsigned char lBlackPieceCount = 0;

		unsigned char lWhite2ndRowPawns = 0;
		unsigned char lBlack2ndRowPawns = 0;

		unsigned char lWhiteUndevelopedPieceCount = 0;
		unsigned char lBlackUndevelopedPieceCount = 0;

		for (int i = 0; i < cSquareCount; i++)
		{
			const SquareState& lSquare = Squares[i];
			if (!lSquare.Piece_) continue;

			if (lSquare.Piece_ & C_WHITE)
			{
				if (lSquare.Piece_ & P_PAWN)
				{
					lWhitePawnCount++;
					if (lSquare.Coordinates.Row == 1)
						lWhite2ndRowPawns++;
				}
				else if (lSquare.Piece_ & (~P_KING & ~P_ROOK))
				{
					lWhitePieceCount++;
					if (lSquare.Coordinates.Row == 0)
						lWhiteUndevelopedPieceCount++;
				}
			}
			else
			{
				if (lSquare.Piece_ & P_PAWN)
				{
					lBlackPawnCount++;
					if (lSquare.Coordinates.Row == 6)
						lBlack2ndRowPawns++;
				}
				else if (lSquare.Piece_ & (~P_KING & ~P_ROOK))
				{
					lBlackPieceCount++;
					if (lSquare.Coordinates.Row == 7)
						lBlackUndevelopedPieceCount++;
				}
			}
		}

		if (lWhitePieceCount >= 3 && lBlackPieceCount >= 3)
		{
			lReturnValue.Opening =
				(
					8 * (lWhiteUndevelopedPieceCount + lBlackUndevelopedPieceCount) +
					lWhitePieceCount + lBlackPieceCount +
					4 * (lWhite2ndRowPawns + lBlack2ndRowPawns) +
					(float)lWhitePawnCount / 2 + (float)lBlackPawnCount / 2
				);

			if (CanCastleKingsideWhite || CanCastleQueensideWhite)
				lReturnValue.Opening += 2;

			if (CanCastleKingsideBlack || CanCastleQueensideBlack)
				lReturnValue.Opening += 2;
			
			lReturnValue.Opening /= 
				(8 * (5 + 5) + 
				5 + 5 + 
				4 * (8 + 8) + 
				4 + 4 + 
				2 + 2);

			lReturnValue.MiddleGame = 1 - lReturnValue.Opening;
		}
		else
		{
			lReturnValue.Endgame = 1;
		}

		return lReturnValue;
	}

	inline bool WasLastMoveDoublePawn() const { return LastMoveWhitePawnDoublePushCol >= 0 || LastMoveBlackPawnDoublePushCol >= 0; }
	inline bool WasLastMoveDoublePawn(const Colour aColour) const 
	{
		if (aColour & C_WHITE)
			return LastMoveWhitePawnDoublePushCol >= 0;
		if (aColour & C_BLACK)
			return LastMoveBlackPawnDoublePushCol >= 0;

		throw EXCEPTION_MEMBER("Invalid colour!");
	}
	inline bool WasLastMoveDoublePawn(const Colour aColour, const char aCol) const
	{
		if (aColour & C_WHITE)
			return LastMoveWhitePawnDoublePushCol == aCol;
		if (aColour & C_BLACK)
			return LastMoveBlackPawnDoublePushCol == aCol;

		throw EXCEPTION_MEMBER("Invalid colour!");
	}

	//inline bool HasPreviousPawnOn2ndRow(const char aColumn, const Colour aColour) const
	//{
	//	if (aColour & C_WHITE)
	//	{
	//		return (PreviousPawnsWhite & (1 << aColumn));
	//	}
	//	else
	//	{
	//		return (PreviousPawnsBlack & (1 << aColumn));
	//	}
	//}

	bool IsMovingPlayerInCheck() const;

	//static inline unsigned char ComputePawnPresense(const BoardState& aBoard, const Colour aColour)
	//{
	//	unsigned char lReturnValue = 0;

	//	const Piece lPawn = P_PAWN & aColour;

	//	char lRowIndex = 1;
	//	if (aColour & C_BLACK) lRowIndex = 6;

	//	for (char iCol = 0; iCol < cCols; iCol++)
	//	{
	//		const char lSquareIndex = CoToSI(lRowIndex, iCol);
	//		const SquareState& lSquare = aBoard.Squares[lSquareIndex];
	//		if (!lSquare.Piece_) continue;
	//		if (lSquare.Piece_ == lPawn)
	//		{
	//			lReturnValue |= (1 << iCol);
	//		}
	//	}

	//	return lReturnValue;
	//}
};

