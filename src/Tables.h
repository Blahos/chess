
#pragma once
#include <vector>
#include <array>
#include <string>
#include <map>

#include "BoardSetup.h"
#include "BoardState.h"
#include "Piece.h"
#include "FunctionsMove.h"

class Tables
{
public:
	// I probably (don't remember anymore) first tested generating the tables with dynamic arrays, then checked how large those were, 
	// saved this value and then switched to static arrays with this size
	const static int cAttackTableSize = 2310;

	static LocalisedPieceIndex AttacksWhiteArray[cAttackTableSize];
	static bool AttacksWhiteLineStartPoints[cAttackTableSize];
	static int AttacksWhiteSquareStartPoints[BoardSetup::cSquareCount];

	static LocalisedPieceIndex AttacksBlackArray[cAttackTableSize];
	static bool AttacksBlackLineStartPoints[cAttackTableSize];
	static int AttacksBlackSquareStartPoints[BoardSetup::cSquareCount];

	static std::map<Piece, std::vector<size_t>> ZobristHashes;

	static std::map<Piece, std::string> FENPieceToSymbol;
	static std::map<std::string, Piece> FENSymbolToPiece;

	static void InitTables();
};
