
class Time;

#pragma once
#include <chrono>

class Time
{
private:
    std::chrono::steady_clock::time_point mCurrentStart = std::chrono::steady_clock::now();
    std::chrono::steady_clock::time_point mCurrentStop = std::chrono::steady_clock::now();
    std::chrono::steady_clock::time_point mLastReportTime = std::chrono::steady_clock::now();
	
	bool mIsRunning = true;
    
public:
    void Start();
    double Stop(const bool& aPrint = false);
	/// <summary>
	/// 
	/// </summary>
	/// <returns>Elapsed time in milliseconds</returns>
    double Elapsed() const;
    // if the given duration has elapsed then display the message and restart itself (call Start()). Otherwise do nothing.
    void Report(const std::string& aMessage, double aDurationMS, bool aUpdateLastReportTime = true);
	
	static time_t CurrentTime();
	static std::string TimeToUTCString(const time_t& aTime);
	static std::string CurrentUTCTimeString();
};
