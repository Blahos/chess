
class CmdArgumentParser;

#pragma once
#include <string>

class CmdArgumentParser
{
public:
	static std::string FindValueForKey(const std::string& aKey, const int aArgCount, const char* const* aArgList)
	{
		for (int i = 0; i < aArgCount - 1; i++)
		{
			if (aKey.compare(std::string(aArgList[i])) == 0)
			{
				return std::string(aArgList[i + 1]);
			}
		}

		return std::string();
	}

	static int FindStringPosition(const std::string& aKey, const int aArgCount, const char* const* aArgList)
	{
		for (int i = 0; i < aArgCount; i++)
		{
			if (aKey.compare(std::string(aArgList[i])) == 0)
			{
				return i;
			}
		}

		return -1;
	}

	static bool IsStringPresent(const std::string& aKey, const int aArgCount, const char* const* aArgList)
	{
		const int lPosition = FindStringPosition(aKey, aArgCount, aArgList);

		return lPosition >= 0 && lPosition < aArgCount;
	}
};
