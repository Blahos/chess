
#include "Exception.h"

Exception::Exception(const std::string& aMessage, const std::string& aFile, const int aCode, const int aLineNumber, const std::string& aFunctionName, const std::string& aClassName)
	: Message(aMessage), File(aFile), Code(aCode), FunctionName(aFunctionName), ClassName(aClassName), LineNumber(aLineNumber)
{
	
}

std::ostream& operator<<(std::ostream& aInStream, const Exception& aEx)
{
	aInStream << "Message: " << aEx.Message << std::endl;
	aInStream << "File: " << aEx.File << std::endl;
	aInStream << "Line number: " << aEx.LineNumber << std::endl;
	aInStream << "Code: " << aEx.Code << std::endl;
	aInStream << "Function: " << aEx.FunctionName << std::endl;
	aInStream << "Class: " << aEx.ClassName << std::endl;

	return aInStream;
}
