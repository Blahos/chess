
class PieceMovement;

#pragma once
#include <vector>
#include <optional>
#include <functional>

#include "Move.h"
#include "BoardStateExtra.h"
#include "StaticList.h"

class PieceMovement
{
public:

	// just an estimate of how many legal moves there will ever be possible on a given board
	// https://www.reddit.com/r/chess/comments/9j70dc/position_with_the_most_number_of_legal_moves/
	static const int cMaxLegalMoves = 120;

	template <typename T, typename TParam>
	static inline bool RunLambda(const T& aLambda, const TParam& aParam)
	{
		return aLambda(aParam);
	}

	class YieldReturnMoveBase
	{
	public:
		virtual bool operator()(const Move& aMove) = 0;
	};

	class YieldReturnMoveIntBase
	{
	public:
		virtual bool operator()(const int& aMoveEndSquareIndex) = 0;
	};

	struct MoveBoard
	{
	public:
		Move Move_;
		BoardState Board;
	};

	/////////////
	/// POTENTIAL
	/////////////
	// doesn't care about checks, only about physical possibility to move to a certain square, given the moving rules
	// king is treated as any other piece here, can be captured
	// appends the moves to the aMoves vector
	static void GetPotentialMoves(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<Move>& aMoves, const BoardCoordinates aStopEndSquare = BoardCoordinates(-1, -1));

	static void GetPotentialMovesYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol, YieldReturnMoveBase& aYieldReturn, const BoardCoordinates aStopEndSquare = BoardCoordinates(-1, -1));
	
	static bool CanPotentiallyMoveTo(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord);

	static void GetLegalMoves(const BoardState& aBoardState, const int aStartRow, const int aStartCol, StaticList<Move, cMaxLegalMoves>& aMoves, const std::optional<BoardStateExtra>& aExtraInfo = std::nullopt);

	static void GetLegalMovesWithBoards(const BoardState& aBoardState, const int aStartRow, const int aStartCol, StaticList<MoveBoard, cMaxLegalMoves>& aMoves, const std::optional<BoardStateExtra>& aExtraInfo = std::nullopt);

	static void GetAllLegalMoves(const BoardState& aBoardState, StaticList<Move, cMaxLegalMoves>& aMoves, const std::optional<BoardStateExtra>& aExtraInfo = std::nullopt);

	// also adds the resulting boards created by the moves
	static void GetAllLegalMovesWithBoards(const BoardState& aBoardState, StaticList<MoveBoard, cMaxLegalMoves>& aMoves, const std::optional<BoardStateExtra>& aExtraInfo = std::nullopt);

	// the buffer is just so it can be resused to avoid multiple reallocations
	static bool AnyLegalMoves(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<Move>& aMoveBuffer);

	// variant using the yield versions of the get potential moves
	static bool AnyLegalMovesYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol);

	// the buffer is just so it can be resused to avoid multiple reallocations
	static bool AnyLegalMoves(const BoardState& aBoardState, std::vector<Move>& aMoveBuffer);

private:
	/////////////
	/// POTENTIAL
	/////////////
	static void GetPotentialMovesPawn(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<int>& aMoves, const BoardCoordinates aStopEndSquare);
	static void GetPotentialMovesKnight(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<int>& aMoves, const BoardCoordinates aStopEndSquare);
	static void GetPotentialMovesBishop(const BoardState& aBoardState, const int aRow, const int aStartCol, std::vector<int>& aMoves, const BoardCoordinates aStopEndSquare);
	static void GetPotentialMovesRook(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<int>& aMoves, const BoardCoordinates aStopEndSquare);
	static void GetPotentialMovesQueen(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<int>& aMoves, const BoardCoordinates aStopEndSquare);
	static void GetPotentialMovesKing(const BoardState& aBoardState, const int aStartRow, const int aStartCol, std::vector<int>& aMoves, const BoardCoordinates aStopEndSquare);

	/////////////////////////
	// POTENTIAL YIELD RETURN
	/////////////////////////
	static void GetPotentialMovesPawnYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol, PieceMovement::YieldReturnMoveIntBase& aYieldReturn, const BoardCoordinates aStopEndSquare);
	static void GetPotentialMovesKnightYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol, PieceMovement::YieldReturnMoveIntBase& aYieldReturn, const BoardCoordinates aStopEndSquare);
	static void GetPotentialMovesBishopYield(const BoardState& aBoardState, const int aRow, const int aStartCol, PieceMovement::YieldReturnMoveIntBase& aYieldReturn, const BoardCoordinates aStopEndSquare);
	static void GetPotentialMovesRookYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol, PieceMovement::YieldReturnMoveIntBase& aYieldReturn, const BoardCoordinates aStopEndSquare);
	static void GetPotentialMovesQueenYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol, PieceMovement::YieldReturnMoveIntBase& aYieldReturn, const BoardCoordinates aStopEndSquare);
	static void GetPotentialMovesKingYield(const BoardState& aBoardState, const int aStartRow, const int aStartCol, PieceMovement::YieldReturnMoveIntBase& aYieldReturn, const BoardCoordinates aStopEndSquare);

	static bool CanPotentiallyMoveToPawn(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord);
	static bool CanPotentiallyMoveToKnight(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord);
	static bool CanPotentiallyMoveToBishop(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord);
	static bool CanPotentiallyMoveToRook(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord);
	static bool CanPotentiallyMoveToQueen(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord);
	static bool CanPotentiallyMoveToKing(const BoardState& aBoardState, const BoardCoordinates& aStartCoord, const BoardCoordinates& aTargetCoord);

	class AnyLegalMovesYieldReturnWrapper : public YieldReturnMoveBase
	{
	private:
		const BoardState& mBoardState;

	public:
		bool LegalMoveFound = false;

		AnyLegalMovesYieldReturnWrapper(const BoardState& aBoardState)
			: mBoardState(aBoardState)
		{

		}

		virtual bool operator()(const Move& aMove) override;
	};

	class GetPotentialMovesYieldReturnWrapper : public YieldReturnMoveIntBase
	{
	private:
		const SquareState& mSquare;
		const BoardCoordinates& mStartCoord;
		const BoardState& mBoardState;
		YieldReturnMoveBase& mYieldInner;
		const bool mIsPawn;

	public:
		GetPotentialMovesYieldReturnWrapper(const SquareState& aSquare, const BoardCoordinates& aStartCoord, const BoardState& aBoardState, YieldReturnMoveBase& aYieldInner, const bool aIsPawn)
			: mSquare(aSquare), mStartCoord(aStartCoord), mBoardState(aBoardState), mYieldInner(aYieldInner), mIsPawn(aIsPawn)
		{

		}

		virtual bool operator()(const int& aEndSquareIndex) override;
	};

	static thread_local std::vector<Move> mMoveBuffer;
	static thread_local std::vector<int> mIntMoveBuffer;
};

