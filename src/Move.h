
struct Move;
struct MoveWithNotes;

#pragma once
#include <string>

#include "BoardCoordinates.h"
#include "Piece.h"
#include "FunctionsCoord.h"
#include "BoardState.h"

struct Move
{
public:
	BoardCoordinates Start;
	BoardCoordinates End;
	Piece MovedPiece;
	// only for cases o pawns coming to last rank, otherwise ignore
	// setting pawn to make it crash on checks if it is not modified when it's supposed to be
	Piece PromotionPiece = P_PAWN;
	// in case it's an en passant pawn capture
	bool IsEnPassant = false;

	inline bool IsPromotion() const
	{
		return (MovedPiece & P_PAWN) && IsLastRow(End, MovedPiece);
	}
	inline bool IsCastle() const
	{
		if ((MovedPiece & P_KING) && std::abs(Start.Col - End.Col) == 2)
		{
			return true;
		}
		return false;
	}
	// the move must be possible on the given board (i.e. at least potential)
	bool IsCapture(const BoardState& aBoard) const;
	// the move must be possible on the given board (i.e. at least potential)
	bool IsPawnCapture(const BoardState& aBoard) const;

	inline bool operator==(const Move& aOther) const
	{
		return Start == aOther.Start && End == aOther.End && MovedPiece == aOther.MovedPiece && IsEnPassant == aOther.IsEnPassant &&
			((!IsPromotion() && !aOther.IsPromotion()) || (IsPromotion() && aOther.IsPromotion() && PromotionPiece == aOther.PromotionPiece));
	}
	inline bool operator!=(const Move& aOther) const
	{
		return !(*this == aOther);
	}
};

struct MoveWithNotes
{
public:
	Move Move_;
	std::string Notes = "";

	MoveWithNotes() { }

	MoveWithNotes(const Move& aMove)
		: Move_(aMove)
	{
	}

	MoveWithNotes operator=(const Move& aMove)
	{
		Move_ = aMove;
		return *this;
	}
};
