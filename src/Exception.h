
class Exception;

#pragma once
#include <string>
#include <numeric>
#include <iostream>
#include <typeinfo>

// doesn't work for static class methods
#define EXCEPTION_MEMBER(message) Exception((message), __FILE__, -1, __LINE__, __func__, typeid(*this).name());
#define EXCEPTION_GLOBAL(message) Exception((message), __FILE__, -1, __LINE__, __func__, "--- not member of a class ---");

class Exception
{
public:
	const std::string Message;
	const std::string File;
	const int Code;
	const std::string FunctionName;
	const std::string ClassName;
	const int LineNumber;

	Exception(const std::string& aMessage, const std::string& aFile = "", const int aCode = 0, const int aLineNumber = -1, const std::string& aFunctionName = "", const std::string& aClassName = "");

	friend std::ostream& operator<<(std::ostream& aInStream, const Exception& aEx);
};
