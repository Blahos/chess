
struct GamePhase;

#pragma once
#include <cmath>
#include <sstream>
#include <string>
#include <iomanip>
#include <iostream>

struct GamePhase 
{
public:
	float Opening = 0;
	float MiddleGame = 0;
	float Endgame = 0;

	inline void Normalise()
	{
		const float lNorm = std::sqrt(Opening * Opening + MiddleGame + MiddleGame + Endgame * Endgame);
		Opening /= lNorm;
		MiddleGame /= lNorm;
		Endgame /= lNorm;
	}
};

inline std::ostream& operator<<(std::ostream& aStream, const GamePhase& aState)
{
	std::stringstream lS;

	lS.precision(2);
	lS << std::fixed;

	lS << "[O: " << aState.Opening << ", M: " << aState.MiddleGame << ", E: " << aState.Endgame << "]";
	aStream << lS.str();

	return aStream;
}