
class MoveProducer;

#pragma once
#include <set>
#include <mutex>
#include <string>
#include "Move.h"

class MoveProducer
{
public:
	class Listener
	{
	public:
		virtual void NewMove(const Move aMove) { }
		virtual void NewMovePGN(const std::string& aPGNMove) { }
	};

private:
	std::mutex mListenersMutex;
	std::set<Listener*> mListeners = std::set<Listener*>();

protected:
	void NotifyNewMove(const Move& aMove)
	{
		mListenersMutex.lock();
		try
		{
			for (auto nListener : mListeners)
			{
				nListener->NewMove(aMove);
			}
		}
		catch (...)
		{
			mListenersMutex.unlock();
			throw;
		}
		mListenersMutex.unlock();
	}
	void NotifyNewMovePGN(const std::string& aMovePGN)
	{
		mListenersMutex.lock();
		try
		{
			for (auto nListener : mListeners)
			{
				nListener->NewMovePGN(aMovePGN);
			}
		}
		catch (...)
		{
			mListenersMutex.unlock();
			throw;
		}
		mListenersMutex.unlock();
	}

public:
	void Subscribe(Listener* const aListener)
	{
		mListenersMutex.lock();
		if (mListeners.find(aListener) == mListeners.end())
			mListeners.insert(aListener);
		mListenersMutex.unlock();
	}
	void Unsubscribe(Listener* const aListener)
	{
		mListenersMutex.lock();
		if (mListeners.find(aListener) != mListeners.end())
			mListeners.erase(aListener);
		mListenersMutex.unlock();
	}
	void UnsubscribeAll()
	{
		mListenersMutex.lock();

		mListeners.clear();

		mListenersMutex.unlock();
	}
};