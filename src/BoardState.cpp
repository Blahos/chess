
#include "BoardState.h"
#include "PieceMovement.h"
#include "FunctionsCoord.h"
#include "FunctionsBoard.h"
#include "FunctionsMove.h"
#include "Tables.h"

#include "Exception.h"

thread_local std::vector<Move> BoardState::mMoveBuffer;
thread_local BoardState::Hash BoardState::mStaticHasher;

std::size_t BoardState::Hash::operator()(const BoardState& aBoardState) const
{
	size_t lReturnValue = 0;
#if defined(HASH_INHOUSE)

	for (char iSquare = 0; iSquare < cSquareCount; iSquare++)
	{
		const size_t lHasPiece = aBoardState.Squares[iSquare].Piece_ ? 1 : 0;
		lReturnValue |= (lHasPiece << iSquare);
	}

	if (cInvertHashForBlack && (aBoardState.PlayerToMove & C_BLACK))
	{
		lReturnValue = ~lReturnValue;
	}
#elif defined(HASH_ZOBRIST)

	for (char iSquare = 0; iSquare < cSquareCount; iSquare++)
	{
		const Piece& lPiece = aBoardState.Squares[iSquare].Piece_;
		if (lPiece)
		{
			const size_t lSquareHash = Tables::ZobristHashes[lPiece][iSquare];

			lReturnValue ^= lSquareHash;
		}
	}
#else
#error No hashing method defined!
#endif
	return lReturnValue;
}

BoardState BoardState::CreateInitialPosition()
{
	BoardState lReturnState;
	
	for (int iRow = 0; iRow < cRows; iRow++)
	{
		for (int iCol = 0; iCol < cCols; iCol++)
		{
			SquareState lSquare;
			//lSquare.SquareColour = (iRow + iCol) % 2 == 0 ? Colour::Black : Colour::White;
			lSquare.Coordinates = BoardCoordinates(iRow, iCol);
			lSquare.Piece_ = P_NONE;
			
			const int lSquareIndex = CoToSI(iRow, iCol);
			lReturnState.Squares[lSquareIndex] = lSquare;
		}
	}
	
	for (int iCol = 0; iCol < cCols; iCol++)
	{
		const int lFirstRow = CoToSI(0, iCol);
		const int lSecondRow = CoToSI(1, iCol);
		const int lSeventhRow = CoToSI(6, iCol);
		const int lEighthRow = CoToSI(7, iCol);
		
		// Squares array
		
		// "major" piece for the first and eighth row
		Piece lPiece;
		if (iCol == 0 || iCol == 7) lPiece = P_ROOK;
		else if (iCol == 1 || iCol == 6) lPiece = P_KNIGHT;
		else if (iCol == 2 || iCol == 5) lPiece = P_BISHOP;
		else if (iCol == 3) lPiece = P_QUEEN;
		else if (iCol == 4) lPiece = P_KING;
		
		lReturnState.Squares[lFirstRow].Piece_ = lPiece & C_WHITE;
		lReturnState.Squares[lSecondRow].Piece_ = P_PAWN_WHITE;
		
		lReturnState.Squares[lSeventhRow].Piece_ = P_PAWN_BLACK;
		lReturnState.Squares[lEighthRow].Piece_ = lPiece & C_BLACK;
	}

	// kings
	lReturnState.WhiteKing = BoardCoordinates(0, 4);
	lReturnState.BlackKing = BoardCoordinates(7, 4);

	// board properties
	
	lReturnState.CanCastleQueensideWhite = true;
	lReturnState.CanCastleKingsideWhite = true;
	lReturnState.CanCastleQueensideBlack = true;
	lReturnState.CanCastleKingsideBlack = true;
	
	lReturnState.PlayerToMove = C_WHITE;

	lReturnState.LastMoveWhitePawnDoublePushCol = -1;
	lReturnState.LastMoveBlackPawnDoublePushCol = -1;

	// compute hash value
	lReturnState.HashValue = mStaticHasher(lReturnState);
	
	return lReturnState;
}
BoardState BoardState::CreateEmptyBoard()
{
	BoardState lReturnState;

	for (int iRow = 0; iRow < cRows; iRow++)
	{
		for (int iCol = 0; iCol < cCols; iCol++)
		{
			SquareState lSquare;
			lSquare.Coordinates = BoardCoordinates(iRow, iCol);
			lSquare.Piece_ = P_NONE;

			const int lSquareIndex = CoToSI(iRow, iCol);
			lReturnState.Squares[lSquareIndex] = lSquare;
		}
	}

	// kings
	lReturnState.WhiteKing = BoardCoordinates();
	lReturnState.BlackKing = BoardCoordinates();

	// board properties

	lReturnState.CanCastleQueensideWhite = false;
	lReturnState.CanCastleKingsideWhite = false;
	lReturnState.CanCastleQueensideBlack = false;
	lReturnState.CanCastleKingsideBlack = false;

	lReturnState.PlayerToMove = C_WHITE;

	lReturnState.LastMoveWhitePawnDoublePushCol = -1;
	lReturnState.LastMoveBlackPawnDoublePushCol = -1;

	// compute hash value
	lReturnState.HashValue = mStaticHasher(lReturnState);

	return lReturnState;
}

BoardState BoardState::MakeMove(const Move& aMove) const
{
	const int lStartInd = CoToSI(aMove.Start);
	const int lEndInd = CoToSI(aMove.End);
	
	if (aMove.Start == aMove.End) throw EXCEPTION_MEMBER("Same start and end square!");
	if (!IsIn(aMove.Start) || !IsIn(aMove.End)) throw EXCEPTION_MEMBER("Move not within the board!");
	if (Squares[lStartInd].Piece_ != aMove.MovedPiece) throw EXCEPTION_MEMBER("Wrong piece on the start square!");
	
	BoardState lNewState(*this);
	lNewState.LastMoveWhitePawnDoublePushCol = -1;
	lNewState.LastMoveBlackPawnDoublePushCol = -1;

	// set indicator for pawn double push
	if ((aMove.MovedPiece & P_PAWN) && (std::abs(aMove.Start.Row - aMove.End.Row) == 2))
	{
		if (PlayerToMove & C_WHITE)
			lNewState.LastMoveWhitePawnDoublePushCol = aMove.Start.Col;
		else
			lNewState.LastMoveBlackPawnDoublePushCol = aMove.Start.Col;
	}
	
	lNewState.Squares[lEndInd].Piece_ = P_NONE;

	// update the hash value
#if defined(HASH_INHOUSE)
	// remove piece at start position
	lNewState.HashValue &= ~((size_t)((size_t)1 << lStartInd));
	// add piece to the end position
	lNewState.HashValue |= ((size_t)1 << lEndInd);
#elif defined(HASH_ZOBRIST)
	// remove piece at start position
	lNewState.HashValue ^= Tables::ZobristHashes[aMove.MovedPiece][lStartInd];
	// add piece to the end position
	lNewState.HashValue ^= Tables::ZobristHashes[aMove.MovedPiece][lEndInd];
#endif

	// hash modifications for the special special cases will be handled later in corresponding if blocks (en passant, castle)

	if (aMove.IsEnPassant)
	{
		const int lCapturedSquareInd = CoToSI(aMove.Start.Row, aMove.End.Col);
		SquareState& lCapturedSquare = lNewState.Squares[lCapturedSquareInd];
		if (lCapturedSquare.Piece_ & P_PAWN & ~PlayerToMove)
		{
			lCapturedSquare.Piece_ = P_NONE;
		}
		else throw EXCEPTION_MEMBER("Invalid en passant!");

		// remove the captured piece from the hash
#if defined(HASH_INHOUSE)
		lNewState.HashValue &= ~((size_t)((size_t)1 << lCapturedSquareInd));
#elif defined(HASH_ZOBRIST)
		lNewState.HashValue ^= Tables::ZobristHashes[P_PAWN & ~PlayerToMove][lCapturedSquareInd];
#endif
	}

	lNewState.Squares[lStartInd].Piece_ = P_NONE;
	lNewState.Squares[lEndInd].Piece_ = Squares[lStartInd].Piece_;

	// check if it's a pawn promotion
	if (aMove.IsPromotion())
	{
		if (aMove.PromotionPiece & (P_PAWN | P_KING))
			throw EXCEPTION_MEMBER("Invalid promotion piece!");
		
		lNewState.Squares[lEndInd].Piece_= aMove.PromotionPiece & PlayerToMove;
	}

	// check if it's a castle and move a rook accordingly
	if (aMove.MovedPiece & P_KING && 
		GetRelativeRow(aMove.Start, PlayerToMove) == 0 && aMove.Start.Col == 4 &&
		std::abs(aMove.Start.Col - aMove.End.Col) == 2)
	{
		int lRookStartInd = CoToSI(BoardCoordinates(aMove.Start.Row, 7));
		int lRookEndInd = CoToSI(BoardCoordinates(aMove.Start.Row, 5));
		// king side
		if (aMove.Start.Col - aMove.End.Col == -2)
		{
			lRookStartInd = CoToSI(BoardCoordinates(aMove.Start.Row, 7));
			lRookEndInd = CoToSI(BoardCoordinates(aMove.Start.Row, 5));
		}
		// queen side
		else if (aMove.Start.Col - aMove.End.Col == 2)
		{
			lRookStartInd = CoToSI(BoardCoordinates(aMove.Start.Row, 0));
			lRookEndInd = CoToSI(BoardCoordinates(aMove.Start.Row, 3));
		}
		else throw EXCEPTION_MEMBER("Invalid castling!");

		// update hash
#if defined(HASH_INHOUSE)
		// remove rook at start position
		lNewState.HashValue &= ~((size_t)((size_t)1 << lRookStartInd));
		// add rook to the end position
		lNewState.HashValue |= ((size_t)1 << lRookEndInd);
#elif defined(HASH_ZOBRIST)
		// remove rook at start position
		lNewState.HashValue ^= Tables::ZobristHashes[P_ROOK & PlayerToMove][lRookStartInd];
		// add rook to the end position
		lNewState.HashValue ^= Tables::ZobristHashes[P_ROOK & PlayerToMove][lRookEndInd];
#endif

		lNewState.Squares[lRookEndInd].Piece_ = lNewState.Squares[lRookStartInd].Piece_;
		lNewState.Squares[lRookStartInd].Piece_ = P_NONE;
	}

	// check if any castling options were broken
	if (aMove.MovedPiece & P_KING)
	{
		if (aMove.MovedPiece & C_WHITE)
		{
			lNewState.CanCastleKingsideWhite = false;
			lNewState.CanCastleQueensideWhite = false;
		}
		if (aMove.MovedPiece & C_BLACK)
		{
			lNewState.CanCastleKingsideBlack = false;
			lNewState.CanCastleQueensideBlack = false;
		}
	}
	else if (aMove.MovedPiece & P_ROOK)
	{
		if (aMove.Start.Col == 0)
		{
			if (aMove.MovedPiece & C_WHITE) lNewState.CanCastleQueensideWhite = false;
			if (aMove.MovedPiece & C_BLACK) lNewState.CanCastleQueensideBlack = false;
		}
		if (aMove.Start.Col == 7)
		{
			if (aMove.MovedPiece & C_WHITE) lNewState.CanCastleKingsideWhite = false;
			if (aMove.MovedPiece & C_BLACK) lNewState.CanCastleKingsideBlack = false;
		}
	}

	// update king position if needed
	if (aMove.MovedPiece & P_KING)
	{
		if (aMove.MovedPiece & C_WHITE)
		{
			lNewState.WhiteKing = aMove.End;
		}
		else
		{
			lNewState.BlackKing = aMove.End;
		}
	}
	
	if (PlayerToMove & C_WHITE) lNewState.PlayerToMove = C_BLACK;
	else lNewState.PlayerToMove = C_WHITE;

#if defined(HASH_INHOUSE)
	if (cInvertHashForBlack)
	{
		// invert the hash value as the player to move changes
		lNewState.HashValue = ~lNewState.HashValue;
	}
#endif
	
	return lNewState;
}

BoardState BoardState::MakeMoveRaw(const Move& aMove) const
{
	const int lStartInd = CoToSI(aMove.Start);
	const int lEndInd = CoToSI(aMove.End);

	const BoardState& lThis = *this;

	BoardState lNewState(lThis);
	lNewState.LastMoveWhitePawnDoublePushCol = -1;
	lNewState.LastMoveBlackPawnDoublePushCol = -1;

	// set indicator for pawn double push
	if ((aMove.MovedPiece & P_PAWN) && (std::abs(aMove.Start.Row - aMove.End.Row) == 2))
	{
		if (PlayerToMove & C_WHITE)
			lNewState.LastMoveWhitePawnDoublePushCol = aMove.Start.Col;
		else
			lNewState.LastMoveBlackPawnDoublePushCol = aMove.Start.Col;
	}

	lNewState.Squares[lEndInd].Piece_ = P_NONE;
	
	lNewState.Squares[lStartInd].Piece_ = P_NONE;
	lNewState.Squares[lEndInd].Piece_ = Squares[lStartInd].Piece_;

	// update the hash value
#if defined(HASH_INHOUSE)
	// remove piece at start position
	lNewState.HashValue &= ~((size_t)((size_t)1 << lStartInd));
	// add piece to the end position
	lNewState.HashValue |= ((size_t)1 << lEndInd);
#elif defined(HASH_ZOBRIST)
	// remove piece at start position
	lNewState.HashValue ^= Tables::ZobristHashes[aMove.MovedPiece][lStartInd];
	// add piece to the end position
	lNewState.HashValue ^= Tables::ZobristHashes[aMove.MovedPiece][lEndInd];
#endif

	// hash modifications for the special special cases will be handled later in corresponding if blocks (en passant, castle)

	if (aMove.IsEnPassant)
	{
		const int lCapturedSquareInd = CoToSI(aMove.Start.Row, aMove.End.Col);
		SquareState& lCapturedSquare = lNewState.Squares[lCapturedSquareInd];
		lCapturedSquare.Piece_ = P_NONE;

		// remove the captured piece from the hash
#if defined(HASH_INHOUSE)
		lNewState.HashValue &= ~((size_t)((size_t)1 << lCapturedSquareInd));
#elif defined(HASH_ZOBRIST)
		lNewState.HashValue ^= Tables::ZobristHashes[P_PAWN & ~PlayerToMove][lCapturedSquareInd];
#endif
	}

	// check if it's a pawn promotion
	if (aMove.IsPromotion())
	{
		lNewState.Squares[lEndInd].Piece_ = aMove.PromotionPiece & PlayerToMove;
	}

	// check if it's a castle and move a rook accordingly
	if (aMove.IsCastle())
	{
		int lRookStartInd = CoToSI(BoardCoordinates(aMove.Start.Row, 7));
		int lRookEndInd = CoToSI(BoardCoordinates(aMove.Start.Row, 5));
		// king side
		if (aMove.Start.Col - aMove.End.Col == -2)
		{
			lRookStartInd = CoToSI(BoardCoordinates(aMove.Start.Row, 7));
			lRookEndInd = CoToSI(BoardCoordinates(aMove.Start.Row, 5));
		}
		// queen side
		else if (aMove.Start.Col - aMove.End.Col == 2)
		{
			lRookStartInd = CoToSI(BoardCoordinates(aMove.Start.Row, 0));
			lRookEndInd = CoToSI(BoardCoordinates(aMove.Start.Row, 3));
		}

		// update hash
#if defined(HASH_INHOUSE)
		// remove rook at start position
		lNewState.HashValue &= ~((size_t)((size_t)1 << lRookStartInd));
		// add rook to the end position
		lNewState.HashValue |= ((size_t)1 << lRookEndInd);
#elif defined(HASH_ZOBRIST)
		// remove rook at start position
		lNewState.HashValue ^= Tables::ZobristHashes[P_ROOK & PlayerToMove][lRookStartInd];
		// add rook to the end position
		lNewState.HashValue ^= Tables::ZobristHashes[P_ROOK & PlayerToMove][lRookEndInd];
#endif

		lNewState.Squares[lRookEndInd].Piece_ = lNewState.Squares[lRookStartInd].Piece_;
		lNewState.Squares[lRookStartInd].Piece_ = P_NONE;
	}

	// check if any castling options were broken
	if (aMove.MovedPiece & P_KING)
	{
		if (aMove.MovedPiece & C_WHITE)
		{
			lNewState.CanCastleKingsideWhite = false;
			lNewState.CanCastleQueensideWhite = false;
		}
		if (aMove.MovedPiece & C_BLACK)
		{
			lNewState.CanCastleKingsideBlack = false;
			lNewState.CanCastleQueensideBlack = false;
		}
	}
	else if (aMove.MovedPiece & P_ROOK)
	{
		if (aMove.Start.Col == 0)
		{
			if (aMove.MovedPiece & C_WHITE) lNewState.CanCastleQueensideWhite = false;
			if (aMove.MovedPiece & C_BLACK) lNewState.CanCastleQueensideBlack = false;
		}
		if (aMove.Start.Col == 7)
		{
			if (aMove.MovedPiece & C_WHITE) lNewState.CanCastleKingsideWhite = false;
			if (aMove.MovedPiece & C_BLACK) lNewState.CanCastleKingsideBlack = false;
		}
	}

	// update king position if needed
	if (aMove.MovedPiece & P_KING)
	{
		if (aMove.MovedPiece & C_WHITE)
		{
			lNewState.WhiteKing = aMove.End;
		}
		else
		{
			lNewState.BlackKing = aMove.End;
		}
	}

	// player to move
	if (PlayerToMove & C_WHITE) lNewState.PlayerToMove = C_BLACK;
	else lNewState.PlayerToMove = C_WHITE;

#if defined(HASH_INHOUSE)
	if (cInvertHashForBlack)
	{
		// invert the hash value as the player to move changes
		lNewState.HashValue = ~lNewState.HashValue;
	}
#endif

	return lNewState;
}

// the mate is always checked for player who is about to move (PlayerToMove)
bool BoardState::IsMate(const bool aCheckedForCheckAlready) const
{
	if (aCheckedForCheckAlready || IsMovingPlayerInCheck())
	{
		mMoveBuffer.clear();
		const bool lAnyLegalMoves = PieceMovement::AnyLegalMoves(*this, mMoveBuffer);

		if (!lAnyLegalMoves)
			return true;
	}
	return false;
}

// the stelemete is always checked for player who is about to move (PlayerToMove)
bool BoardState::IsStalemate(const bool aCheckedForCheckAlready) const
{
	BoardState lBoardCopy = *this;
	lBoardCopy.PlayerToMove = GetOppositeColour(this->PlayerToMove);

	//const BoardCoordinates lKingCoord = FindFirstPiece(*this, Piece(PieceType::King, PlayerToMove));	
	BoardCoordinates lKingCoord;
	if (PlayerToMove & C_WHITE) lKingCoord = WhiteKing;
	else lKingCoord = BlackKing;

	if (!IsIn(lKingCoord))
		throw EXCEPTION_MEMBER("King not found!");

	if (aCheckedForCheckAlready || !IsAttacked(lBoardCopy, lKingCoord))
	{
		if (!PieceMovement::AnyLegalMoves(*this, mMoveBuffer))
		{
			return true;
		}
	}
	return false;
}

BoardState BoardState::FromFEN(const std::string& aFEN)
{
	BoardState lReturnState = CreateEmptyBoard();

	const std::vector<std::string> lParts = SplitString(aFEN, " ");
	if (lParts.size() < 4) throw EXCEPTION_GLOBAL("Expected at least 4 strings in the FEN string!");

	// piece placement
	const std::string lPiecesString = lParts[0];
	const std::vector<std::string> lRowsString = SplitString(lPiecesString, "/");

	if (lRowsString.size() != cRows) throw EXCEPTION_GLOBAL("Expected 8 strings for 8 rows!");

	bool lWhiteKingSet = false;
	bool lBlackKingSet = false;

	for (int iRow = cRows - 1; iRow >= 0; iRow--)
	{
		const std::string lRowString = lRowsString[cRows - 1 - iRow];
		int lCol = 0;

		for (int i = 0; i < lRowString.size(); i++)
		{
			const char lChar = lRowString[i];

			if (lChar >= '0' && lChar <= '9')
			{
				lCol += lChar - '0';
				continue;
			}
			const std::string lString = std::string(1, lChar);
			if (Tables::FENSymbolToPiece.find(lString) == Tables::FENSymbolToPiece.end())
				throw EXCEPTION_GLOBAL("Unrecognised FEN piece symbol: " + lString + "!");

			const Piece lPiece = Tables::FENSymbolToPiece[lString];
			const int lSquareIndex = CoToSI(iRow, lCol);
			lReturnState.Squares[lSquareIndex].Piece_ = lPiece;

			if (lReturnState.Squares[lSquareIndex].Piece_ == P_KING_WHITE)
			{
				lReturnState.WhiteKing = BoardCoordinates(iRow, lCol);
				lWhiteKingSet = true;
			}
			else if (lReturnState.Squares[lSquareIndex].Piece_ == P_KING_BLACK)
			{
				lReturnState.BlackKing = BoardCoordinates(iRow, lCol);
				lBlackKingSet = true;
			}

			lCol++;
		}
	}

	if (!lWhiteKingSet || !lBlackKingSet)
	{
		throw EXCEPTION_GLOBAL("At least one king was not set from the FEN!");
	}

	// player to move
	if (lParts[1].size() == 1 && tolower(lParts[1][0]) == 'w')
		lReturnState.PlayerToMove = C_WHITE;
	else if (lParts[1].size() == 1 && tolower(lParts[1][0]) == 'b')
		lReturnState.PlayerToMove = C_BLACK;
	else throw EXCEPTION_GLOBAL("Unrecognised player to move part of FEN: " + lParts[1] + "!");

	// castling rights
	for (int i = 0; i < lParts[2].size(); i++)
	{
		const char lChar = lParts[2][i];
		if (lChar == 'K')
			lReturnState.CanCastleKingsideWhite = true;
		else if (lChar == 'Q')
			lReturnState.CanCastleQueensideWhite = true;
		if (lChar == 'k')
			lReturnState.CanCastleKingsideBlack = true;
		else if (lChar == 'q')
			lReturnState.CanCastleQueensideBlack = true;
	}

	// pawn 2 square move
	if (lParts[3].size() == 2)
	{
		const BoardCoordinates lCoord = PGNCoordToBoardCoord(lParts[3]);

		if (lReturnState.PlayerToMove == C_WHITE)
		{
			lReturnState.LastMoveBlackPawnDoublePushCol = lCoord.Col;
		}
		else
		{
			lReturnState.LastMoveWhitePawnDoublePushCol = lCoord.Col;
		}
	}
	else
	{
		lReturnState.LastMoveWhitePawnDoublePushCol = -1;
		lReturnState.LastMoveBlackPawnDoublePushCol = -1;
	}

	return lReturnState;
}

std::string BoardState::ToFEN(const int aPliesFromLastPawnCorA, const int aMoveNumber) const
{
	std::string lReturnString;

	// piece placement
	for (int iRow = cRows - 1; iRow >= 0; iRow--)
	{
		int lEmptyCount = 0;
		for (int iCol = 0; iCol < cCols; iCol++)
		{
			const int lSquareIndex = CoToSI(BoardCoordinates(iRow, iCol));
			const SquareState& lSquare = Squares[lSquareIndex];
			const Piece& lPiece = lSquare.Piece_;

			if (!lPiece)
			{
				lEmptyCount++;
			}
			else
			{
				if (lEmptyCount > 0)
				{
					lReturnString += std::to_string(lEmptyCount);
					lEmptyCount = 0;
				}
				const std::string lFENSymbol = Tables::FENPieceToSymbol[lPiece];
				lReturnString += lFENSymbol;
			}
		}
		if (lEmptyCount > 0) lReturnString += std::to_string(lEmptyCount);
		
		if (iRow > 0) lReturnString += "/";
	}

	lReturnString += " ";

	// player to move
	if (this->PlayerToMove & C_WHITE) lReturnString += "w";
	else lReturnString += "b";

	lReturnString += " ";

	// castling rights
	bool lAnyCastle = false;
	if (this->CanCastleKingsideWhite)
	{
		lReturnString += "K";
		lAnyCastle = true;
	}
	if (this->CanCastleQueensideWhite)
	{
		lReturnString += "Q";
		lAnyCastle = true;
	}
	if (this->CanCastleKingsideBlack)
	{
		lReturnString += "k";
		lAnyCastle = true;
	}
	if (this->CanCastleQueensideBlack)
	{
		lReturnString += "q";
		lAnyCastle = true;
	}
	if (!lAnyCastle) lReturnString += "-";

	lReturnString += " ";

	// pawn 2 square move
	if (this->LastMoveWhitePawnDoublePushCol >= 0)
	{
		lReturnString += ('a' + this->LastMoveWhitePawnDoublePushCol);
		lReturnString += "3";
	}
	else if (this->LastMoveBlackPawnDoublePushCol >= 0)
	{
		lReturnString += ('a' + this->LastMoveBlackPawnDoublePushCol);
		lReturnString += "6";
	}
	else lReturnString += "-";

	lReturnString += " ";

	// number of plies from last pawn capture or advance

	if (aPliesFromLastPawnCorA >= 0)
		lReturnString += std::to_string(aPliesFromLastPawnCorA);
	else lReturnString += "0";

	lReturnString += " ";

	// move number

	if (aMoveNumber >= 1) lReturnString += std::to_string(aMoveNumber);
	else lReturnString += "1";

	return lReturnString;
}

int BoardState::CountPositions(const int aDepth, const int aStartDepth) const
{
	if (aDepth > aStartDepth)
	{
		throw EXCEPTION_MEMBER("Depth larger than start depth!");
	}
	if (aDepth <= 0) return 1;
	else if (aDepth == 1)
	{
		StaticList<Move, PieceMovement::cMaxLegalMoves> lChildren;
		PieceMovement::GetAllLegalMoves(*this, lChildren);
		return lChildren.ActualSize;
	}

	StaticList<Move, PieceMovement::cMaxLegalMoves> lChildren;
	PieceMovement::GetAllLegalMoves(*this, lChildren);

	int lSum = 0;

#pragma omp parallel for reduction (+:lSum) if (aDepth == aStartDepth) schedule(dynamic)
	for (int i = 0; i < lChildren.ActualSize; i++)
	{
		const BoardState lBoard = this->MakeMoveRaw(lChildren.Items[i]);
		lSum += lBoard.CountPositions(aDepth - 1, aStartDepth);
	}

	return lSum;
}

bool BoardState::IsMovingPlayerInCheck() const
{
	BoardState lOppPlayer(*this);
	lOppPlayer.PlayerToMove = GetOppositeColour(lOppPlayer.PlayerToMove);

	const bool lReturnValue = IsAttacked(lOppPlayer, (this->PlayerToMove & C_WHITE) ? this->WhiteKing : this->BlackKing);
	return lReturnValue;
}