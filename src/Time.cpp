
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include "Time.h"

void Time::Start()
{
    mCurrentStart = std::chrono::steady_clock::now();
    mLastReportTime = std::chrono::steady_clock::now();
	
	mIsRunning = true;
}

double Time::Stop(const bool& aPrint)
{
	if (mIsRunning)
	{
		mCurrentStop = std::chrono::steady_clock::now();
	}
	
	mIsRunning = false;

    std::chrono::duration<double, std::milli> lDurationMS = mCurrentStop - mCurrentStart;
    const double lMs = lDurationMS.count();
    
    if (aPrint) std::cout << "Elapsed time: " << lMs << " ms" << std::endl;
    
    return lMs;
}

double Time::Elapsed() const
{
	std::chrono::duration<double, std::milli> lDurationMS;
	double lMs;
	
	if (mIsRunning)
	{
		std::chrono::steady_clock::time_point lNow = std::chrono::steady_clock::now();
		lDurationMS = lNow - mCurrentStart;
	}
	else
	{
		lDurationMS = mCurrentStop - mCurrentStart;
	}
	
	lMs = lDurationMS.count();
	
	return lMs;
}

void Time::Report(const std::string& aMessage, double aDurationMS, bool aUpdateLastReportTime)
{
    std::chrono::steady_clock::time_point lNow = std::chrono::steady_clock::now();

    std::chrono::duration<double, std::milli> lDurationMS = lNow - mLastReportTime;
    double lElapsed = lDurationMS.count();
    
    if (lElapsed >= aDurationMS) 
    {
        std::cout << aMessage << std::endl;
        if (aUpdateLastReportTime) mLastReportTime = std::chrono::steady_clock::now();
    }
}
time_t Time::CurrentTime()
{
	std::chrono::system_clock::time_point lNow = std::chrono::system_clock::now();
	return std::chrono::system_clock::to_time_t(lNow);
}
std::string Time::TimeToUTCString(const time_t& aTime)
{
	tm* lGMTime = nullptr;
#if defined (_WIN32)
	lGMTime = gmtime(&aTime);
#elif defined(__linux__)
	// TODO use gmtime_r or gmtime_s here
	tm lGMTimeTmp;
	gmtime_r(&aTime, &lGMTimeTmp);
	lGMTime =  &lGMTimeTmp;
#endif
	
	if (lGMTime == nullptr) throw "Invalid GM time!";
	
	const int lBufferSize = 100;
	char lBuffer[lBufferSize];
	
	strftime(lBuffer, lBufferSize, "%F_%T", lGMTime);
	
	std::string lReturnValue = std::string(lBuffer);

	std::replace(lReturnValue.begin(), lReturnValue.end(), ':', '_'); // to comply with Windows where ':' char can not be in a path string
	return lReturnValue;
}
std::string Time::CurrentUTCTimeString()
{
	return TimeToUTCString(CurrentTime());
}