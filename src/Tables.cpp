
#include "Tables.h"

#include <iostream>
#include <limits>

#include "Random.h"

#include "FunctionsMove.h"

LocalisedPieceIndex Tables::AttacksWhiteArray[Tables::cAttackTableSize];
bool Tables::AttacksWhiteLineStartPoints[Tables::cAttackTableSize];
int Tables::AttacksWhiteSquareStartPoints[BoardSetup::cSquareCount];

LocalisedPieceIndex Tables::AttacksBlackArray[Tables::cAttackTableSize];
bool Tables::AttacksBlackLineStartPoints[Tables::cAttackTableSize];
int Tables::AttacksBlackSquareStartPoints[BoardSetup::cSquareCount];

std::map<Piece, std::vector<size_t>> Tables::ZobristHashes;
std::map<Piece, std::string> Tables::FENPieceToSymbol;
std::map<std::string, Piece> Tables::FENSymbolToPiece;

void Tables::InitTables()
{
	std::cout << "Initialising tables ... " << std::endl;

	////////////////////
	// ATTACKS FOR WHITE
	////////////////////

	for (int i = 0; i < cAttackTableSize; i++)
	{
		AttacksWhiteLineStartPoints[i] = false;
	}

	int lArrayIndex = 0;

	for (int iSq = 0; iSq < BoardSetup::cSquareCount; iSq++)
	{
		AttacksWhiteSquareStartPoints[iSq] = lArrayIndex;

		const BoardCoordinates lSquareCoord = SIToCo(iSq);

		//std::vector<std::vector<LocalisedPieceIndex>> lAttacksForThisSquare;

		// check for knights
		const char lKnightRowDiff[] = { -2, -1, 1, 2, 2, 1, -1, -2 };
		const char lKnightColDiff[] = { -1, -2, -2, -1, 1, 2, 2, 1 };

		BoardCoordinates lStartCoord;

		for (int i = 0; i < 8; i++)
		{
			lStartCoord.Row = lSquareCoord.Row + lKnightRowDiff[i];
			lStartCoord.Col = lSquareCoord.Col + lKnightColDiff[i];
			if (!IsIn(lStartCoord)) continue;

			AttacksWhiteLineStartPoints[lArrayIndex] = true;

			//std::vector<LocalisedPieceIndex> lKnightAttack;
			//lKnightAttack.push_back(LocalisedPieceIndex(P_KNIGHT_WHITE, lStartCoord));
			//lAttacksForThisSquare.push_back(lKnightAttack);

			AttacksWhiteArray[lArrayIndex++] = LocalisedPieceIndex(P_KNIGHT_WHITE, lStartCoord);
		}

		// check for bishops and queens
		const char lDiagRowDiff[] = { 1, 1, -1, -1 };
		const char lDiagColDiff[] = { 1, -1, 1, -1 };

		for (int i = 0; i < 4; i++)
		{
			char lDist = 0;

			const char lCurentRowDiff = lDiagRowDiff[i];
			const char lCurrentColDiff = lDiagColDiff[i];

			//std::vector<LocalisedPieceIndex> lQueenOrBishopAttack;

			while (true)
			{
				lDist++;

				lStartCoord.Row = lSquareCoord.Row + lDist * lCurentRowDiff;
				lStartCoord.Col = lSquareCoord.Col + lDist * lCurrentColDiff;

				if (IsIn(lStartCoord))
				{
					if (lDist == 1)
						AttacksWhiteLineStartPoints[lArrayIndex] = true;

					AttacksWhiteArray[lArrayIndex++] = LocalisedPieceIndex(P_QUEEN_WHITE | P_BISHOP_WHITE, lStartCoord);
					//lQueenOrBishopAttack.push_back(LocalisedPieceIndex(P_QUEEN_WHITE | P_BISHOP_WHITE, lStartCoord));
				}
				else
				{
					//if (lQueenOrBishopAttack.size() > 0)
					//	lAttacksForThisSquare.push_back(lQueenOrBishopAttack);

					break;
				}
			}
		}

		// check for rooks or queens
		const char lStarightRowDiff[] = { 1, 0, -1, 0 };
		const char lStarightColDiff[] = { 0, 1, 0, -1 };

		for (int i = 0; i < 4; i++)
		{
			char lDist = 0;

			const char lCurentRowDiff = lStarightRowDiff[i];
			const char lCurrentColDiff = lStarightColDiff[i];

			//std::vector<LocalisedPieceIndex> lQueenOrRookAttack;

			while (true)
			{
				lDist++;

				lStartCoord.Row = lSquareCoord.Row + lDist * lCurentRowDiff;
				lStartCoord.Col = lSquareCoord.Col + lDist * lCurrentColDiff;

				if (IsIn(lStartCoord))
				{
					if (lDist == 1)
						AttacksWhiteLineStartPoints[lArrayIndex] = true;

					AttacksWhiteArray[lArrayIndex++] = LocalisedPieceIndex(P_QUEEN_WHITE | P_ROOK_WHITE, lStartCoord);
					//lQueenOrRookAttack.push_back(LocalisedPieceIndex(P_QUEEN_WHITE | P_ROOK_WHITE, lStartCoord));
				}
				else
				{
					//if (lQueenOrRookAttack.size() > 0)
					//	lAttacksForThisSquare.push_back(lQueenOrRookAttack);

					break;
				}
			}
		}

		// king attacks
		const char lAllDirRowDiff[] = { 1, 0, -1, 0, 1, 1, -1, -1 };
		const char lAllDirColDiff[] = { 0, 1, 0, -1, 1, -1, 1, -1 };

		for (int i = 0; i < 8; i++)
		{
			char lDist = 1;

			const char lCurentRowDiff = lAllDirRowDiff[i];
			const char lCurrentColDiff = lAllDirColDiff[i];

			std::vector<LocalisedPieceIndex> lKingAttack;

			lStartCoord.Row = lSquareCoord.Row + lDist * lCurentRowDiff;
			lStartCoord.Col = lSquareCoord.Col + lDist * lCurrentColDiff;

			if (IsIn(lStartCoord))
			{
				AttacksWhiteLineStartPoints[lArrayIndex] = true;
				//lKingAttack.push_back(LocalisedPieceIndex(P_KING_WHITE, lStartCoord));
				//lAttacksForThisSquare.push_back(lKingAttack);

				AttacksWhiteArray[lArrayIndex++] = LocalisedPieceIndex(P_KING_WHITE, lStartCoord);
			}
		}

		// check for pawns, only standard capture (no en passant)
		const char lForwardDir = GetForwardRowDirection(C_WHITE);

		// we are at the end square of a potential capture, so we need to look in the opposite of the forward direction
		const BoardCoordinates lPawnCaptureStart1(lSquareCoord.Row - lForwardDir, lSquareCoord.Col + char(1));
		const BoardCoordinates lPawnCaptureStart2(lSquareCoord.Row - lForwardDir, lSquareCoord.Col - char(1));

		if (IsIn(lPawnCaptureStart1))
		{
			AttacksWhiteLineStartPoints[lArrayIndex] = true;
			//std::vector<LocalisedPieceIndex> lPawnAttack;
			//lPawnAttack.push_back(LocalisedPieceIndex(P_PAWN_WHITE, lPawnCaptureStart1));
			//lAttacksForThisSquare.push_back(lPawnAttack);
			AttacksWhiteArray[lArrayIndex++] = LocalisedPieceIndex(P_PAWN_WHITE, lPawnCaptureStart1);
		}

		if (IsIn(lPawnCaptureStart2))
		{
			AttacksWhiteLineStartPoints[lArrayIndex] = true;
			//std::vector<LocalisedPieceIndex> lPawnAttack;
			//lPawnAttack.push_back(LocalisedPieceIndex(P_PAWN_WHITE, lPawnCaptureStart2));
			//lAttacksForThisSquare.push_back(lPawnAttack);
			AttacksWhiteArray[lArrayIndex++] = LocalisedPieceIndex(P_PAWN_WHITE, lPawnCaptureStart2);
		}

		//AttacksWhite.push_back(lAttacksForThisSquare);
	}

	if (lArrayIndex != cAttackTableSize) throw EXCEPTION_GLOBAL("Array size not correct!");
	
	////////////////////
	// ATTACKS FOR BLACK
	////////////////////	

	for (int i = 0; i < cAttackTableSize; i++)
	{
		AttacksBlackLineStartPoints[i] = false;
	}

	lArrayIndex = 0;

	for (int iSq = 0; iSq < BoardSetup::cSquareCount; iSq++)
	{
		AttacksBlackSquareStartPoints[iSq] = lArrayIndex;

		const BoardCoordinates lSquareCoord = SIToCo(iSq);

		//std::vector<std::vector<LocalisedPieceIndex>> lAttacksForThisSquare;

		// check for knights
		const char lKnightRowDiff[] = { -2, -1, 1, 2, 2, 1, -1, -2 };
		const char lKnightColDiff[] = { -1, -2, -2, -1, 1, 2, 2, 1 };

		BoardCoordinates lStartCoord;

		for (int i = 0; i < 8; i++)
		{
			lStartCoord.Row = lSquareCoord.Row + lKnightRowDiff[i];
			lStartCoord.Col = lSquareCoord.Col + lKnightColDiff[i];
			if (!IsIn(lStartCoord)) continue;

			//std::vector<LocalisedPieceIndex> lKnightAttack;
			//lKnightAttack.push_back(LocalisedPieceIndex(P_KNIGHT_WHITE, lStartCoord));
			//lAttacksForThisSquare.push_back(lKnightAttack);

			AttacksBlackLineStartPoints[lArrayIndex] = true;

			AttacksBlackArray[lArrayIndex++] = LocalisedPieceIndex(P_KNIGHT_BLACK, lStartCoord);
		}

		// check for bishops and queens
		const char lDiagRowDiff[] = { 1, 1, -1, -1 };
		const char lDiagColDiff[] = { 1, -1, 1, -1 };

		for (int i = 0; i < 4; i++)
		{
			char lDist = 0;

			const char lCurentRowDiff = lDiagRowDiff[i];
			const char lCurrentColDiff = lDiagColDiff[i];

			//std::vector<LocalisedPieceIndex> lQueenOrBishopAttack;

			while (true)
			{
				lDist++;

				lStartCoord.Row = lSquareCoord.Row + lDist * lCurentRowDiff;
				lStartCoord.Col = lSquareCoord.Col + lDist * lCurrentColDiff;

				if (IsIn(lStartCoord))
				{
					if (lDist == 1)
						AttacksBlackLineStartPoints[lArrayIndex] = true;

					AttacksBlackArray[lArrayIndex++] = LocalisedPieceIndex(P_QUEEN_BLACK | P_BISHOP_BLACK, lStartCoord);
					//lQueenOrBishopAttack.push_back(LocalisedPieceIndex(P_QUEEN_WHITE | P_BISHOP_WHITE, lStartCoord));
				}
				else
				{
					//if (lQueenOrBishopAttack.size() > 0)
					//	lAttacksForThisSquare.push_back(lQueenOrBishopAttack);

					break;
				}
			}
		}

		// check for rooks or queens
		const char lStarightRowDiff[] = { 1, 0, -1, 0 };
		const char lStarightColDiff[] = { 0, 1, 0, -1 };

		for (int i = 0; i < 4; i++)
		{
			char lDist = 0;

			const char lCurentRowDiff = lStarightRowDiff[i];
			const char lCurrentColDiff = lStarightColDiff[i];

			//std::vector<LocalisedPieceIndex> lQueenOrRookAttack;

			while (true)
			{
				lDist++;

				lStartCoord.Row = lSquareCoord.Row + lDist * lCurentRowDiff;
				lStartCoord.Col = lSquareCoord.Col + lDist * lCurrentColDiff;

				if (IsIn(lStartCoord))
				{
					if (lDist == 1)
						AttacksBlackLineStartPoints[lArrayIndex] = true;

					AttacksBlackArray[lArrayIndex++] = LocalisedPieceIndex(P_QUEEN_BLACK | P_ROOK_BLACK, lStartCoord);
					//lQueenOrRookAttack.push_back(LocalisedPieceIndex(P_QUEEN_WHITE | P_ROOK_WHITE, lStartCoord));
				}
				else
				{
					//if (lQueenOrRookAttack.size() > 0)
					//	lAttacksForThisSquare.push_back(lQueenOrRookAttack);

					break;
				}
			}
		}

		// king attacks
		const char lAllDirRowDiff[] = { 1, 0, -1, 0, 1, 1, -1, -1 };
		const char lAllDirColDiff[] = { 0, 1, 0, -1, 1, -1, 1, -1 };

		for (int i = 0; i < 8; i++)
		{
			char lDist = 1;

			const char lCurentRowDiff = lAllDirRowDiff[i];
			const char lCurrentColDiff = lAllDirColDiff[i];

			std::vector<LocalisedPieceIndex> lKingAttack;

			lStartCoord.Row = lSquareCoord.Row + lDist * lCurentRowDiff;
			lStartCoord.Col = lSquareCoord.Col + lDist * lCurrentColDiff;

			if (IsIn(lStartCoord))
			{
				//lKingAttack.push_back(LocalisedPieceIndex(P_KING_WHITE, lStartCoord));
				//lAttacksForThisSquare.push_back(lKingAttack);

				AttacksBlackLineStartPoints[lArrayIndex] = true;

				AttacksBlackArray[lArrayIndex++] = LocalisedPieceIndex(P_KING_BLACK, lStartCoord);
			}
		}

		// check for pawns, only standard capture (no en passant)
		const char lForwardDir = GetForwardRowDirection(C_BLACK);

		// we are at the end square of a potential capture, so we need to look in the opposite of the forward direction
		const BoardCoordinates lPawnCaptureStart1(lSquareCoord.Row - lForwardDir, lSquareCoord.Col + char(1));
		const BoardCoordinates lPawnCaptureStart2(lSquareCoord.Row - lForwardDir, lSquareCoord.Col - char(1));

		if (IsIn(lPawnCaptureStart1))
		{
			//std::vector<LocalisedPieceIndex> lPawnAttack;
			//lPawnAttack.push_back(LocalisedPieceIndex(P_PAWN_WHITE, lPawnCaptureStart1));
			//lAttacksForThisSquare.push_back(lPawnAttack);
			AttacksBlackLineStartPoints[lArrayIndex] = true;
			AttacksBlackArray[lArrayIndex++] = LocalisedPieceIndex(P_PAWN_BLACK, lPawnCaptureStart1);
		}

		if (IsIn(lPawnCaptureStart2))
		{
			//std::vector<LocalisedPieceIndex> lPawnAttack;
			//lPawnAttack.push_back(LocalisedPieceIndex(P_PAWN_WHITE, lPawnCaptureStart2));
			//lAttacksForThisSquare.push_back(lPawnAttack);
			AttacksBlackLineStartPoints[lArrayIndex] = true;
			AttacksBlackArray[lArrayIndex++] = LocalisedPieceIndex(P_PAWN_BLACK, lPawnCaptureStart2);
		}

		//AttacksWhite.push_back(lAttacksForThisSquare);
	}

	if (lArrayIndex != cAttackTableSize) throw EXCEPTION_GLOBAL("Array size not correct!");
	
	// initialise Zobrist hashes

	for (int i = 0; i < sizeof(C_AllInividualPieces) / sizeof(Piece); i++)
	{
		const Piece lPiece = C_AllInividualPieces[i];

		ZobristHashes[lPiece] = std::vector<size_t>();
		ZobristHashes[lPiece].reserve(BoardSetup::cSquareCount);

		for (int i = 0; i < BoardSetup::cSquareCount; i++)
		{
			ZobristHashes[lPiece].push_back(Random::GetIntU64(0, std::numeric_limits<unsigned long long>::max()));
		}
	}

	// initialise FEN piece symbols

	for (int i = 0; i < sizeof(C_AllInividualPieces) / sizeof(Piece); i++)
	{
		const Piece lPiece = C_AllInividualPieces[i];

		std::string lPieceSymbol = GetPiecePGNSymbolFromPiece(lPiece);
		if (lPieceSymbol == "") lPieceSymbol = "P";

		if (lPiece & C_BLACK)
		{
			std::transform(lPieceSymbol.begin(), lPieceSymbol.end(), lPieceSymbol.begin(),
				[](unsigned char c) { return std::tolower(c); });
		}

		FENPieceToSymbol.insert({ lPiece, lPieceSymbol });
		FENSymbolToPiece.insert({ lPieceSymbol, lPiece });
	}

	std::cout << "Initialising tables ... done" << std::endl;
}