
#pragma once

#include "BoardSetup.h"
#include "BoardCoordinates.h"
#include "Exception.h"

inline char CoToSI(const char aRow, const char aCol) { return aCol * BoardSetup::cRows + aRow; }
inline char CoToSI(const BoardCoordinates& aCoordinates) { return CoToSI(aCoordinates.Row, aCoordinates.Col); }
//inline BoardCoordinates SIToCo(const char aSquareIndex) { return BoardCoordinates(aSquareIndex % BoardSetup::cRows, aSquareIndex / BoardSetup::cRows); }
inline BoardCoordinates SIToCo(const char aSquareIndex) { return BoardCoordinates(aSquareIndex % BoardSetup::cRows, aSquareIndex >> 3); }
inline bool IsIn(const char aRow, const char aCol) { return aRow >= 0 && aRow < BoardSetup::cRows && aCol >= 0 && aCol < BoardSetup::cCols; }
inline bool IsIn(const BoardCoordinates& aCoordinates) { return IsIn(aCoordinates.Row, aCoordinates.Col); }
inline bool IsIn(const char aIndex) { return aIndex >= 0 && aIndex < BoardSetup::cSquareCount; }
// returns row index relative for the colour, i.e. just copies the row index for white and reverts it for black (7 - index)
inline char GetRelativeRow(const BoardCoordinates& aCoordinates, const Colour aColour)
{
    //if (aColour == Colour::White) return aCoordinates.Row;
    //else if (aColour == Colour::Black) return BoardSetup::cRows - 1 - aCoordinates.Row;
    //else throw EXCEPTION_GLOBAL("Unknown colour!");    
	
	if (aColour & C_WHITE) return aCoordinates.Row;
	else return BoardSetup::cRows - 1 - aCoordinates.Row;
}
inline bool IsLastRow(const BoardCoordinates& aCoordinates, const Colour aColour) 
{
    const int lRelRow = GetRelativeRow(aCoordinates, aColour);
    return lRelRow == BoardSetup::cRows - 1;
}
// forward direction in sense of the pawn progress
inline int GetForwardRowDirection(const Colour aColour)
{
	return (aColour & C_WHITE) ? 1 : -1;
}