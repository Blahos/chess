
#pragma once
#include <iostream>
#include <string>

#include "Functions.h"
#include "BoardState.h"
#include "BoardCoordinates.h"
#include "FunctionsCoord.h"
#include "StaticList.h"

#ifdef _WIN32
constexpr auto WHITE_KING = "KW";
constexpr auto WHITE_QUEEN = "QW";
constexpr auto WHITE_PAWN = "PW";
constexpr auto WHITE_KNIGHT = "NW";
constexpr auto WHITE_BISHOP = "BW";
constexpr auto WHITE_ROOK = "RW";

constexpr auto BLACK_KING = "KB";
constexpr auto BLACK_QUEEN = "QB";
constexpr auto BLACK_PAWN = "PB";
constexpr auto BLACK_KNIGHT = "NB";
constexpr auto BLACK_BISHOP = "BB";
constexpr auto BLACK_ROOK = "RB";
#elif __unix__
constexpr auto WHITE_KING = "\u2654 ";
constexpr auto WHITE_QUEEN = "\u2655 ";
constexpr auto WHITE_PAWN = "\u2659 ";
constexpr auto WHITE_KNIGHT = "\u2658 ";
constexpr auto WHITE_BISHOP = "\u2657 ";
constexpr auto WHITE_ROOK = "\u2656 ";

constexpr auto BLACK_KING = "\u265A ";
constexpr auto BLACK_QUEEN = "\u265B ";
constexpr auto BLACK_PAWN = "\u265F ";
constexpr auto BLACK_KNIGHT = "\u265E ";
constexpr auto BLACK_BISHOP = "\u265D ";
constexpr auto BLACK_ROOK = "\u265C ";
#endif

inline bool IsSquareEmpty(const BoardState& aState, const int aRow, const int aCol)
{
	return !aState.Squares[CoToSI(aRow, aCol)].Piece_;
}

inline bool IsSquareEmpty(const BoardState& aState, const BoardCoordinates& aCoord)
{
	return IsSquareEmpty(aState, aCoord.Row, aCoord.Col);
}

inline bool HasPieceOfColour(const BoardState& aState, const BoardCoordinates& aCoord, const Colour aColour)
{
	const SquareState& lSquare = aState.Squares[CoToSI(aCoord)];
	return lSquare.Piece_ & aColour;
}

inline void PrintBoard(const BoardState& aBoardState)
{
	for (int iRow = BoardSetup::cRows - 1; iRow >= 0; iRow--)
	{
		for (int iCol = 0; iCol < BoardSetup::cCols; iCol++)
		{
			std::string lPrint = "  ";
			
			const SquareState& lSquare = aBoardState.Squares[CoToSI(iRow, iCol)];
			
			if (lSquare.Piece_)
			{
				if (lSquare.Piece_ & P_PAWN_WHITE)
					lPrint = WHITE_PAWN;
				else if (lSquare.Piece_ & P_KNIGHT_WHITE)
					lPrint = WHITE_KNIGHT;
				else if (lSquare.Piece_ & P_BISHOP_WHITE)
					lPrint = WHITE_BISHOP;
				else if (lSquare.Piece_ & P_ROOK_WHITE)
					lPrint = WHITE_ROOK;
				else if (lSquare.Piece_ & P_QUEEN_WHITE)
					lPrint = WHITE_QUEEN;
				else if (lSquare.Piece_ & P_KING_WHITE)
					lPrint = WHITE_KING;
				else if (lSquare.Piece_ & P_PAWN_BLACK)
					lPrint = BLACK_PAWN;
				else if (lSquare.Piece_ & P_KNIGHT_BLACK)
					lPrint = BLACK_KNIGHT;
				else if (lSquare.Piece_ & P_BISHOP_BLACK)
					lPrint = BLACK_BISHOP;
				else if (lSquare.Piece_ & P_ROOK_BLACK)
					lPrint = BLACK_ROOK;
				else if (lSquare.Piece_ & P_QUEEN_BLACK)
					lPrint = BLACK_QUEEN;
				else if (lSquare.Piece_ & P_KING_BLACK)
					lPrint = BLACK_KING;
			}
			
			if ((iRow + iCol) % 2 == 0)
				std::cout << "\033[1;47m" << lPrint << "\033[0m";
			else 
				std::cout << "\033[1;40m" << lPrint << "\033[0m";
		}
		std::cout << std::endl;
	}
	std::cout << ((aBoardState.PlayerToMove & C_WHITE) ? "w" : "b") << std::endl;
	bool lAny = false;
	if (aBoardState.CanCastleKingsideWhite) { std::cout << "K"; lAny = true; }
	if (aBoardState.CanCastleQueensideWhite) { std::cout << "Q"; lAny = true; }
	if (aBoardState.CanCastleKingsideBlack) { std::cout << "k"; lAny = true; }
	if (aBoardState.CanCastleQueensideBlack) { std::cout << "q"; lAny = true; }
	if (!lAny) std::cout << "-";
	std::cout << std::endl;
	if (aBoardState.WasLastMoveDoublePawn()) std::cout << "true" << std::endl;
	else std::cout << "false" << std::endl;
}

// checks if the piece on the given square is blocked, i.e. can not move (potential move) anywhere
inline bool IsBlocked(const BoardState& aBordState, const char& aSquareIndex)
{
	const SquareState& lSquare = aBordState.Squares[aSquareIndex];
	if (!(lSquare.Piece_)) return false;

	const Piece lPiece = lSquare.Piece_;
	const Colour lPieceColour = PIECE_COLOUR(lPiece);

	if (lPiece & P_ROOK)
	{
		const int cDirCount = 4;
		const int cRowDir[] = { -1,  0, 1, 0 };
		const int cColDir[] = { 0, -1, 0, 1 };

		for (int i = 0; i < cDirCount; i++)
		{
			const BoardCoordinates lCoord(lSquare.Coordinates.Row + cRowDir[i], lSquare.Coordinates.Col + cColDir[i]);

			if (!IsIn(lCoord)) continue;

			const char lIndex = CoToSI(lCoord);
			if (aBordState.Squares[lIndex].Piece_ & lPieceColour) continue;
			return false;
		}

		return true;
	}
	else if (lPiece & P_BISHOP)
	{
		const int cDirCount = 4;
		const int cRowDir[] = { -1, 1, -1, 1 };
		const int cColDir[] = { -1, -1, 1, 1 };

		for (int i = 0; i < cDirCount; i++)
		{
			const BoardCoordinates lCoord(lSquare.Coordinates.Row + cRowDir[i], lSquare.Coordinates.Col + cColDir[i]);

			if (!IsIn(lCoord)) continue;

			const char lIndex = CoToSI(lCoord);
			if (aBordState.Squares[lIndex].Piece_ & lPieceColour) continue;
			return false;
		}

		return true;
	}
	else if (lPiece & (P_QUEEN | P_KING))
	{
		const int cDirCount = 8;
		const int cRowDir[] = { -1, 1, -1, 1, -1,  0, 1, 0 };
		const int cColDir[] = { -1, -1, 1, 1, 0, -1, 0, 1 };

		for (int i = 0; i < cDirCount; i++)
		{
			const BoardCoordinates lCoord(lSquare.Coordinates.Row + cRowDir[i], lSquare.Coordinates.Col + cColDir[i]);

			if (!IsIn(lCoord)) continue;

			const char lIndex = CoToSI(lCoord);
			if (aBordState.Squares[lIndex].Piece_ & lPieceColour) continue;
			return false;
		}

		return true;
	}

	return false;
}

inline bool IsThreefoldRepetition(const BoardState& aCurrentBoard, const std::vector<BoardState>& aPreviousBoards, const int aMaxHistory = -1)
{
	int lFoundCount = 0;

	int lPositionsChecked = 0;

	for (int i = aPreviousBoards.size() - 1; i >= 0; i--)
	{
		lPositionsChecked++;
		if (aPreviousBoards[i] == aCurrentBoard)
		{
			lFoundCount++;

			if (lFoundCount == 2)
				return true;
		}

		if (aMaxHistory > 0 && lPositionsChecked >= aMaxHistory)
			break;
	}

	return false;
}

// return all square states from given board between the provided coordinates
// (the coordinates themselves are NOT included, so if you provide adjecent squares the list will be empty)
// goes in straight line (vertical, horizontal or diagonal)
// if the given coordinates are not on a straight line it returns empty vector
inline StaticList<SquareState, 6> GetSquaresBetween(const BoardState& aBoard, const BoardCoordinates& aCoord1, const BoardCoordinates& aCoord2)
{
	StaticList<SquareState, 6> lReturnList;

	const char lDiffRow = aCoord2.Row - aCoord1.Row;
	const char lDiffCol = aCoord2.Col - aCoord1.Col;
	const char lDiffRowAbs = std::abs(lDiffRow);
	const char lDiffColAbs = std::abs(lDiffCol);

	if (lDiffRow == 0 || lDiffCol == 0 || lDiffRowAbs == lDiffColAbs)
	{
		const char lMaxDiffAbs = std::max(lDiffRowAbs, lDiffColAbs);

		if (lMaxDiffAbs <= 1) return lReturnList;

		const char lRowDir = Sgn(lDiffRow);
		const char lColDir = Sgn(lDiffCol);

		int lSize = 0;
		for (char i = 1; i < lMaxDiffAbs; i++)
		{
			const BoardCoordinates lCoord(aCoord1.Row + i * lRowDir, aCoord1.Col + i * lColDir);
			lReturnList.Items[lSize] = aBoard.Squares[CoToSI(lCoord)];
			lSize++;
		}
		lReturnList.ActualSize = lSize;
	}

	return lReturnList;
}
