
#pragma once
#include <string>
#include <vector>
#include <cctype>

#include "Move.h"
#include "BoardState.h"
#include "BoardCoordinates.h"
#include "PieceMovement.h"

#include "Exception.h"

inline bool IsPGNCoord(const char aChar)
{
	return (aChar >= 'a' && aChar <= 'h') || (aChar >= '1' && aChar <= '8');
}

inline BoardCoordinates PGNCoordToBoardCoord(const std::string& aCoord)
{
    if (aCoord.size() > 2) throw EXCEPTION_GLOBAL("Too long string!");

    BoardCoordinates lReturnStruct;

    for (int i = 0; i < aCoord.size(); i++)
    {
        //const char lChar = tolower(aCoord[i]);
        const char lChar = aCoord[i];
        if (lChar >= 'a' && lChar <= 'h')
        {
            lReturnStruct.Col = lChar - 'a';
        }
        else if (lChar >= '1' && lChar <= '8')
        {
            lReturnStruct.Row = lChar - '1';
        }
        else throw EXCEPTION_GLOBAL("Unrecognised coordinate character!");
    }

    return lReturnStruct;
}

// remove everything else from the PGN string and keep only the end and possibly start coordinates
inline std::string GetPureMoveCoordinates(const std::string& aPGNString)
{
    std::string lReturnString;

    for (int i = 0; i < aPGNString.size(); i++)
    {
        if (IsPGNCoord(aPGNString[i]))
            lReturnString += aPGNString[i];
    }

    if (lReturnString.size() < 2 || lReturnString.size() > 4) 
		throw EXCEPTION_GLOBAL("Invalid string length!");
    return lReturnString;
}

inline Piece GetPieceFromPNGSymbol(const char aPieceChar)
{
    if      (aPieceChar == 'K') return P_KING;
    else if (aPieceChar == 'Q') return P_QUEEN;
    else if (aPieceChar == 'N') return P_KNIGHT;
    else if (aPieceChar == 'B') return P_BISHOP;
    else if (aPieceChar == 'R') return P_ROOK;
    else return P_PAWN;
}

inline std::string GetPiecePGNSymbolFromPiece(const Piece& aPieceType)
{
	if		(aPieceType & P_KING) return "K";
	else if (aPieceType & P_QUEEN) return "Q";
	else if (aPieceType & P_KNIGHT) return "N";
	else if (aPieceType & P_BISHOP) return "B";
	else if (aPieceType & P_ROOK) return "R";
	else if (aPieceType & P_PAWN) return "";
	else throw EXCEPTION_GLOBAL("Unknown piece!");
}

inline Piece GetMovedPiece(const std::string& aPGNString)
{
    if (aPGNString.size() <= 0) throw EXCEPTION_GLOBAL("String too short!");
    return GetPieceFromPNGSymbol(aPGNString[0]);
}

Move ParsePGNMove(const std::string& aPGNString, const BoardState& aBoard);
// the board is needed to check whether the move is a capture, also if there is a need to specify start row or column etc.
std::string MoveToPGN(const Move& aMove, const BoardState& aBoard);

// check if there are any potential moves to the given square on the given board
bool CanBeMovedTo(const BoardState& aBoard, const BoardCoordinates& aSquareCoord);

// check if a square is potentially attacked by any piece
// does not consider en passant, so don't use it to detect attacked pawns
bool IsAttacked(const BoardState& aBoard, const BoardCoordinates& aSquareCoord);

// check if there is an X ray from square to square
// works on bishops, rooks and queens
// returns how many pieces are blocking the line between the square
// doesn't care about whose move it is
// returns -1 if there is no xray
// does not check for the square indices validity so set them right!
int IsXRay(const BoardState& aBoard, const int& aFromSquare, const int& aToSquare);

std::vector<std::string> ParsePGNMovesFile(const std::string aPath);