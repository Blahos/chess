
struct LocalisedPiece;

#pragma once
#include "PieceType.h"
#include "Colour.h"
#include "BoardCoordinates.h"
#include "FunctionsCoord.h"

typedef unsigned short Piece;

// piece definitions
#define P_NONE			(Piece)(0)

#define P_WHITE			C_WHITE
#define P_BLACK			C_BLACK

#define P_PAWN_WHITE	(Piece)(1)
#define P_KNIGHT_WHITE	(Piece)(2)
#define P_BISHOP_WHITE	(Piece)(4)
#define P_ROOK_WHITE	(Piece)(8)
#define P_QUEEN_WHITE	(Piece)(16)
#define P_KING_WHITE	(Piece)(32)

#define P_PAWN_BLACK	(Piece)(P_PAWN_WHITE << 8)
#define P_KNIGHT_BLACK	(Piece)(P_KNIGHT_WHITE << 8)
#define P_BISHOP_BLACK	(Piece)(P_BISHOP_WHITE << 8)
#define P_ROOK_BLACK	(Piece)(P_ROOK_WHITE << 8)
#define P_QUEEN_BLACK	(Piece)(P_QUEEN_WHITE << 8)
#define P_KING_BLACK	(Piece)(P_KING_WHITE << 8)

#define P_PAWN			(Piece)(P_PAWN_WHITE | P_PAWN_BLACK)
#define P_KNIGHT		(Piece)(P_KNIGHT_WHITE | P_KNIGHT_BLACK)
#define P_BISHOP		(Piece)(P_BISHOP_WHITE | P_BISHOP_BLACK)
#define P_ROOK			(Piece)(P_ROOK_WHITE | P_ROOK_BLACK)
#define P_QUEEN			(Piece)(P_QUEEN_WHITE | P_QUEEN_BLACK)
#define P_KING			(Piece)(P_KING_WHITE | P_KING_BLACK)

#define PIECE_COLOUR(piece) ((piece & C_WHITE) ? (C_WHITE) : (C_BLACK))

const Piece C_AllInividualPieces[] = { 
	P_PAWN_WHITE, P_KNIGHT_WHITE, P_BISHOP_WHITE, P_ROOK_WHITE, P_QUEEN_WHITE, P_KING_WHITE,
	P_PAWN_BLACK, P_KNIGHT_BLACK, P_BISHOP_BLACK, P_ROOK_BLACK, P_QUEEN_BLACK, P_KING_BLACK };

struct LocalisedPiece
{
public:
	Piece Piece_ = P_NONE;
	BoardCoordinates Coord;

	LocalisedPiece() {}
	LocalisedPiece(const Piece& aPiece, const BoardCoordinates& aCoord) : Piece_(aPiece), Coord(aCoord) { }

	bool operator==(const LocalisedPiece& aOther) const
	{
		return Piece_ == aOther.Piece_ && Coord == aOther.Coord;
	}
	bool operator!=(const LocalisedPiece& aOther) const
	{
		return !(*this == aOther);
	}
};

struct LocalisedPieceIndex
{
public:
	Piece Piece_ = P_NONE;
	char SquareIndex;

	LocalisedPieceIndex() {}
	LocalisedPieceIndex(const Piece& aPiece, const BoardCoordinates& aCoord) : Piece_(aPiece), SquareIndex(CoToSI(aCoord)) { }
	LocalisedPieceIndex(const Piece& aPiece, const char& aSquareIndex) : Piece_(aPiece), SquareIndex(aSquareIndex) { }

	bool operator==(const LocalisedPieceIndex& aOther) const
	{
		return Piece_ == aOther.Piece_ && SquareIndex == aOther.SquareIndex;
	}
	bool operator!=(const LocalisedPieceIndex& aOther) const
	{
		return !(*this == aOther);
	}
};