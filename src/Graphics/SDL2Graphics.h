
#ifdef WITH_SDL2

class SDL2Graphics;

#pragma once
#include "GameGraphics.h"
#include "../GameEngine.h"
#include "../BoardState.h"
#include "../FunctionsBoard.h"
#include "../Graphics/BoardWindow.h"
#include "../Players/Human.h"
#include "../Players/Random.h"
#include "../Players/Materialist.h"
#include "../Players/SmarterMaterialist.h"
#include "../Players/KingHunter.h"
#include "../Colour.h"

class SDL2Graphics : public GameGraphics
{
private:
	bool mGamePaused = false;

public:
	virtual void Run(GameEngine* aEngine, CmdInputReader* aCmdInput = nullptr) override
	{
		BoardWindow lWindow;
		lWindow.Init(aEngine);

		auto lWhitePlayer = aEngine->GetPlayer(C_WHITE);
		auto lBlackPlayer = aEngine->GetPlayer(C_BLACK);

		if (dynamic_cast<const Players::Human*>(lWhitePlayer))
		{
			lWindow.Subscribe((Players::Human*)lWhitePlayer);
		}
		if (dynamic_cast<const Players::Human*>(lBlackPlayer))
		{
			lWindow.Subscribe((Players::Human*)lBlackPlayer);
		}

		while (true)
		{
			if (!mGamePaused)
				aEngine->PlayGameTick();

			std::vector<CmdCommand> lNewCommands;

			// get cmd commands
			if (aCmdInput != nullptr)
			{
				lNewCommands = aCmdInput->PopCommands();
			}
			
			// Get the next event (has to be polled otherwise the window will get stuck!)
			SDL_Event lEvent;
			if (SDL_PollEvent(&lEvent))
			{
				 //lNewCommands.clear();
				const bool lProcessed = lWindow.ProcessEvent(lEvent);

				if (!lProcessed)
				{
					if (lEvent.type == SDL_KEYDOWN)
					{
						if (lEvent.key.keysym.sym == SDLK_SPACE)
						{
							mGamePaused = !mGamePaused;

							if (mGamePaused) std::cout << "Game paused" << std::endl;
							else std::cout << "Game unpaused" << std::endl;
						}
					}
				}
			}

			bool lTerminateCommand = false;
			for (int i = 0; i < lNewCommands.size(); i++)
			{
				const CmdCommand lCommand = lNewCommands[i];
				const bool lCommandAppliedInGame = aEngine->ApplyCmdCommand(lCommand);

				if (!lCommandAppliedInGame)
				{
					if (lCommand.Type == CmdCommandType::RotateBoard)
					{
						lWindow.RotateBoard();
					}
					if (lCommand.Type == CmdCommandType::Quit)
					{
						lTerminateCommand = true;
					}
				}
			}

			const bool lTerminate = lWindow.IsTerminated() || lTerminateCommand;
			if (lTerminate) break;

			lWindow.Redraw();

			std::this_thread::sleep_for(std::chrono::milliseconds(5));
		}

		lWindow.Finalise();

		SDL_Quit();
	}
};

#endif