
class GameGraphics;

#pragma once
#include "../GameEngine.h"
#include "../CmdInputReader.h"

class GameGraphics
{
public:
	virtual void Run(GameEngine* aEngine, CmdInputReader* aCmdInput = nullptr) = 0;
};