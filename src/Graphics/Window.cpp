
#include "Window.h"

void Window::Init(const std::string& aName, const int aWidth, const int aHeight, const bool aResizable)
{
	mWindow = SDL_CreateWindow(aName.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, aWidth, aHeight, aResizable ? SDL_WINDOW_RESIZABLE : 0);
	mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED);
}

void Window::Finalise()
{
	if (mWindow != nullptr) SDL_DestroyWindow(mWindow);
	if (mRenderer != nullptr) SDL_DestroyRenderer(mRenderer);
}

bool Window::ProcessEvent(const SDL_Event& aEvent)
{
	return false;
}