
class BoardWindow;

#pragma once
#include <map>
#include <fstream>
#include <thread>
#include <chrono>
#include <vector>
#include <string>

#include "../Move.h"
#include "../BoardState.h"
#include "../Piece.h"
#include "../MoveWrap.h"
#include "../GameEngine.h"

#include "Window.h"
#include "PromotionWindow.h"
#include "../MoveProducer.h"

class BoardWindow : public Window, public GameEngine::Listener, public MoveProducer
{
#ifdef WITH_SDL2
private:

	const std::string cPiecesImageFile = "pieces.png";

	GameEngine* mEngine = nullptr;
	BoardState mBoardState;
	BoardCoordinates mHighlightedSquare = BoardCoordinates();

    int mBoardSize = 480;
	SDL_Texture* mBoardTexture = nullptr;
    SDL_Texture* mPiecesTexture = nullptr;
	SDL_Texture* mHighlightSquareTexture = nullptr;
	SDL_Texture* mHighlightSquare2Texture = nullptr;
	SDL_Texture* mHighlightSquare3Texture = nullptr;
	SDL_Texture* mHighlightSquare4Texture = nullptr;

    std::map<Piece, SDL_Rect> mPieceTextureSrcRect;
	StaticList<Move, PieceMovement::cMaxLegalMoves> mLegalMoves;
	MoveWrap SetHighlightSquare(const BoardCoordinates aSquareCoord);
	BoardCoordinates MouseToSquare(const int aMouseX, const int aMouseY);

	bool mTerminate = false;
	bool mRotateBoard = false;

	PromotionWindow* mPromotionWindow = nullptr;
	// just to save the move when it's a promotion so we can wait for the promotion pick and then apply it
	Move mPromotionMoveBuffer;

public:

    void Init(GameEngine* const aEngine);
    virtual void Finalise() override;
	virtual bool ProcessEvent(const SDL_Event& aEvent) override;

    //void SetBoard(const BoardState& aBoardState);
    void SetSize(const int aNewSize) { mBoardSize = aNewSize; }

	int GetSize() const { return mBoardSize; }
	bool IsTerminated() { return mTerminate; }

	void Redraw();
	
	virtual void BoardChanged(const BoardState& aNewBoard) override;

	void RotateBoard();

#endif
};