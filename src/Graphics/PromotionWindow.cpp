
#include "PromotionWindow.h"

#include <vector>
#include <cmath>

#include "SDL_image.h"

void PromotionWindow::Init(const std::string aPiecesImageFile, const std::map<Piece, SDL_Rect>& aPieceMap, const Colour aColour)
{
	Window::Init("Select piece", cPieces.size() * cHeight, cHeight, false);

	SDL_Surface* lSurf = IMG_Load(aPiecesImageFile.c_str());
	mPiecesTexture = SDL_CreateTextureFromSurface(GetRenderer(), lSurf);
	SDL_FreeSurface(lSurf);

	std::vector<Piece> lPieces;
	
	for (int i = 0; i < cPieces.size(); i++)
	{
		lPieces.push_back(cPieces[i] & aColour);
	}

	SDL_SetRenderDrawColor(GetRenderer(), 102, 255, 102, 255);
	SDL_RenderClear(GetRenderer());

	for (int iPiece = 0; iPiece < lPieces.size(); iPiece++)
	{
		const Piece& lPiece = lPieces[iPiece];
		const bool lHasSourceRect = aPieceMap.find(lPiece) != aPieceMap.end();
		if (!lHasSourceRect) continue;

		const SDL_Rect lSrcR = aPieceMap.at(lPiece);

		SDL_Rect lDstR;

		const double lPiecePadCoeff = 0.1;
		lDstR.x = (int)std::round((iPiece + lPiecePadCoeff) * cHeight);
		lDstR.y = (int)std::round(lPiecePadCoeff * cHeight);
		lDstR.w = (int)std::round((1 - 2 * lPiecePadCoeff) * cHeight);
		lDstR.h = (int)std::round((1 - 2 * lPiecePadCoeff) * cHeight);

		SDL_RenderCopy(GetRenderer(), mPiecesTexture, &lSrcR, &lDstR);
	}
	SDL_RenderPresent(GetRenderer());
}
void PromotionWindow::Finalise()
{
	if (mPiecesTexture != nullptr)
		SDL_DestroyTexture(mPiecesTexture);

	Window::Finalise();
}

bool PromotionWindow::ProcessEvent(const SDL_Event& aEvent)
{
	if (aEvent.window.windowID == SDL_GetWindowID(GetWindow()))
	{
		if (aEvent.type == SDL_MOUSEBUTTONDOWN)
		{
			if (aEvent.button.button == SDL_BUTTON_LEFT)
			{
				if (aEvent.button.x < 0 || aEvent.button.x >= cPieces.size() * cHeight ||
					aEvent.button.y < 0 || aEvent.button.y >= cHeight)
				{
					return true;
				}

				const int lSelectedPieceIndex = (int)std::floor((double)aEvent.button.x / cHeight);

				mIsPieceSelected = true;
				mSelectedPiece = cPieces[lSelectedPieceIndex];

				return true;
			}
		}
	}

	return false;
}