
class PromotionWindow;

#pragma once
#include <map>
#include <string>
#include <vector>

#include "Window.h"

#include "../Piece.h"

class PromotionWindow : public Window
{
#ifdef WITH_SDL2
private:
	const int cHeight = 64;
	const std::vector<Piece> cPieces = { P_QUEEN, P_KNIGHT, P_ROOK, P_BISHOP };
	SDL_Texture* mPiecesTexture = nullptr;

	bool mIsPieceSelected = false;
	Piece mSelectedPiece = P_PAWN;

public:
	void Init(const std::string aPiecesImageFile, const std::map<Piece, SDL_Rect>& aPieceMap, const Colour aColour);
	virtual void Finalise() override;
	virtual bool ProcessEvent(const SDL_Event& aEvent) override;
	bool IsPieceSelected() { return mIsPieceSelected; }
	Piece GetSelectedPiece() { return mSelectedPiece; }

#endif
};