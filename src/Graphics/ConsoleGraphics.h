
class ConsoleGraphics;

#pragma once
#include <thread>
#include <chrono>

#include "GameGraphics.h"
#include "../GameEngine.h"
#include "../BoardState.h"
#include "../FunctionsBoard.h"
#include "../Players/Human.h"
#include "../Players/KingHunter.h"

class ConsoleGraphics : public GameGraphics
{
private:
	BoardState mCurrentBoard;

public:
	virtual void Run(GameEngine* aEngine, CmdInputReader* aCmdInput = nullptr) override
	{
		while (true)
		{
			aEngine->PlayGameTick();

			std::vector<CmdCommand> lNewCommands;

			if (aCmdInput != nullptr)
			{
				lNewCommands = aCmdInput->PopCommands();
			}

			for (int i = 0; i < lNewCommands.size(); i++)
			{
				const CmdCommand lCommand = lNewCommands[i];
				const bool lCommandAppliedInGame = aEngine->ApplyCmdCommand(lCommand);

				if (!lCommandAppliedInGame)
				{
					// handle the command in another way
				}
			}

			const BoardState lNewBoard = aEngine->GetCurrentBoard();

			bool lIsBoardDifferent = mCurrentBoard != lNewBoard;

			mCurrentBoard = lNewBoard;

			if (lIsBoardDifferent)
			{
				PrintBoard(lNewBoard);
			}

			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
	}
};