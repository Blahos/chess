
#include <iostream>
#include <cmath>
#include <set>

#include "BoardWindow.h"
#include "SDL_image.h"
#include "../PieceMovement.h"
#include "PromotionWindow.h"

void BoardWindow::Init(GameEngine* const aEngine)
{
	if (mEngine != nullptr)
	{
		mEngine->Unsubscribe(this);
		Unsubscribe(aEngine);
	}
	mEngine = aEngine;

	if (mEngine != nullptr)
	{
		mEngine->Subscribe(this);
		Subscribe(aEngine);
	}

	mBoardState = mEngine->GetCurrentBoard();

	Window::Init("Board window", mBoardSize, mBoardSize);

    SDL_Surface* lBoardSurf = IMG_Load("board.png");
    mBoardTexture = SDL_CreateTextureFromSurface(GetRenderer(), lBoardSurf);
    SDL_FreeSurface(lBoardSurf);

    SDL_Surface* lPiecesSurf = IMG_Load(cPiecesImageFile.c_str());
    mPiecesTexture = SDL_CreateTextureFromSurface(GetRenderer(), lPiecesSurf);
    SDL_FreeSurface(lPiecesSurf);

	SDL_Surface* lSquareHighlightSurf = IMG_Load("square_highlight.png");
	mHighlightSquareTexture = SDL_CreateTextureFromSurface(GetRenderer(), lSquareHighlightSurf);
	SDL_FreeSurface(lSquareHighlightSurf);

	SDL_Surface* lSquareHighlight2Surf = IMG_Load("square_highlight2.png");
	mHighlightSquare2Texture = SDL_CreateTextureFromSurface(GetRenderer(), lSquareHighlight2Surf);
	SDL_FreeSurface(lSquareHighlight2Surf);

	SDL_Surface* lSquareHighlight3Surf = IMG_Load("square_highlight3.png");
	mHighlightSquare3Texture = SDL_CreateTextureFromSurface(GetRenderer(), lSquareHighlight3Surf);
	SDL_FreeSurface(lSquareHighlight3Surf);

	SDL_Surface* lSquareHighlight4Surf = IMG_Load("square_highlight4.png");
	mHighlightSquare4Texture = SDL_CreateTextureFromSurface(GetRenderer(), lSquareHighlight4Surf);
	SDL_FreeSurface(lSquareHighlight4Surf);

    Piece lTempPiece;

    for (int i = 0; i < 2; i++)
    {
        Colour lColour = C_WHITE;
        int lHeightOffset = 0;
        if (i == 1)
        {
            lColour = C_BLACK;
            lHeightOffset = 337;
        }

        lTempPiece = P_KING & lColour;
        mPieceTextureSrcRect[lTempPiece] = SDL_Rect();
        mPieceTextureSrcRect[lTempPiece].x = 59;
        mPieceTextureSrcRect[lTempPiece].y = lHeightOffset;
        mPieceTextureSrcRect[lTempPiece].w = 260;
        mPieceTextureSrcRect[lTempPiece].h = 270;

		lTempPiece = P_QUEEN & lColour;
        mPieceTextureSrcRect[lTempPiece] = SDL_Rect();
        mPieceTextureSrcRect[lTempPiece].x = 378;
        mPieceTextureSrcRect[lTempPiece].y = lHeightOffset;
        mPieceTextureSrcRect[lTempPiece].w = 289;
        mPieceTextureSrcRect[lTempPiece].h = 270;

		lTempPiece = P_BISHOP & lColour;
        mPieceTextureSrcRect[lTempPiece] = SDL_Rect();
        mPieceTextureSrcRect[lTempPiece].x = 726;
        mPieceTextureSrcRect[lTempPiece].y = lHeightOffset;
        mPieceTextureSrcRect[lTempPiece].w = 260;
        mPieceTextureSrcRect[lTempPiece].h = 270;

		lTempPiece = P_KNIGHT & lColour;
        mPieceTextureSrcRect[lTempPiece] = SDL_Rect();
        mPieceTextureSrcRect[lTempPiece].x = 1060;
        mPieceTextureSrcRect[lTempPiece].y = lHeightOffset;
        mPieceTextureSrcRect[lTempPiece].w = 250;
        mPieceTextureSrcRect[lTempPiece].h = 270;

		lTempPiece = P_ROOK & lColour;
        mPieceTextureSrcRect[lTempPiece] = SDL_Rect();
        mPieceTextureSrcRect[lTempPiece].x = 1400;
        mPieceTextureSrcRect[lTempPiece].y = lHeightOffset;
        mPieceTextureSrcRect[lTempPiece].w = 245;
        mPieceTextureSrcRect[lTempPiece].h = 270;

		lTempPiece = P_PAWN & lColour;
        mPieceTextureSrcRect[lTempPiece] = SDL_Rect();
        mPieceTextureSrcRect[lTempPiece].x = 1742;
        mPieceTextureSrcRect[lTempPiece].y = lHeightOffset;
        mPieceTextureSrcRect[lTempPiece].w = 220;
        mPieceTextureSrcRect[lTempPiece].h = 270;
    }
}

void BoardWindow::Finalise()
{
	UnsubscribeAll();

	if (mEngine != nullptr)
	{
		mEngine->Unsubscribe(this);
		Unsubscribe(mEngine);
		mEngine = nullptr;
	}

    if (mBoardTexture != nullptr) SDL_DestroyTexture(mBoardTexture);
    if (mPiecesTexture != nullptr) SDL_DestroyTexture(mPiecesTexture);
	if (mHighlightSquareTexture != nullptr) SDL_DestroyTexture(mHighlightSquareTexture);
	if (mHighlightSquare2Texture != nullptr) SDL_DestroyTexture(mHighlightSquare2Texture);
	if (mHighlightSquare3Texture != nullptr) SDL_DestroyTexture(mHighlightSquare3Texture);
	if (mHighlightSquare4Texture != nullptr) SDL_DestroyTexture(mHighlightSquare4Texture);

	Window::Finalise();
}

bool BoardWindow::ProcessEvent(const SDL_Event& aEvent)
{
	if (mPromotionWindow == nullptr)
	{
		if (aEvent.window.windowID != SDL_GetWindowID(GetWindow())) return false;

		if (aEvent.type == SDL_WINDOWEVENT)
		{
			if (aEvent.window.event == SDL_WINDOWEVENT_RESIZED)
			{
				const int lNewBoardSize = std::min(aEvent.window.data1, aEvent.window.data2);
				mBoardSize = lNewBoardSize;
				return true;
			}
			if (aEvent.window.event == SDL_WINDOWEVENT_CLOSE)
			{
				mTerminate = true;
				return true;
			}
		}
		if (aEvent.type == SDL_MOUSEBUTTONDOWN)
		{
			if (mEngine == nullptr) throw EXCEPTION_MEMBER("No engine!");
			if (aEvent.button.button == SDL_BUTTON_LEFT)
			{
				const BoardCoordinates lCoord = MouseToSquare(aEvent.button.x, aEvent.button.y);
				MoveWrap lWrap = SetHighlightSquare(lCoord);

				if (lWrap.IsMove)
				{
					if (lWrap.Move_.IsPromotion())
					{
						if (mPromotionWindow != nullptr) throw EXCEPTION_MEMBER("Promotion window already exists!");
						mPromotionWindow = new PromotionWindow();
						mPromotionWindow->Init(cPiecesImageFile, mPieceTextureSrcRect, PIECE_COLOUR(lWrap.Move_.MovedPiece));
						mPromotionMoveBuffer = lWrap.Move_;
					}
					else
					{
						NotifyNewMove(lWrap.Move_);
					}
				}

				return true;
			}
			else if (aEvent.button.button == SDL_BUTTON_RIGHT)
			{
				SetHighlightSquare(BoardCoordinates());
				return true;
			}
		}
	}
	else
	{
		const bool lReturnValue = mPromotionWindow->ProcessEvent(aEvent);

		if (mPromotionWindow->IsPieceSelected())
		{
			if (!mPromotionMoveBuffer.IsPromotion()) throw EXCEPTION_MEMBER("Move is not a promotion!");
			mPromotionMoveBuffer.PromotionPiece = mPromotionWindow->GetSelectedPiece();
			
			NotifyNewMove(mPromotionMoveBuffer);

			mPromotionWindow->Finalise();
			delete mPromotionWindow;
			mPromotionWindow = nullptr;
		}

		return lReturnValue;
	}

	return false;
}

MoveWrap BoardWindow::SetHighlightSquare(const BoardCoordinates aSquareCoord)
{
	MoveWrap lReturnWrap;
	lReturnWrap.IsMove = false;

	mHighlightedSquare = aSquareCoord;

	for (int i = 0; i < mLegalMoves.ActualSize; i++)
	{
		if (mLegalMoves.Items[i].End == aSquareCoord)
		{
			lReturnWrap.IsMove = true;
			lReturnWrap.Move_ = mLegalMoves.Items[i];
			break;
		}
	}

	if (IsIn(mHighlightedSquare))
	{
		mLegalMoves.ActualSize = 0;
		PieceMovement::GetLegalMoves(mBoardState, mHighlightedSquare.Row, mHighlightedSquare.Col, mLegalMoves);
	}
	else mLegalMoves.ActualSize = 0;

	return lReturnWrap;
}
BoardCoordinates BoardWindow::MouseToSquare(const int aMouseX, const int aMouseY)
{
	const double lBoardSize = (double)mBoardSize;
	const double lPixelsPerRow = lBoardSize / BoardSetup::cRows;
	const double lPixelsPerCol = lBoardSize / BoardSetup::cCols;

	int lRow = (int)std::round(BoardSetup::cRows - 1.0 - (int)std::floor((double)aMouseY / lPixelsPerRow));
	int lCol = (int)std::floor((double)aMouseX / lPixelsPerCol);

	if (mRotateBoard)
	{
		lRow = BoardSetup::cRows - 1 - lRow;
		lCol = BoardSetup::cCols - 1 - lCol;
	}

	return BoardCoordinates(lRow, lCol);
}

void BoardWindow::Redraw()
{
	double lRotateAngle = mRotateBoard ? 180 : 0;

	SDL_Rect lBoardSrc;
	lBoardSrc.x = 5;
	lBoardSrc.y = 5;
	lBoardSrc.w = 810 - 2 * 5;
	lBoardSrc.h = 810 - 2 * 5;

	SDL_FRect lBoardDst;
	lBoardDst.x = 0;
	lBoardDst.y = 0;
	lBoardDst.w = mBoardSize;
	lBoardDst.h = mBoardSize;

	SDL_RenderClear(GetRenderer());

	SDL_RenderCopyExF(GetRenderer(), mBoardTexture, &lBoardSrc, &lBoardDst, lRotateAngle, nullptr, SDL_FLIP_NONE);

	const double lSquareSizeX = (double)mBoardSize / BoardSetup::cCols;
	const double lSquareSizeY = (double)mBoardSize / BoardSetup::cRows;

	for (int iSquare = 0; iSquare < BoardSetup::cRows * BoardSetup::cCols; iSquare++)
	{
		const SquareState& lSquare = mBoardState.Squares[iSquare];
		if (!lSquare.Piece_) continue;

		const bool lHasSourceRect = mPieceTextureSrcRect.find(lSquare.Piece_) != mPieceTextureSrcRect.end();
		if (!lHasSourceRect) continue;

		SDL_Rect lSrcR = mPieceTextureSrcRect.at(lSquare.Piece_);
		SDL_FRect lDstR;

		const double lPiecePadCoeff = 0.1;

		BoardCoordinates lDrawCoordinates(lSquare.Coordinates);
		if (mRotateBoard)
		{
			lDrawCoordinates.Row = BoardSetup::cRows - lDrawCoordinates.Row - 1;
			lDrawCoordinates.Col = BoardSetup::cCols - lDrawCoordinates.Col - 1;
		}

		lDstR.x = (lDrawCoordinates.Col + lPiecePadCoeff) * lSquareSizeX;
		lDstR.y = mBoardSize - ((double)lDrawCoordinates.Row + 1 - lPiecePadCoeff) * lSquareSizeY;
		lDstR.w = (1 - 2 * lPiecePadCoeff) * lSquareSizeX;
		lDstR.h = (1 - 2 * lPiecePadCoeff) * lSquareSizeY;
		SDL_RenderCopyF(GetRenderer(), mPiecesTexture, &lSrcR, &lDstR);
	}

	// highlight last move
	if (mEngine != nullptr)
	{
		const Move lLastMove = mEngine->GetLastMove();
		if (IsIn(lLastMove.Start) && IsIn(lLastMove.End))
		{
			BoardCoordinates lDrawCoordinates(lLastMove.Start);
			if (mRotateBoard)
			{
				lDrawCoordinates.Row = BoardSetup::cRows - lDrawCoordinates.Row - 1;
				lDrawCoordinates.Col = BoardSetup::cCols - lDrawCoordinates.Col - 1;
			}

			SDL_FRect lDstR1;
			lDstR1.x = lDrawCoordinates.Col * lSquareSizeX;
			lDstR1.y = mBoardSize - (lDrawCoordinates.Row + 1) * lSquareSizeY;
			lDstR1.w = lSquareSizeX;
			lDstR1.h = lSquareSizeY;

			SDL_RenderCopyF(GetRenderer(), mHighlightSquare3Texture, NULL, &lDstR1);

			lDrawCoordinates = lLastMove.End;
			if (mRotateBoard)
			{
				lDrawCoordinates.Row = BoardSetup::cRows - lDrawCoordinates.Row - 1;
				lDrawCoordinates.Col = BoardSetup::cCols - lDrawCoordinates.Col - 1;
			}

			SDL_FRect lDstR2;
			lDstR2.x = lDrawCoordinates.Col * lSquareSizeX;
			lDstR2.y = mBoardSize - (lDrawCoordinates.Row + 1) * lSquareSizeY;
			lDstR2.w = lSquareSizeX;
			lDstR2.h = lSquareSizeY;

			SDL_RenderCopyF(GetRenderer(), mHighlightSquare3Texture, NULL, &lDstR2);
		}
	}

	// highlight checked king
	if (mBoardState.IsMovingPlayerInCheck())
	{
		const BoardCoordinates& lKingCoord = (mBoardState.PlayerToMove & C_WHITE) ? mBoardState.WhiteKing : mBoardState.BlackKing;

		BoardCoordinates lDrawCoordinates(lKingCoord);
		if (mRotateBoard)
		{
			lDrawCoordinates.Row = BoardSetup::cRows - lDrawCoordinates.Row - 1;
			lDrawCoordinates.Col = BoardSetup::cCols - lDrawCoordinates.Col - 1;
		}

		SDL_FRect lDstR;
		lDstR.x = lDrawCoordinates.Col * lSquareSizeX;
		lDstR.y = mBoardSize - (lDrawCoordinates.Row + 1) * lSquareSizeY;
		lDstR.w = lSquareSizeX;
		lDstR.h = lSquareSizeY;

		SDL_RenderCopyF(GetRenderer(), mHighlightSquare4Texture, NULL, &lDstR);
	}

	// highlight selected square and legal moves
	if (IsIn(mHighlightedSquare))
	{
		BoardCoordinates lDrawCoordinates(mHighlightedSquare);
		if (mRotateBoard)
		{
			lDrawCoordinates.Row = BoardSetup::cRows - lDrawCoordinates.Row - 1;
			lDrawCoordinates.Col = BoardSetup::cCols - lDrawCoordinates.Col - 1;
		}

		SDL_FRect lDstR;
		lDstR.x = (lDrawCoordinates.Col) * lSquareSizeX;
		lDstR.y = mBoardSize - (lDrawCoordinates.Row + 1) * lSquareSizeY;
		lDstR.w = lSquareSizeX;
		lDstR.h = lSquareSizeY;

		SDL_RenderCopyF(GetRenderer(), mHighlightSquareTexture, NULL, &lDstR);

		const double lMoveHighlightOffset = 0.2;

		std::set<BoardCoordinates> lLegalMovesEndSquares;

		for (int i = 0; i < mLegalMoves.ActualSize; i++)
		{
			const BoardCoordinates lCoord = mLegalMoves.Items[i].End;

			// this is to avoid highlighting certain squares multiple times 
			// (can happen with pawn promotion, because those are 4 moves with the same end coordinates)
			if (lLegalMovesEndSquares.find(lCoord) != lLegalMovesEndSquares.end()) continue;
			lLegalMovesEndSquares.insert(lCoord);

			BoardCoordinates lDrawCoordinates(lCoord);
			if (mRotateBoard)
			{
				lDrawCoordinates.Row = BoardSetup::cRows - lDrawCoordinates.Row - 1;
				lDrawCoordinates.Col = BoardSetup::cCols - lDrawCoordinates.Col - 1;
			}

			SDL_FRect lDstR2;
			lDstR2.x = (lDrawCoordinates.Col + lMoveHighlightOffset) * lSquareSizeX;
			lDstR2.y = mBoardSize - (lDrawCoordinates.Row + 1 - lMoveHighlightOffset) * lSquareSizeY;
			lDstR2.w = (1 - 2 * lMoveHighlightOffset) * lSquareSizeX;
			lDstR2.h = (1 - 2 * lMoveHighlightOffset) * lSquareSizeY;

			SDL_RenderCopyF(GetRenderer(), mHighlightSquare2Texture, NULL, &lDstR2);
		}
	}

	//Update screen
	SDL_RenderPresent(GetRenderer());
}

void BoardWindow::BoardChanged(const BoardState& aNewBoard)
{
	mBoardState = aNewBoard;
	mLegalMoves.ActualSize = 0;
	mHighlightedSquare = BoardCoordinates();
}

void BoardWindow::RotateBoard()
{
	mRotateBoard = !mRotateBoard;
	SetHighlightSquare(BoardCoordinates());
}