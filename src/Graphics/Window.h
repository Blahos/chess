
class Window;

#pragma once
#include <string>

#ifdef WITH_SDL2
#include "SDL.h"
#ifdef _WIN32
// needed for SDL2
#undef main
#endif
#endif

class Window
{
#ifdef WITH_SDL2
private:
	SDL_Window* mWindow = nullptr;
	SDL_Renderer* mRenderer = nullptr;

public:
	virtual void Init(const std::string& aName, const int aWidth, const int aHeight, const bool aResizable = true);
	virtual void Finalise();

	virtual bool ProcessEvent(const SDL_Event& aEvent);

	SDL_Window* GetWindow() { return mWindow; }
	SDL_Renderer* GetRenderer() { return mRenderer; }

#endif
};