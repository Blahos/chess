
#pragma once

#include <iostream>
#include <string>
#include "Exception.h"

namespace Curl
{
#ifdef WITH_CURL
#include "curl/curl.h"

	void Init()
	{
		const CURLcode lCode = curl_global_init(CURL_GLOBAL_ALL);
		if (lCode != 0)
			throw EXCEPTION_GLOBAL("CURL initialisation failed! Code: " + std::to_string(lCode));
		std::cout << "CURL initialised successfully" << std::endl;
	}

	void Cleanup()
	{
		curl_global_cleanup();
		std::cout << "CURL cleaned up successfully" << std::endl;
	}

	size_t WriteCallback(void* contents, size_t size, size_t nmemb, void* userp)
	{
		(*(std::string*)userp) = std::string((char*)contents, size * nmemb);
		return size * nmemb;
	}

	std::string Get(const std::string& aURL)
	{
		CURL* curl;
		CURLcode res;
		std::string readBuffer;

		curl = curl_easy_init();
		if (curl) 
		{
			curl_easy_setopt(curl, CURLOPT_URL, aURL.c_str());
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
			res = curl_easy_perform(curl);

			curl_easy_cleanup(curl);

			return readBuffer;
		}
		else
		{
			std::cout << "CURL failed!" << std::endl;
			return "";
		}
	}

	std::string Post(const std::string& aMessage, const std::string& aURL)
	{
		CURL* curl;
		CURLcode res;
		std::string readBuffer;

		/* get a curl handle */
		curl = curl_easy_init();
		if (curl)
		{
			/* First set the URL that is about to receive our POST. This URL can
			   just as well be a https:// URL if that is what should receive the
			   data. */
			curl_easy_setopt(curl, CURLOPT_URL, aURL.c_str());
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
			/* Now specify the POST data */
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, aMessage.c_str());

			/* Perform the request, res will get the return code */
			res = curl_easy_perform(curl);
			/* Check for errors */
			if (res != CURLE_OK)
				fprintf(stderr, "curl_easy_perform() failed: %s\n",
					curl_easy_strerror(res));

			/* always cleanup */
			curl_easy_cleanup(curl);

			return readBuffer;
		}
		else
		{
			std::cout << "CURL failed!" << std::endl;
			return "";
		}
	}

#endif
}
