
template <class TItem, int TMaxSize>
struct StaticList;

#pragma once
#include <array>

template <class TItem, int TMaxSize>
struct StaticList
{
public:
	std::array<TItem, TMaxSize> Items;
	int ActualSize = 0;
};
