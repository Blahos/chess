
#include <iostream>
#include <string>
#include <sstream>
#include <thread>
#include <fstream>
#include <limits>

#include "src/BoardState.h"
#include "src/FunctionsBoard.h"
#include "src/FunctionsMove.h"

#include "src/Exception.h"

#include "src/GameEngine.h"
#include "src/Graphics/ConsoleGraphics.h"
#include "src/CmdInputReader.h"
#include "src/Players/Human.h"
#include "src/Players/WebPlayer.h"

#include "src/Tables.h"

#include "src/Graphics/SDL2Graphics.h"

#include "src/FunctionsCURL.h"

#include "src/CmdArgumentParser.h"

int main(int argc, char **argv)
{
	std::cout << "Chess started with " << argc << " arguments: " << std::endl;
	for (int i = 0; i < argc; i++)
	{
		std::cout << i << ": " << std::string(argv[i]) << std::endl;
	}

	//const int lOMPThreadCount = GetOMPThreadCount();
	//std::cout << "OpenMP thread count: " << lOMPThreadCount << std::endl;

	GameGraphics* lGraphics = nullptr;

	std::cout << "Board state size: " << sizeof(BoardState) << std::endl;
	std::cout << "Square array size: " << sizeof(BoardState::Squares) << std::endl;

	try
	{
#ifdef WITH_CURL
		Curl::Init();
#endif
		Tables::InitTables();

		GameEngine lEngine;

		lEngine.SetMode(GameEngine::BoardMode::Game);

		CmdInputReader* lCmdReader = new CmdInputReader();

		lCmdReader->Subscribe(&lEngine);

		// set players for the engine
		
		Players::Player* lWhitePlayer = new Players::Human();
		//Players::Player* lWhitePlayer = new Players::Random();
		//Players::Player* lWhitePlayer = new Players::Materialist(6, 100000, 1 * 1024);
		//Players::Player* lWhitePlayer = new Players::SmarterMaterialist(6, -1, 1 * 1024);
		//Players::Player* lWhitePlayer = new Players::KingHunter(7, -1, 2 * 1024, 0 * 1024, true);
		//Players::Player* lWhitePlayer = new Players::WebPlayer("localhost:8080", "JiriChessAppWhite", "JiriChessAppBlack", C_WHITE);

		//Players::Player* lBlackPlayer = new Players::Human();
		//Players::Player* lBlackPlayer = new Players::Random();
		//Players::Player* lBlackPlayer = new Players::Materialist(6, -1,  0 * 1024, 0, true, true);
		//Players::Player* lBlackPlayer = new Players::SmarterMaterialist(6);
		Players::Player* lBlackPlayer = new Players::KingHunter(5, -1, 0 * 1024, 0 * 1024, true, true);
		//Players::Player* lBlackPlayer = new Players::WebPlayer("localhost:8080", "JiriChessAppBlack", "JiriChessAppWhite", C_BLACK);

		if (dynamic_cast<Players::Human*>(lWhitePlayer))
		{
			lCmdReader->Subscribe((Players::Human*)lWhitePlayer);
		}
		if (dynamic_cast<Players::Human*>(lBlackPlayer))
		{
			lCmdReader->Subscribe((Players::Human*)lBlackPlayer);
		}

		lEngine.SetPlayer(C_WHITE, lWhitePlayer);
		lEngine.SetPlayer(C_BLACK, lBlackPlayer);

		std::vector<std::string> lInitialMoves;

		const std::string lInitialMovesFile = CmdArgumentParser::FindValueForKey("-m", argc, argv);
		//const std::string lInitialMovesFile = "C:\\Users\\jiriblahos\\Desktop\\moves.txt";

		if (lInitialMovesFile.size() > 0)
		{
			std::string lPGNFile(lInitialMovesFile);
			lInitialMoves = ParsePGNMovesFile(lPGNFile);
		}

		lEngine.SetInitialMoves(lInitialMoves);

		lEngine.NewGame();

#ifdef WITH_SDL2
		lGraphics = new SDL2Graphics();
#else
		lGraphics = new ConsoleGraphics();
#endif

		std::thread lCmdInputThread(&CmdInputReader::Run, lCmdReader);

		try
		{
			lGraphics->Run(&lEngine, lCmdReader);
		}
		catch (const Exception& lEx)
		{
			lCmdReader->Unsubscribe(&lEngine);
			lCmdReader->Terminate();
			std::cout << "Exception: " << std::endl << lEx << std::endl;
			std::cout << "Press enter to finish cmd reader ..." << std::endl;
			lCmdInputThread.join();
			delete lCmdReader;
			throw;
		}
		catch (...)
		{
			lCmdReader->Terminate();
			std::cout << "Press enter to finish cmd reader ..." << std::endl;
			lCmdInputThread.join();
			delete lCmdReader;
			throw;
		}

		lCmdReader->Terminate();
		std::cout << "Press enter to finish cmd reader ..." << std::endl;
		lCmdInputThread.join();

		delete lCmdReader;

#ifdef WITH_CURL
		Curl::Cleanup();
#endif
	}
	catch (const Exception& lEx)
	{
		std::cout << "Exception: " << std::endl;
		std::cout << lEx << std::endl;
	}

	if (lGraphics != nullptr)
		delete lGraphics;
	
    return 0;
}

