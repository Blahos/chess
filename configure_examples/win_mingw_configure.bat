
mkdir build
cd build

cmake ^
	-G "MinGW Makefiles" ^
	-D With_SDL2="TRUE" ^
	-D CMAKE_BUILD_TYPE="Release" ^
	-D SDL2_INCLUDE_DIR="C:\Libraries\SDL2\SDL2-2.0.10\include;C:\Libraries\SDL2\SDL2_image-2.0.5\include" ^
	-D SDL2_LIB_DIR="C:\Libraries\SDL2\SDL2_image-2.0.5\lib\x64;C:\Libraries\SDL2\SDL2-2.0.10\lib\x64" ^
	../
