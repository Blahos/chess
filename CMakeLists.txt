cmake_minimum_required(VERSION 2.6)
project(chess)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
cmake_policy(SET CMP0012 NEW)

find_package(OpenMP)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
else()
	message(STATUS "OpenMP not found")
endif()

if ((DEFINED With_SDL2) AND (${With_SDL2}))

    message(STATUS "Configuring with SDL2")
    add_definitions(-DWITH_SDL2)
    if (DEFINED SDL2_INCLUDE_DIR)
        include_directories(${SDL2_INCLUDE_DIR})
    endif()

    if (DEFINED SDL2_LIB_DIR)
        link_directories(${SDL2_LIB_DIR})
    endif()
endif()

if ((DEFINED With_CURL) AND (${With_CURL}))

    message(STATUS "Configuring with CURL")
    add_definitions(-DWITH_CURL)

    if (DEFINED CURL_INCLUDE_DIR)
        include_directories(${CURL_INCLUDE_DIR})
    endif()

    if (DEFINED CURL_LIB_DIR)
        link_directories(${CURL_LIB_DIR})
    endif()
	
endif()

#3rd party libs included directly in the project (just headers)
include_directories("./3rd_party_libs/robin-hood/robin-hood-hashing-3.4.3/src/include/")
include_directories("./3rd_party_libs/json_nlohmann_3.7.3/single_include/nlohmann")

configure_file("./Images/board.png" "./board.png" COPYONLY)
configure_file("./Images/pieces.png" "./pieces.png" COPYONLY)
configure_file("./Images/square_highlight.png" "./square_highlight.png" COPYONLY)
configure_file("./Images/square_highlight2.png" "./square_highlight2.png" COPYONLY)
configure_file("./Images/square_highlight3.png" "./square_highlight3.png" COPYONLY)
configure_file("./Images/square_highlight4.png" "./square_highlight4.png" COPYONLY)

set(CHESS_SRC src/BoardState.cpp src/Exception.cpp src/PieceMovement.cpp src/FunctionsMove.cpp src/Move.cpp src/Tables.cpp src/Time.cpp src/Random.cpp src/Players/TreeSearch.cpp)

if (${With_SDL2})
    set(CHESS_SRC ${CHESS_SRC} src/Graphics/Window.cpp src/Graphics/BoardWindow.cpp src/Graphics/PromotionWindow.cpp)
endif()

add_executable(chess main.cpp ${CHESS_SRC})

set(LIBS "")

IF(MINGW)
	set(LIBS ${LIBS} atomic)
endif()

if (UNIX)
    set(LIBS ${LIBS} pthread atomic)
endif()

if (${With_SDL2})
    set(LIBS ${LIBS} SDL2 SDL2_image)
endif()

if (${With_CURL})
	set(LIBS ${LIBS} libcurl)
endif()

target_link_libraries(chess ${LIBS})

install(TARGETS chess RUNTIME DESTINATION bin)

if (${With_CURL})
	if (WIN32)
		find_file(LIBCURL libcurl.dll PATHS ${CURL_LIB_DIR})
		configure_file(${LIBCURL} "./libcurl.dll" COPYONLY)
	endif()
endif()

if (${With_SDL2})
    if (WIN32)
        find_file(SDL2_LIB_SDL SDL2.dll PATHS ${SDL2_LIB_DIR})
        configure_file(${SDL2_LIB_SDL} "./SDL2.dll" COPYONLY)

        find_file(SDL2_LIB_SDL_Image SDL2_image.dll PATHS ${SDL2_LIB_DIR})
        configure_file(${SDL2_LIB_SDL_Image} "./SDL2_image.dll" COPYONLY)

        find_file(SDL2_LIB_PNG libpng16-16.dll PATHS ${SDL2_LIB_DIR})
        configure_file(${SDL2_LIB_PNG} "./libpng16-16.dll" COPYONLY)

        find_file(SDL2_LIB_ZLIB1 zlib1.dll PATHS ${SDL2_LIB_DIR})
        configure_file(${SDL2_LIB_ZLIB1} "./zlib1.dll" COPYONLY)
    endif()
endif()

set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3 -std=c++11")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -std=c++11")
